class Solution {
public:
    void solveSudoku(vector<vector<char>>& board) 
    {
        m_col = vector<vector<bool>>(9,vector<bool>(9,false));
        m_row = vector<vector<bool>>(9,vector<bool>(9,false));
        m_blk = vector<vector<vector<bool>>>(3,vector<vector<bool>>(3,vector<bool>(9,false)));
        for(int i=0;i<9;i++)
        {
            for(int j=0;j<9;j++)
            {
                if(board[i][j] == '.')
                    continue;
                int n = board[i][j] - '0' - 1;
                m_col[i][n] = true;
                m_row[j][n] = true;
                m_blk[i/3][j/3][n] = true;
            }
        }
        helper(board,0,0);
    }
private:
    vector<vector<bool>> m_col;
    vector<vector<bool>> m_row;
    vector<vector<vector<bool>>> m_blk;
    bool helper(vector<vector<char>>& board,int x,int y)
    {
        if(x >= 9)
            return true;
        int next_y = (y+1) % 9;
        int next_x = (next_y == 0) ? x+1: x;
        cout<<x << " "<<y <<endl;
        if(board[x][y] != '.')
            return helper(board,next_x,next_y);
        for(int i = 1 ;i <= 9;i++)
        {
            if(!m_col[x][i - 1]&&!m_row[y][i - 1]&&!m_blk[x/3][y/3][i - 1])
            {
                //cout<<"!"<<i<<endl;
                board[x][y] = i + '0';
                m_col[x][i - 1] = true;
                m_row[y][i - 1] = true;
                m_blk[x/3][y/3][i - 1] = true;
                if(helper(board,next_x,next_y))
                    return true;
                board[x][y] = '.';
                m_col[x][i - 1] = false;
                m_row[y][i - 1] = false;
                m_blk[x/3][y/3][i - 1] = false;
            }
        }
        //cout<<endl;
        return false;
    }
};