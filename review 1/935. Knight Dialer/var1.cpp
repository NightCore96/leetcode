class Solution {
public:
    int knightDialer(int N)
    {
        int mod =  1e9 + 7;
        vector<vector<int>> dp(10,vector<int>(N,0));
        for(int i = 0;i<10;i++)
        {
            dp[i][0] = 1;
        }
        for(int i = 1;i<N;i++)
        {
            dp[1][i] = (dp[6][i-1] + dp[8][i-1]) % mod;
            dp[2][i] = (dp[7][i-1] + dp[9][i-1]) % mod;
            dp[3][i] = (dp[4][i-1] + dp[8][i-1]) % mod;
            dp[4][i] = ((dp[3][i-1] + dp[9][i-1])% mod + dp[0][i-1]) % mod;
            dp[6][i] = ((dp[1][i-1] + dp[7][i-1])% mod + dp[0][i-1]) % mod;
            dp[7][i] = (dp[2][i-1] + dp[6][i-1]) % mod;
            dp[8][i] = (dp[1][i-1] + dp[3][i-1]) % mod;
            dp[9][i] = (dp[2][i-1] + dp[4][i-1]) % mod;
            dp[0][i] = (dp[4][i-1] + dp[6][i-1]) % mod;
        }
        int res = 0;
        for(int i = 0;i<10;i++)
        {
            res += dp[i][N-1];
            res %= mod;
        }
        return res;
    }
};
