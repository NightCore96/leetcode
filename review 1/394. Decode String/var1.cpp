class Solution {
public:
    string decodeString(string s) 
    {
        string res = "";
        stack<string> strstack;
        stack<int> numstack;
        int nums = 0;
        string str = "";
        for(char c : s)
        {
            if(c>='0'&&c<='9')
            {
                nums = nums*10 + (c - '0');
            }
            else
            {
                if(nums> 0)
                {
                    numstack.push(nums);
                    nums = 0;
                }
                if(c == '[')
                {
                    if(str != "")
                    {
                        strstack.push(str);
                        str = "";
                    }       
                    strstack.push("[");
                }
                else if(c == ']')
                {
                    if(str != "")
                    {
                        strstack.push(str);
                        str = "";
                    }
                    string temp = "";
                    while(!strstack.empty())
                    {
                        string top = strstack.top();
                        strstack.pop();
                        if(top == "[")
                        {
                            break;
                        }
                        temp = top + temp;
                    }
                    int n =  numstack.empty()? 1 : numstack.top();
                    numstack.pop();
                    string numsstr = "";
                    for(int i = 0; i< n;i++)
                    {
                        numsstr =  temp + numsstr;
                    }
                    strstack.push(numsstr);
                }
                else
                {
                    str += c;
                }
            }
        }
        if(str != "")
        {
            strstack.push(str);
            str = "";
        }
        while(!strstack.empty())
        {
            res = strstack.top() + res ;
            strstack.pop();
        }
        return res;
    }
};