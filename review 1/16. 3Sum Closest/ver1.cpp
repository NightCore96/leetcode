class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) 
    {
        sort(nums.begin(),nums.end());
        int size = nums.size();
        int res = 0;
        int closeset = INT_MAX;
        for(int i = 0; i < size - 2; i++)
        {
            int L = i + 1;
            int R = size - 1;
            while(L < R)
            {
                int sum = nums[i] + nums[R] + nums[L];
                if(sum == target)
                {
                    return target;
                }
                else if(sum > target)
                {
                    R--;
                }
                else
                {
                    L++;
                }
                int c = abs(target-sum);
                if( c < closeset)
                {
                    closeset = c;
                    res = sum;
                }
            }
            
        }
        return res;
    }
};