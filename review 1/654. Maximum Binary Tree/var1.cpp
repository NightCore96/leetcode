/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums)
    {
       int N = nums.size();
        vector<int> parent(N, -1);
        vector<TreeNode*> nodes(N);
        int r = 0;
        
        for (int i = 0; i < N; ++i)
            nodes[i] = new TreeNode(nums[i]);
        
        for (int i = 1; i < N; ++i)
        {
            if (nums[i-1] > nums[i])
            {
                nodes[i-1]->right = nodes[i];
                parent[i] = i - 1;
            }
            else
            {
                int j = i - 1;
                while (parent[j] != -1 && nums[parent[j]] < nums[i])
                    j = parent[j];
                
                parent[i] = parent[j];
                nodes[i]->left = nodes[j];
                parent[j] = i;
                
                if (parent[i] != -1)
                    nodes[parent[i]]->right = nodes[i];
                else
                    r = i;
            }
        }
        
        return nodes[r];
    }
};
