class Solution {
public:
    int leastBricks(vector<vector<int>>& wall)
    {
        if(wall.empty())
            return 0;
        int m = wall.size();
        int n = wall[0].size();
        vector<vector<int>> sums = wall;
        int res = 0;
        unordered_map<int,int> bricks;
        
        for(int i = 0 ; i < m ; i++)
        {
            for(int j = 0; j < wall[i].size() - 1; j++)
            {
                if( j > 0 )
                    sums[i][j] = sums[i][j-1] + wall[i][j];
                bricks[sums[i][j]]++;
                res = max(res,bricks[sums[i][j]]);
            }
        }
        
        return m - res;
        
    }
};
