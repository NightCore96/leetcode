class Solution {
public:
    int numDecodings(string s) {

    long long n=s.length();
    if(s[0]=='0' || n==0)return 0; /* if beginning is 0 or length is 0 return 0*/
    
    int mm=1000000000+7;
    vector<long long> dp(n+1,0);
    dp[0]=1;
   
    if(s[0]=='*')dp[1]=9;  /* if 1st dig is 9 - then we have 9 cases*/
    else if(s[0]=='0')dp[1]=0;
    else dp[1]=1;
    
    for(int i=2;i<=n;i++){
                if(s[i-1]=='*'){  /*making separate case if it starts with * */
            
            dp[i]=9*dp[i-1];
            
            if(s[i-2]=='1')dp[i]=(dp[i]+9*dp[i-2])%mm;  // if 1 is before * then we have 9 cases
            else if(s[i-2]=='2')dp[i]=(dp[i]+6*dp[i-2])%mm; // if 2 is before * then we have 6 cases
            else if(s[i-2]=='*')dp[i]=(dp[i]+15*dp[i-2])%mm; // if * is before * then we have all 9+6 cases
                     }
        else {
            dp[i]=s[i-1]!='0'?dp[i-1]:0;
            if(s[i-2]=='1')dp[i]=(dp[i]+dp[i-2])%mm; //normalcase
            else if(s[i-2]=='2' && s[i-1]-'0'<7)dp[i]=(dp[i]+dp[i-2])%mm; //normalcase
            else if(s[i-2]=='*')dp[i]=(dp[i]+(s[i-1]-'0'<7 ? 2 :1)*dp[i-2])%mm;
        }
    }
    return (int)dp[n];
}
};
