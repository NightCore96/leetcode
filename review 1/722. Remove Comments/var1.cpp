class Solution {
private:
    char C_1 = '/';
    char C_2 = '*';
public:
    vector<string> removeComments(vector<string>& source)
    {
        
        vector<string> res;
        bool isCBlock = false;
        string temp ="";
        for(string line:source)
        {
            temp += readline(line,isCBlock);
            if(isCBlock)
            {
                continue;
            }
            if(!temp.empty())
            {
                res.push_back(temp);
            }
            temp = "";
        }
        return res;
    }
    
    string readline(string line,bool &isCBlock)
    {
        string res = "";
        for(int i=0;i<line.size();)
        {
            if(isCBlock)
            {
                if(line[i]==C_2&&i+1<line.size()&&line[i+1]==C_1)
                {
                    isCBlock=false;
                    i+=2;
                    continue;
                }
            }
            else
            {
                if(line[i]==C_1&&i+1<line.size())
                {
                    if(line[i+1]==C_1)
                    {
                        break;
                    }
                    else if (line[i+1]==C_2)
                    {
                        isCBlock=true;
                        i+=2;
                        continue;
                    }
                }
                res += line[i];
            }
            i++;
        }
        return res;
    }
};
