class Solution {
public:
    bool canConvert(string str1, string str2)
    {
        if(str1 == str2)
            return true;
        unordered_map<char,char> table;
        //unordered_set<char> r1(str1.begin(),str1.end());
        
        for(int i = 0 ; i < str1.size();i++)
        {
            if(table.count(str1[i]))
            {
                if(table[str1[i]] != str2[i])
                    return false;
            }
            else
            {
                table[str1[i]] = str2[i];
            }
        }
        unordered_set<char> r2(str2.begin(),str2.end());
        return r2.size() < 26;
    }
};
