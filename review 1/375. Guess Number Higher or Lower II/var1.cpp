class Solution {
public:
    int getMoneyAmount(int n)
    {
        vector<vector<int>> dp(n+1,vector<int>(n+1,0));
        return helper(1,n,dp);
    }
    
    int helper(int start , int end, vector<vector<int>>& dp)
    {
        if(start >= end)
            return 0;
        if(dp[start][end] > 0)
            return dp[start][end];
        int res = INT_MAX;
        for(int i = start; i <= end;i++)
        {
            int cost = i+ max(helper(start, i-1,dp),helper(i+1,end,dp));
            res = min(res,cost);
        }
        dp[start][end] = res;
        return res;
    }
};
