class Solution {
public:
    vector<int> smallestSufficientTeam(vector<string>& req_skills, vector<vector<string>>& people) {
        
        unordered_map<string, int> mp;
        int n = req_skills.size();
        int m = people.size();
        
        
        vector<vector<int> > dp(1<<n, vector<int>(1000));
        
        int skill[m];
        fill(skill, skill + m, 0);
        
        //base case
        dp[0].clear();
        
        
        // map strings to integers
        for(int i=0;i<n;i++)
            mp[req_skills[i]]=i;
        
        
        // constructing mask each person
        for(int i=0;i<m;i++)
        {
            for(auto x: people[i])
            {
                skill[i] |= 1<<mp[x];
            }
        }
        
        
        
        for(int i=1;i<1<<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                
                // checking if the current person is a subset of i
                if((skill[j]&i) != skill[j])
                    continue;
                
                int mask = skill[j];
                
                // iterating over each submask of current mask
                
                for(int sub = mask; sub>0; sub = (sub-1)&mask)
                {
                    
                    int temp = i&(~sub);
                     if(dp[i].size() > dp[temp].size() + 1)
                     {
                        dp[i] = dp[temp];
                        dp[i].push_back(j);
                     }
                }
                
            }
        
        }
        return dp[(1<<n)-1];
    }
};
