class Solution {
public:
    vector<int> diffWaysToCompute(string input)
    {
        return helper(input,0,input.size() - 1);
    }
    
    vector<int> helper(string& s,int l,int r)
    {
        vector<int> res;
        for(int i =l ;i<=r;i++)
        {
            char c = s[i];
            if(c == '+' || c == '-' || c == '*')
            {
                vector<int> lsum = helper(s,l,i-1);
                vector<int> rsum = helper(s,i+1,r);
                for(const auto& ls:lsum)
                {
                    for(const auto rs:rsum)
                    {
                        int temp = -1;
                        switch(c)
                        {
                            case '+':
                                temp = ls+rs;
                                break;
                            case '-':
                                temp = ls-rs;
                                break;
                            case '*':
                                temp = ls*rs;
                                break;
                        }
                        res.push_back(temp);
                    }
                }
            }
        }
        if(res.empty())
            res.push_back(stoi(s.substr(l,r-l+1)));
        return res;
    }
};
