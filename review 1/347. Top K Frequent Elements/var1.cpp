class Solution {
public:
    vector<int> topKFrequent(vector<int>& nums, int k) 
    {
        unordered_map<int,int> table;
        for(int n:nums)
        {
            if(table.count(n))
                table[n]++;
            else
                table[n] = 1;    
        }
        auto cmp = [](const pair<int,int>& a, const pair<int,int>& b) 
        {
            return a.second < b.second;
        };
        priority_queue<pair<int,int>, vector<pair<int,int>>, decltype(cmp)> maxHeap(cmp);
        for(auto p: table)
        {
            maxHeap.push(p);
        }
        vector<int> res;
        for(int i=0;i<k;i++)
        {
            res.push_back(maxHeap.top().first);
            maxHeap.pop();
        }
        return res;
    }
};