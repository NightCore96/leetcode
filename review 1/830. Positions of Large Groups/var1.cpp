class Solution {
public:
    vector<vector<int>> largeGroupPositions(string S)
    {
        int start = 0;
        int count = 1;
        vector<vector<int>> res;
        for(int i = 1;i<S.size();i++)
        {
            if(S[i] == S[i-1])
            {
                count++;
            }
            else
            {
                if(count >= 3)
                {
                    res.push_back({start,i-1});
                }
                count = 1;
                start = i;
            }
        }
        if(count >= 3)
        {
            int end = S.size() - 1;
            res.push_back({start,end});
        }
        return res;
    }
};
