/*
// Definition for Employee.
class Employee {
public:
    int id;
    int importance;
    vector<int> subordinates;
};
*/

class Solution {
public:
    int getImportance(vector<Employee*> employees, int id)
    {
        unordered_map<int,Employee*> list;
        for(auto& e:employees)
            list[e->id] = e;
        return dfs(list,id);
    }
    int dfs(unordered_map<int,Employee*>& list,int id)
    {
        int res = 0;
        if(!list.count(id))
            return res;
        res += list[id]->importance;
        if(!list[id]->subordinates.empty())
        {
            for(auto& i:list[id]->subordinates)
            {
                res += dfs(list,i);
            }
        }
        return res;
    }
};
