class Solution {
public:
    int minTransfers(vector<vector<int>>& transactions)
    {
        unordered_map<int,int> account;
        for(auto v:transactions)
        {
            account[v[1]] += v[2];
            account[v[0]] -= v[2];
        }
        vector<int> acc;
        int res = INT_MAX;
        for(auto p:account)
        {
            if(p.second != 0)
                acc.push_back(p.second);
        }
        dfs(acc,0,0,res);
        return res;
    }
    
    void dfs(vector<int>& acc,int start,int step,int &res)
    {
        while(start < acc.size() && acc[start] == 0)
            start++;
        if(start == acc.size())
        {
            res = min(res,step);
            return;
        }
        //cout<< start<<endl;
        for(int i = start + 1; i < acc.size() ; i++)
        {
            if((acc[start] > 0 && acc[i] < 0) || (acc[start] < 0 && acc[i] > 0))
            {
                acc[i] += acc[start];
                dfs(acc,start+1,step + 1,res);
                acc[i] -= acc[start];
            }
        }
    }
};
