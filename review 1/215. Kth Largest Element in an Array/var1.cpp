class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) 
    {
        int n = nums.size();
        int start = 0, end = n-1,mid;
        k = n-k;
        while(start<=end)
        {
            mid = partions(nums ,start ,end);
            if(mid == k)
                break;
            else if(mid > k)
                end = mid-1;
            else
                start = mid+1;
        }
        return nums[k];
    }
    
    int partions(vector<int>& nums ,int start ,int end)
    {
        int r = rand()%(end - start + 1) +start;
        int p = nums[r];
        int res = start;
        swap(nums[end],nums[r]);
        for(int i=start;i<end;i++)
        {
            if(nums[i]<=p)
            {
                swap(nums[i],nums[res]);
                res++;
            }
        }
        swap(nums[end],nums[res]);
        return res;
    }
};