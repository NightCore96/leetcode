class Solution {
public:
    vector<int> findAnagrams(string s, string p)
    {
        int ptable[26] = {0};
        int stable[26] = {0};
        int psize = p.size();
        int start = 0;
        vector<int> res;
        if(s.size() < p.size())
            return res;
        for(const auto& c:p)
        {
            int i = c - 'a';
            //cout<<c<<" "<< i << endl;
            ptable[i]++;
        }
        
        for(int i = 0 ; i < s.size() ;i++)
        {
            stable[s[i] - 'a']++;
            if(i < psize - 1)
            {
                continue;
            }
            bool isfind = true;
            for(int j =0;j<26;j++)
            {
                if(ptable[j]!=stable[j])
                {
                    isfind =false;
                    break;
                }
            }
            if(isfind)
                res.push_back(i - psize + 1);
            stable[s[start] - 'a'] --;
            start++;
        }
        return res;
    }
};
