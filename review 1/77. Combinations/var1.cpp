class Solution {
public:
    vector<vector<int>> combine(int n, int k) 
    {
        vector<vector<int>> res;
    
        vector<int> now;
        find(res,now,0,n,k);
        return res;
    }
    
    void find(vector<vector<int>> &res,vector<int> &now,int c,int &n,int &k)
    {
        
        if(now.size()==k)
        {
            res.push_back(now);
            return ;
        }
        for(int i=c+1;i<=n;i++)
        {
            now.push_back(i);
            find(res,now,i,n,k);
            now.pop_back();
        }
    }
    
};