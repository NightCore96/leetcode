class Solution {
public:
    bool isNumber(string s) 
    {
        if(s.empty())
            return false;
        int i = 0;
        int len = s.size();
        while(s[i] == ' ')
            i++;
        if(s[i] == '+'||s[i] == '-')
            i++;
        if(s[i] == '.')
        {
            i++;
            if(!isdigit(s[i]))
                return false;
            i++;
        }
        else if(isdigit(s[i]))
        {
            while(isdigit(s[i]))
                i++;
            if(s[i] == '.')
                i++;
        }
        else
            return false;
        while(isdigit(s[i]))
                i++;
        if(s[i] == 'e')
        {
            i++;
            if(s[i] == '+'||s[i] == '-')
                i++;
            if(!isdigit(s[i]))
                return false;
            while(isdigit(s[i]))
                i++;
        }
        while(s[i] == ' ')
            i++;
        return i == len;
    }
};