class Solution {
public:
    string findReplaceString(string S, vector<int>& indexes, vector<string>& sources, vector<string>& targets)
    {
        string res = S;
        int shift = 0;
        map<int,int> index;
        for(int i = 0;i< indexes.size();i++)
        {
            if(isSame(S,sources[i],indexes[i]))
            {
                index[indexes[i]] = i;
            }
        }
        for(auto& p:index)
        {
            int id = p.first;
            int t_len = targets[p.second].size();
            int s_len = sources[p.second].size();
            res.replace(id+shift,s_len,targets[p.second]);
            shift += t_len - s_len;
        }
        return res;
    }
    
    bool isSame(string& s,string& t,int& p)
    {
        if(p+t.size()-1 >= s.size())
            return false;
        for(int i=0;i<t.size();i++)
        {
            if(s[i+p] != t[i])
                return false;
        }
        return true;
    }
};
