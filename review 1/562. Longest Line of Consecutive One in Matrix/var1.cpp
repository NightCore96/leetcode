class Solution {
public:
    int longestLine(vector<vector<int>>& M)
    {
        if(M.empty())
            return 0;
        int res = 0;
        vector<vector<int>> dp1(M.size(),vector<int>(M[0].size(),0));
        vector<vector<int>> dp2(M.size(),vector<int>(M[0].size(),0));
        vector<vector<int>> dp3(M.size(),vector<int>(M[0].size(),0));
        vector<vector<int>> dp4(M.size(),vector<int>(M[0].size(),0));
        for(int i = 0; i < M.size();i++)
        {
            for(int j = 0; j < M[0].size(); j++)
            {
                if(M[i][j] == 1)
                {
                    dp1[i][j] = i > 0 ? 1 + dp1[i-1][j] : 1;
                    dp2[i][j] = j > 0 ? 1 + dp2[i][j-1] : 1;
                    dp3[i][j] = i > 0 && j > 0 ? 1 + dp3[i-1][j-1] : 1;
                    dp4[i][j] = i > 0 && j < (M[0].size()-1)  ? 1 + dp4[i-1][j+1] : 1;
                    
                    res = max(res,dp1[i][j]);
                    res = max(res,dp2[i][j]);
                    res = max(res,dp3[i][j]);
                    res = max(res,dp4[i][j]);
                }
            }
        }
        return res;
    }
};
