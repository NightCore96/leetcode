class Solution {
public:
    int missingElement(vector<int>& nums, int k)
    {
        for(int i = 0; i < nums.size() - 1;i++)
        {
            int diff = nums[i+1] - nums[i] - 1;
            if(k <= diff)
                return nums[i] + k;
            k -= diff;
        }
        return nums.back() + k;
    }
};
