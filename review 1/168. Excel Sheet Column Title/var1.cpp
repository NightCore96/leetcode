class Solution {
public:
    string convertToTitle(int n)
    {
        string res = "";
        int s = (n-1)/26;
        int t = (n-1)%26;
        res = t + 'A';
        if(s == 0)
        {
            return res;
        }
        else
        {
            return convertToTitle(s)+res;
        }
    }
};

