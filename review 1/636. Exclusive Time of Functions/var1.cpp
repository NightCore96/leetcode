class Solution {
public:
    vector<int> exclusiveTime(int n, vector<string>& logs)
    {
        if(n == 0 )
            return {};
        vector<int> res(n,0);
        stack<pair<int,int>> stk;
        for(auto& log:logs)
        {
            int i = log.find_first_of(':');
            int j = log.find_last_of(':');
            int id = stoi(log.substr(0,i));
            string action = log.substr(i+1, j - i - 1);
            int time = stoi(log.substr(j+1));
            //cout<<id << " "<< timestamp << " " <<action<<endl;
            if(action == "start")
            {
                if(!stk.empty())
                {
                    auto p = stk.top();
                    res[p.first] += time - p.second;
                }
                stk.push({id,time});
            }
            else
            {
                auto p = stk.top();
                res[p.first] += time - p.second + 1;
                stk.pop();
                if(!stk.empty())
                {
                    stk.top().second =  time + 1;
                }
            }
        }
        return res;
    }
};
