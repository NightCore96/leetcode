/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isCompleteTree(TreeNode* root)
    {
        if(root == nullptr)
            return false;
        queue<pair<TreeNode*,int>> q;
        q.push({root,1});
        int count = 1;
        while(!q.empty())
        {
            auto p = q.front();
            if(p.second != count)
                return false;
            count++;
            q.pop();
            if(p.first->left)
            {
                q.push({p.first->left,p.second*2});
            }
            if(p.first->right)
            {
                q.push({p.first->right,p.second*2 + 1});
            }
        }
        return true;
    }
};
