class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) 
    {
        vector<vector<int>> res;
        if(intervals.empty())
        {
            res.push_back(newInterval);
            return res;
        }
        map<int,int> count;
        for(auto v:intervals)
        {
            count[v[0]]++;
            count[v[1]]--;
        }
        count[newInterval[0]]++;
        count[newInterval[1]]--;
        int c = 0;
        int start = INT_MIN;
        int end = INT_MIN;
        for(auto p:count)
        {
            c += p.second;
            if(start == INT_MIN)
            {
                start = p.first;
                
            }
            end = p.first;
            if(c == 0)
            {
                c = 0;
                res.push_back({start,end});
                start = INT_MIN;
                end = INT_MIN;
            }
        }
        return res;
    }
};