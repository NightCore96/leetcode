/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void recoverTree(TreeNode* root) 
    {
        TreeNode* swap1 = nullptr;
        TreeNode* swap2 = nullptr;
        TreeNode* pre = new TreeNode(INT_MIN);
        TreeNode* node = root;
        while(node)
        {
            if(!node->left)
            {
                if(pre->val > node->val)
                {
                    if(!swap1)
                        swap1 = pre;
                    if(swap1)
                        swap2 = node;
                }
                pre = node;
                node = node->right;
            }
            else
            {
                TreeNode* pretemp = node->left;
                while(pretemp->right && pretemp->right != node)
                  pretemp = pretemp->right;
                if(!pretemp->right)
                {
                    pretemp->right = node;
                    node = node->left;
                }
                else
                {
                    if(pre->val > node->val)
                    {
                        if(!swap1)
                            swap1 = pre;
                        if(swap1)
                            swap2 = node;
                    }
                    pre = node;
                    pretemp->right = nullptr;
                    node = node->right;
                }
            }
        }
        swap (swap1->val,swap2->val);
    }
};