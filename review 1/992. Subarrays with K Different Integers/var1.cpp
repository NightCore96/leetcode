 class Solution {
 public:
     int subarraysWithKDistinct(vector<int>& A, int K)
     {
         int res = 0;
         unordered_map<int,int> table;
         int end1 = 0;
         for(int i = 0;i < A.size()-K + 1;i++)
         {
             if( i > 0)
             {
                 table[A[i-1]]--;
                 if(table[A[i-1]] == 0)
                     table.erase(A[i-1]);
             }
             int end2 = 0;
             while(end1 < A.size() && table.size() < K)
             {
                 table[A[end1]]++;
                 end1++;
             }
             end2 = end1;
             while(end2 < A.size() && table.count(A[end2]))
             {
                 end2++;
             }
             //cout <<i<<"<"<< end1 <<" " <<end2 <<endl;
             if(table.size() == K)
             {
                 res += (end2 - end1 + 1);
             }
         }
         return res;
     }
 };
