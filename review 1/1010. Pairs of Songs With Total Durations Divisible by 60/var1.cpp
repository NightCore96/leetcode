class Solution {
public:
    int numPairsDivisibleBy60(vector<int>& time)
    {
        unordered_map<int,int> hash;
        int res = 0;
        //hash[60] = 1;
        for(const auto& t:time)
        {
            int l = 60 - (t % 60);
            //cout << l  << " " << t % 60 <<endl;
        
            if(hash.count(l))
            {
                res += hash[l];
            }
            if( l == 60)
                hash[l]++;
            else
                hash[t%60]++;
        }
        return res;
    }
};
