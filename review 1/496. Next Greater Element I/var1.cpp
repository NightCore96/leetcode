class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2)
    {
        vector<int> res;
        unordered_map<int,int> list;
        stack<int> stk;
        for(int i = nums2.size() - 1 ; i >= 0 ;i--)
        {
            while(!stk.empty() && nums2[i] >= stk.top())
                stk.pop();
            if(stk.empty())
                list[nums2[i]] = -1;
            else
                list[nums2[i]] = stk.top();
            stk.push(nums2[i]);
        }
        for(int& i: nums1)
        {
            res.push_back(list[i]);
        }
        return res;
    }
};
