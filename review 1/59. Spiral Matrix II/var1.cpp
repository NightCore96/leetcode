class Solution {
public:
    vector<vector<int>> generateMatrix(int n) 
    {
        if(n < 1)
            return {};
        vector<vector<int>> res(n,vector<int>(n,0));        
        int lu = 0;
        int ld = 0;
        int rd = n-1;
        int ru = n-1;
        int count = 0;
        while(lu <= ru && ld <= rd)
        {
            for(int i=lu;i<=ru;i++)
                res[lu][i] = ++count;
            for(int i=lu+1;i<=rd;i++)
                res[i][ru] = ++count;
            for(int i=ru-1;rd!=ld&&i>=ld;i--)
                res[rd][i] = ++count;
            for(int i=rd-1;ru!=lu&&i>lu;i--)
                res[i][ld] = ++count;
            lu++;
            ru--;
            ld++;
            rd--;
        }
        return res;
    }
};