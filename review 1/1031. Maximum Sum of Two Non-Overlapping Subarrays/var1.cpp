class Solution {
public:
    int maxSumTwoNoOverlap(vector<int>& A, int L, int M)
    {
        return max(helper(A,L,M),helper(A,M,L));
    }
    
    int helper(vector<int>& A, int& L, int& M)
    {
        int sumL = 0,sumM = 0;
        int mx = 0;
        int res = 0;
        for(int i =0;i<A.size();i++)
        {
            sumL += A[i];
            if(i >= L)
            {
                sumL -= A[i-L];
                sumM += A[i-L];
            }
            if(i >= L+M)
            {
                sumM -= A[i-L-M];
            }
            mx = max(mx,sumM);
            //cout << sumL << " " << mx <<endl;
            res = max(res,sumL+mx);
        }
        return res;
    }
};
