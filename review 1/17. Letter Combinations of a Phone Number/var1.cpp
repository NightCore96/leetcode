class Solution {
public:
    vector<string> letterCombinations(string digits) 
    {
        vector<string> letter = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        vector<string> res;
        if(digits=="")
            return {};
        string now = "";
        helper(res,letter,digits,0,now);
        return res;
    }
    
    void helper(vector<string> &res,vector<string> &letter,string &digits,int index,string &now)
    {
        if(index == digits.size())
        {
            res.push_back(now);
            return;
        }
        int i = digits[index]-'0';
        for(char c:letter[i])
        {
            now+=c;
            helper(res,letter,digits,index+1,now);
            now.erase(now.size()-1,1);
        }
    }
};