class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int S)
    {
        int sum = 0;
        for(int i: nums)
            sum += i;
        if(sum < S || (S + sum) % 2 ==1)
            return 0;
        int target = (S + sum) / 2;
        //cout<<target<<endl;
        vector<int> dp(target + 1, 0);
            dp[0] = 1;
        for(int x : nums)
        {
            for(int i = target ; i >= x ; i--)
            {
                dp[i] += dp[i - x];
            }
        }
        return dp[target];
    }
};
