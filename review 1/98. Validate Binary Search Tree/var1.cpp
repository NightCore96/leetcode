/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) 
    {
        if(!root)
            return true;
        vector<int> num;
        sort(root,num);
        int size = num.size();
        for(int i = 0;i <size - 1;i++)
        {
            if(num.at(i) >= num.at(i+1))
            {
                return false;    
            }
        }
        return true;
    }
    void sort(TreeNode* root,vector<int> &num)
    {
        if(!root)
            return;
        sort(root->left,num);
        num.push_back(root->val);
        sort(root->right,num);
    }
};