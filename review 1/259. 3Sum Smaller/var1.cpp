class Solution {
public:
    int threeSumSmaller(vector<int>& nums, int target)
    {
        int size = nums.size();
        if(size < 3)
            return 0;
        int res = 0;
        sort(nums.begin(),nums.end());
        for(int i = 0;i < size-2 ;i++)
        {
            int x = nums[i];
            int j = i + 1;
            int k = size - 1;
            while(j < k)
            {
                int s = x + nums[j] + nums[k] ;
                if( s < target)
                {
                    res += k -j;
                    j++;
                }
                else
                {
                    k--;
                }
            }
        }
        return res;
    }
};
