class Solution {
public:
    int isFound(string &s,int start,int end)
    {
        for(int i=start;i<end;i++)
        {
            if(s[i]==s[end])
                return i;
        }
        return -1;
    }
    int lengthOfLongestSubstring(string s) 
    {
        int n = s.size();
        int res = 0;
        int start = 0;
        for(int i=1;i<n;i++)
        {
            int f = isFound(s,start,i);
            if(f!=-1)
            {
                res = max(res,i-start);
                start = f+1;
            }
        }
            
        return max(res,n-start);
    }
};