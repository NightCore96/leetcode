class Solution {
private:
    deque<unordered_map<string,int>> table;
public:
    int evaluate(string expression) {
        table.clear();
        int pos =0;
        return Parse(expression,pos);
    }
private:
    int Parse(string e,int& pos)
    {
        int res = 0;
        table.push_front(unordered_map<string,int>());
        if(e[pos]=='(')
            pos++;
        string token  = nextToken(e,pos);
        if(token == "add")
        {
            int v1 = Parse(e,++pos);
            int v2 = Parse(e,++pos);
            res = v1 + v2;
        }
        else if(token == "mult")
        {
            int v1 = Parse(e,++pos);
            int v2 = Parse(e,++pos);
            res = v1 * v2;
        }
        else if(token == "let")
        {
            string r;
            while(e[pos]!=')')
            {
                ++pos;
                if(e[pos]=='(')
                {
                    res = Parse(e,++pos);
                    break;
                }
                r = nextToken(e,pos);
                if(e[pos]==')')
                {
                    if(isalpha(r[0]))
                    {
                        res = findSymbol(r);
                    }
                    else
                    {
                        res = stoi(r);
                    }
                    break;
                }
                res = table.front()[r]= Parse(e,++pos);
            }
        }
        else if(isalpha(token[0]))
        {
            res = findSymbol(token);
        }
        else
        {
            res = stoi(token);
        }
        if(e[pos]==')')
            pos++;
        table.pop_front();

        return res;
    }
    
    int findSymbol(string s)
    {
        for(auto u:table)
        {
            if(u.count(s))
                return u.at(s);
        }
        return 0;
    }
    
    string nextToken(string e,int& pos)
    {
        string res="";
        while(pos<e.size())
        {
            if(e[pos] == ')'||e[pos] == ' ')
                break;
            res += e[pos];
            pos++;
        }
        return res;
    }
};
