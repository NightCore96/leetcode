class Solution {
public:
    int compareVersion(string version1, string version2)
    {
        int i1 = 0;
        int i2 = 0;
        while(i1 < version1.size() && i2 < version2.size())
        {
            int v1 = 0;
            int v2 = 0;
            while(i1<version1.size() &&version1[i1] != '.')
            {
                v1 *=10;
                v1 += (version1[i1] - '0');
                i1++;
            }
            while(i2<version2.size() &&version2[i2] != '.')
            {
                v2 *=10;
                v2 += (version2[i2] - '0');
                i2++;
            }
            if(v1 > v2)
                return 1;
            if(v1 < v2)
                return -1;
            i1++;
            i2++;
        }
        if(i1 < version1.size())
        {
           for(;i1<version1.size();i1++)
           {
               if(version1[i1] != '0' && version1[i1] != '.')
                   return 1;
           }
        }
        if(i2 < version2.size())
        {
            for(;i2<version2.size();i2++)
           {
               if(version2[i2] != '0' && version2[i2] != '.')
                   return -1;
           }
        }
            
        return 0;
    }
};
