/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<TreeNode*> findDuplicateSubtrees(TreeNode* root)
    {
        vector<TreeNode*> res;
        unordered_map<string,int> table;
        helper(res,root,table);
        return res;
    }
    
    string helper(vector<TreeNode*>& res,TreeNode* root,unordered_map<string,int>& table)
    {
        if(root == nullptr)
            return "X";
        string s = to_string(root->val) +","+helper(res,root->left,table)+","+helper(res,root->right,table);
        if(table[s] == 1)
            res.push_back(root);
        table[s]++;
        return s;
    }
};
