class Solution {
public:
    vector<vector<string>> findDuplicate(vector<string>& paths)
    {
        vector<vector<string>> res;
        unordered_map<string,vector<string>> hash;
        for(string& p:paths)
        {
            int index =p.find_first_of(' ');
            string dir = p.substr(0,index);
            string files = p.substr(index+1);
            while(!files.empty() && index != string::npos)
            {
                index =files.find_first_of(' ');
                
                string f = files.substr(0,index);
                files = files.substr(index+1);
                
                auto left = f.find_first_of('(');
                auto right = f.find_first_of(')');
                string content = f.substr(left+1, right - left - 1);
                string name = f.substr(0,left);
                hash[content].push_back(dir + "/"+ name);
            }
        }
        
        for(auto& p: hash)
        {
            if(p.second.size() > 1)
                res.push_back(p.second);
        }
            
        return res;
    }
};
