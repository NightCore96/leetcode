/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) 
    {
        vector<int> res;
        inorder_iteratively(root, res);
        return res;
    }
    
    void inorder_recursive(TreeNode* root, vector<int> &res)
    {
        if(!root)
            return;
        inorder_recursive(root->left, res);
        res.push_back(root->val);
        inorder_recursive(root->right, res);
    }
    
    void inorder_iteratively(TreeNode* root, vector<int> &res)
    {
        if(!root)
            return;
        stack<TreeNode*> stack;
        TreeNode* cur = root;
        while(!stack.empty()||cur)
        {
            if(cur)
            {
                stack.push(cur);
                cur = cur->left;
            }
            else
            {
                res.push_back(stack.top()->val);
                cur = stack.top()->right;
                stack.pop();
            }
        }
    }
};