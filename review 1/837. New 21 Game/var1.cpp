class Solution {
public:
    double new21Game(int N, int K, int W)
    {
        if(K==0||N>K+W-1)
            return 1.0;
        vector<double> dp(N+1,0.0);
        dp[0]=1.0;
        double Win = 1.0;
        double res = 0.0;
        for(int i=1;i<=N;i++)
        {
            dp[i]=Win/W;
            if(i<K)
                Win+=dp[i];
            else
                res+=dp[i];
            if(i-W>=0)
                Win -= dp[i - W];
        }
        return res;
    }
};
