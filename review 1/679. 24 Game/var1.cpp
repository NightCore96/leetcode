class Solution {
public:
    bool judgePoint24(vector<int>& nums)
    {
        vector<double> temp(nums.begin(),nums.end());
        
        return find(temp);
    }
    
    bool find(vector<double>& nums)
    {
        if(nums.size()==1)
            return (abs(nums[0]-24)<0.0001);
        for(int i=0;i<nums.size();i++)
        {
            for(int j=0;j<nums.size();j++)
            {
                if(i==j)
                    continue;
                vector<double> temp;
                for(int k=0;k<nums.size();k++)
                {
                    if (k != i && k != j)
                        temp.push_back(nums[k]);
                }
                double a = nums[i];
                double b = nums[j];
                for(int o=0;o<4;o++)
                {
                    if((o==0||o==2)&&i>j)
                        continue;
                    if(o==3&&b<0.0001)
                        continue;
                    switch(o)
                    {
                        case 0:
                        {
                            temp.push_back(a+b);
                            break;
                        }
                        case 1:
                        {
                            temp.push_back(a-b);
                            break;
                        }
                        case 2:
                        {
                            temp.push_back(a*b);
                            break;
                        }
                        case 3:
                        {
                            temp.push_back(a/b);
                            break;
                        }
                    }
                    if(find(temp))
                        return true;
                    temp.pop_back();
                }
                    
            }
        }
        return false;
    }
    
};
