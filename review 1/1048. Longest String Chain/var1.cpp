class Solution {
public:
    int longestStrChain(vector<string>& words)
    {
        unordered_map<int,unordered_map<string,int>> table;
        sort(words.begin(),words.end(),[](const string& a,string& b)
             {
                 return a.size() < b.size();
             });
        for(int i = 0;i < words.size();i++)
        {
           string& s = words[i];
           table[s.size()].insert({s,i});
        }
        vector<int> dp(words.size(),1);
        for(int i = 0 ; i < words.size();i++)
        {
            int len = words[i].size();
            if(!table.count(len-1))
                continue;
            for(int j = 0;j < len;j++)
            {
                string temp = words[i];
                temp.erase(j,1);
                cout<< temp<<endl;
                //cout <<words[i]<<" "<< temp <<endl;
                if(table[len-1].count(temp))
                {
                   //cout << dp[i] <<" " << dp[table[len-1][temp]] <<endl;
                   dp[i] = max(dp[i],dp[table[len-1][temp]] + 1);
                }
            }
        }
        int res = 0;
        for(const auto& i:dp)
        {
            res = max(res,i);
        }
        return res;
    }
};
