class NumArray
{
private:
    vector<int> buffer;
    vector<int> sums;
public:
    NumArray(vector<int>& nums)
    {
        buffer = nums;
        int sum = 0;
        sums.push_back(sum);
        for(auto& i:buffer)
        {
            sum += i;
            sums.push_back(sum);
        }
    }
    
    void update(int i, int val)
    {
        int diff = buffer[i] - val;
        buffer[i] = val;
        for(int j = i + 1;j<sums.size();j++)
        {
            sums[j] -= diff;
        }
        
    }
    
    int sumRange(int i, int j)
    {
        return sums[j + 1] - sums[i];
    }
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * obj->update(i,val);
 * int param_2 = obj->sumRange(i,j);
 */
