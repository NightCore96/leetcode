class Solution {
public:
    int findMin(vector<int>& nums)
    {
        int s = 0;
        int e = nums.size()-1;
        while(s<e)
        {
            int mid = s+ (e - s)/2;
            //cout<<s<<" "<<e<<" "<<mid<<endl;
            if(nums[e] < nums[mid])
                s = mid+1;
            else
                e = mid;
        }
        return nums[e];
    }
};
