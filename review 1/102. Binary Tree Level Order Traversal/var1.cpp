/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode* root) 
    {
        vector<vector<int>> res;
        if(!root)
            return res;
        vector<int> buffer;
        queue<pair<TreeNode*,int>> q;
        q.push({root,0});
        while(!q.empty())
        {
            auto p = q.front();
            q.pop();
            if(p.second>res.size())
            {
                res.push_back(buffer);
                buffer.clear();
            }
            buffer.push_back(p.first->val);
            if(p.first->left)
                q.push({p.first->left,p.second+1});
            if(p.first->right)
                q.push({p.first->right,p.second+1});
        }
        res.push_back(buffer);
        return res;
    }
};