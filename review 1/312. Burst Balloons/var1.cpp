class Solution {
public:
    int maxCoins(vector<int>& nums) 
    {
        if(nums.empty())
            return 0;
        int size = nums.size();
        nums.push_back(1);
        nums.insert(nums.begin(),1);
        vector< vector<int>> dp(size+2, vector<int>(size+2,0));
        for(int i = 1; i <= size; i++) //length
        {
            for(int L = 1 ; L <= size - i + 1 ; L++) // start;
            {
                int R = L + i - 1; //end;
                for(int c = L ;  c <=R ; c++)
                {
                    dp[L][R] = max(dp[L][R],nums[L-1]*nums[c]*nums[R+1] + dp[L][c -1 ] + dp[c +1][R]);
                }
            }
        }
        return dp[1][size];
    }
};