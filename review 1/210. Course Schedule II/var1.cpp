class Solution {
public:
    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites)
    {
        unordered_map<int,unordered_set<int>> graph;
        for(auto p :prerequisites)
        {
            graph[p[0]].insert(p[1]);
        }
        vector<int> res;
        unordered_set<int> visit;
        unordered_set<int> OK;
        for(int i = 0; i < numCourses;i++)
        {
            if(!helper(i,numCourses,graph,res,visit,OK))
            {
                res.clear();
                return res;
            }
        }
        return res;
    }
    
    bool helper(int now,int numC, unordered_map<int,unordered_set<int>> &graph, vector<int> &res,unordered_set<int> &visit,unordered_set<int> &OK)
    {
        if(res.size() == numC)
            return true;
        else if(OK.count(now))
            return true;
        else if(visit.count(now))
            return false;
        visit.insert(now);
        for(auto c: graph[now])
        {
            if(!helper(c,numC,graph,res,visit,OK))
                return false;
        }
        res.push_back(now);
        OK.insert(now);
        return true;
    }
};
