class Solution {
public:
    int minDistance(string word1, string word2) 
    {
        if(word1.empty())
            return word2.size();
        if(word2.empty())
            return word1.size();
        int M = word1.size()+1;
        int N = word2.size()+1;
        vector<vector<int>> dp(M,vector(N,0));
        for(int i=0;i<M;i++)
        {
            dp[i][0]=i;
        }
        for(int i=0;i<N;i++)
        {
            dp[0][i]=i;
        }
        for(int i=1;i<M;i++)
        {
            for(int j=1;j<N;j++)
            {
                dp[i][j]=dp[i-1][j-1];
                if(word1[i-1]!=word2[j-1])
                    dp[i][j] = min(dp[i-1][j-1],min(dp[i][j-1],dp[i-1][j]))+1;
            }
        }
        return dp[M-1][N-1];
    }
};