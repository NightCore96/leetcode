class Solution {
public:
    int rectangleArea(vector<vector<int>>& rectangles)
    {
        const int MOD = 1000000007;
        unordered_set<int> setX, setY;
        vector<int> Xset, Yset;
        for(auto v:rectangles)
        {
            setX.insert(v[0]);
            setX.insert(v[2]);
            setY.insert(v[1]);
            setY.insert(v[3]);
        }
        for(int i:setX)
            Xset.push_back(i);
        for(int i:setY)
            Yset.push_back(i);
        sort(Xset.begin(),Xset.end());
        sort(Yset.begin(),Yset.end());
        
        unordered_map<int,int> Xmap,Ymap;
        for(int i=0;i<Xset.size();i++)
            Xmap[Xset[i]]=i;
        for(int i=0;i<Yset.size();i++)
            Ymap[Yset[i]]=i;
        
        vector<vector<bool>> grid(Xset.size(),vector<bool>(Yset.size(),false));
        
        for(auto v:rectangles)
        {
            int X1 = Xmap[v[0]];
            int Y1 = Ymap[v[1]];
            int X2 = Xmap[v[2]];
            int Y2 = Ymap[v[3]];
            for(int i=X1;i<X2;i++)
            {
                for(int j=Y1;j<Y2;j++)
                {
                    grid[i][j]=true;
                }
            }
        }
        
        long long res = 0;
        for(int i=0;i<grid.size();i++)
        {
            for(int j=0;j<grid[0].size();j++)
            {
                if(grid[i][j])
                {
                    res +=(long long)(Xset[i+1]-Xset[i])*(long long)(Yset[j+1]-Yset[j]);
                    res %=MOD;
                }
            }
        }
        return res;
    }
};
