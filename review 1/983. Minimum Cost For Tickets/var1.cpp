 class Solution {
 public:
     int mincostTickets(vector<int>& days, vector<int>& costs)
     {
         unordered_set<int> hash(days.begin(),days.end());
         int finalday = days.back();
         vector<int> dp(finalday+1,0);
         for(int i = 1;i<dp.size();i++)
         {
             if(hash.count(i))
             {
                 dp[i] = costs[0] + dp[i-1];
                 dp[i] = min(dp[i],costs[1] + dp[max(0,i-7)]);
                 dp[i] = min(dp[i],costs[2] + dp[max(0,i-30)]);
             }
             else
             {
                 dp[i] = dp[i-1];
             }
         }
         return dp[finalday];
     }
 };
