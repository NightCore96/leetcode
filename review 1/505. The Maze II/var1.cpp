class Solution {
private:
    vector<pair<int,int>> dist = {{0,1},{1,0},{-1,0},{0,-1}};
public:
    int shortestDistance(vector<vector<int>>& maze, vector<int>& start, vector<int>& destination)
    {
        int M = maze.size();
        int N = maze[0].size();
        vector<vector<int>> path(maze.size(),vector<int>(maze[0].size(),INT_MAX));
        queue<pair<int,int>> q;
        q.push({start[0],start[1]});
        path[start[0]][start[1]] = 0;
        while(!q.empty())
        {
            pair<int,int> p = q.front();
            q.pop();
            for(auto d:dist)
            {
                int x = p.first;
                int y = p.second;
                int cur = path[x][y];
                while(x>=0&&y>=0&&x<M&&y<N&&maze[x][y]==0)
                {
                    x+=d.first;
                    y+=d.second;
                    cur++;
                }
                x-=d.first;
                y-=d.second;
                cur--;
                if(path[x][y]>cur)
                {
                    path[x][y]=cur;
                    if(x!=destination[0]||y!=destination[1])
                    {
                        q.push({x,y});
                    }
                }
            }
        }
        
        return path[destination[0]][destination[1]]==INT_MAX?-1:path[destination[0]][destination[1]];
    }
};
