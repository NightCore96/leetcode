class Solution {
public:
    vector<int> findSubstring(string s, vector<string>& words) 
    {
        vector<int> res;
        if(s.empty()||words.empty())
            return res;
        int size = s.size();
        int num = words.size();
        int len = words[0].size();
        int total = num*len;
        if(total > size)
            return res;
        unordered_map<string,int> table,curr;
        for(auto &a: words)
        {
            table[a]++;
        }
        for(int i = 0; i < size - total + 1 ; i++)
        {
            bool isOK = true;
            curr.clear();
            //unordered_map<string,int> curr = table;
            string sub = s.substr(i,total);
            for(int j = 0;j < total ; j+= len)
            {
                string str = sub.substr(j,len);
                if(table.count(str))
                {
                    curr[str]++;
                    if(curr[str] > table[str])
                    {
                        isOK = false;
                        break;
                    }
                }
                else
                {
                    isOK = false;
                    break;
                }
            }
            if(isOK)
                res.push_back(i);
        }
        return res;
    }
};