class Solution {
public:
    int maxProduct(vector<int>& nums) 
    {
        if(nums.empty())
            return 0;
        int pMax = 1,pMin = 1;
        int res = INT_MIN;
        for(int i:nums)
        {
            int tMax = pMax;
            int tMin = pMin;
            pMax = max(max(tMax*i,tMin*i),i);
            pMin = min(min(tMax*i,tMin*i),i);
            res = max(res,pMax);
        }
        return res;
    }
};