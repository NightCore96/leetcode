class Solution {
public:
    int minRefuelStops(int target, int startFuel, vector<vector<int>>& stations)
    {
        int res = 0;
        int now = startFuel;
        stations.push_back({target,0});
        priority_queue<int> pq;
        for(auto& s:stations)
        {
            int mile = s[0];
            int oil = s[1];
            if( mile > now)
            {
                while(!pq.empty())
                {
                    now += pq.top();
                    pq.pop();
                    res++;
                    if(mile<=now)
                        break;
                }
                if(mile > now)
                {
                    return -1;
                }
            }
            pq.push(oil);
        }
        return res;
    }
};
