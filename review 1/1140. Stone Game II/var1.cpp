class Solution {
public:
    int dfs(vector<vector<int>>& dp,vector<int>& resum,int& N,int i,int M)
    {
        if(i >= N) // over piles size
            return 0;
        if(dp[i][M] >= 0) // record pre-compute
            return dp[i][M];
        int minv = -1;
        for(int j = 1;j <= 2*M;j++)
        {
            if(j + i > N)
                break;
            int temp = dfs(dp,resum,N,j+i,max(j,M));
            if(minv < 0 || temp < minv)
                minv = temp;
        }
        int res = resum[i] - minv;
        dp[i][M] = res;
        return res;
    }
    
    int stoneGameII(vector<int>& piles)
    {
        int N = piles.size();
        vector<vector<int>> dp(N,vector<int>(N*2,-1));
        vector<int> resum(N+1,0);
        int sum = 0;
        for(int i = N-1; i >= 0;i--)
        {
            sum += piles[i];
            resum[i] = sum;
        }
        
        return dfs(dp,resum,N,0,1);
    }
};
