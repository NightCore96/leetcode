class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums)
    {
        vector<string> res;
        int start;
        int end;
        if(nums.empty())
            return res;
        start = nums[0];
        end = nums[0];
        for(int i = 1;i < nums.size();i++)
        {
            if(nums[i] == nums[i-1]+1)
            {
                end = nums[i];
            }
            else
            {
                if(start == end)
                    res.push_back(to_string(start));
                else
                    res.push_back(to_string(start) + "->" + to_string(end));
                end = nums[i];
                start = nums[i];
            }
        }
        if(start == end)
            res.push_back(to_string(start));
        else
            res.push_back(to_string(start) + "->" + to_string(end));
        return res;
    }
};
