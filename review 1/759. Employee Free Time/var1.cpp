/*
// Definition for an Interval.
class Interval {
public:
    int start;
    int end;

    Interval() {}

    Interval(int _start, int _end) {
        start = _start;
        end = _end;
    }
};
*/

class Solution {
public:
    vector<Interval> employeeFreeTime(vector<vector<Interval>> schedule)
    {
        map<int,int> time;
        for(auto& v:schedule)
        {
            for(auto& i:v)
            {
                auto it = time.lower_bound(i.start);
                if(it == time.end()) // not found
                {
                    if(time.begin()->first < i.end)
                        time.begin()->first = i.start;
                    else
                        time[i.start] = i.end;
                    continue;
                }
                bool newtime = true;
                if(it != time.begin() && --it->second > i.start)
                {
                    --it->second = max(i.start,--it->second);
                    newtime =false;
                }
                if(i.end > it->first)
                {
                    it->first = i.start;
                    it->second = max(it->second,i.end)
                    newtime = false;
                }
                if(newtime)
                {
                    time[i.start] = i.end;
                }
                else if(it != time.begin())
                {
                    
                }
            }
        }
    }
};
