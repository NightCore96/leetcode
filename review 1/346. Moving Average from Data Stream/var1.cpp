class MovingAverage
{
private:
    int m_size;
    queue<int> m_queue;
    int sum;
public:
    /** Initialize your data structure here. */
    MovingAverage(int size)
    {
        m_size = size;
        sum = 0;
    }
    
    double next(int val)
    {
        sum += val;
        m_queue.push(val);
        if(m_queue.size() > m_size )
        {
            int f = m_queue.front();
            m_queue.pop();
            sum -= f;
        }
        return (double)sum/m_queue.size();
    }
};

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage* obj = new MovingAverage(size);
 * double param_1 = obj->next(val);
 */
