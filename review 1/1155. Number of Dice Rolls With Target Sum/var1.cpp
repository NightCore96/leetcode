class Solution {
public:
    int numRollsToTarget(int d, int f, int target)
    {
        int mod = 1e9 +7;
        vector<vector<int>> dp(d+1,vector<int>(target+1,0));
        if(d == 1)
            return target <= f ? 1 : 0;
        for(int i = 1 ; i <= f;i++)
        {
            if( i > target)
                break;
            dp[1][i] = 1;
        }
        for(int i = 2; i <=d;i++)
        {
            for(int j = i ; j <= f*d ; j++)
            {
                
                if( j > target)
                  break;
                for(int k = 1; k<= f ; k++)
                {
                    if ( j - k < 1 )
                        break;
                    
                    dp[i][j] = (dp[i][j] + dp[i - 1][j - k]) % mod;
                }
            }
        }
        return dp[d][target];
    }
};
