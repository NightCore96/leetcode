class Solution {
public:
    int maximizeSweetness(vector<int>& sweetness, int K)
    {
        int start = 0;
        int end = 0;
        for(auto& i :sweetness)
        {
            end += i;
        }
        
        while(start <= end)
        {
            cout << start <<" " << end << endl;
            int mid = start + (end - start )/2;
            if(isOK(sweetness,K,mid))
            {
                start = mid + 1;
            }
            else
            {
                end = mid - 1;
            }
        }
        return end;
    }
    
    bool isOK(vector<int>& sweetness,int& K,int s)
    {
        int count = 0;
        int p = 0;
        for(const auto& c:sweetness)
        {
            count += c;
            if(count >= s)
            {
                p++;
                count = 0;
            }
        }
        
        return p >= K+1;
    }
};
