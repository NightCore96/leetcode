class Solution {
public:
    int singleNonDuplicate(vector<int>& nums)
    {
        if(nums.size() < 3)
            return nums[0];
        int start = 0;
        int end = nums.size() - 1;
        while(start < end)
        {
            int mid = start + (end - start) / 2;
            //cout<< start <<  " " <<mid << " "<< end <<endl;
            if(mid == 0 && nums[mid] != nums[mid+1])
                return nums[mid];
            if(mid == nums.size() && nums[mid] != nums[mid - 1])
                return nums[mid];
            if(nums[mid] != nums[mid+1] && nums[mid] != nums[mid-1])
                return nums[mid];
            
            if(mid % 2 == 0)
            {
                if(nums[mid] == nums[mid - 1])
                    end = mid - 1;
                else
                    start = mid +1;
            }
            else
            {
                if(nums[mid] == nums[mid + 1])
                    end = mid - 1;
                else
                    start = mid +1;
            }
            
        }
        return nums[start];
    }
};
