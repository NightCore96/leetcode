class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target)
    {
        sort(candidates.begin(),candidates.end());
        vector<int> buffer;
        vector<vector<int>> res;
        
        helper(candidates,target,buffer,res,0);
        return res;
    }
    
    void helper(vector<int>& candidates, int target,vector<int>& buffer,vector<vector<int>> &res,int index)
    {
        if(target < 0)
            return;
        if(target == 0)
        {
            res.push_back(buffer);
            return;
        }
        for(int i = index;i<candidates.size();i++)
        {
            if(i> index &&candidates[i] == candidates[i-1])
                continue;
            buffer.push_back(candidates[i]);
            target-=candidates[i];
            helper(candidates,target,buffer,res,i+1);
            buffer.pop_back();
            target+=candidates[i];
        }
    }
};
