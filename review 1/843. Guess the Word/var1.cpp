/**
 * // This is the Master's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Master {
 *   public:
 *     int guess(string word);
 * };
 */
class Solution {
public:
    void findSecretWord(vector<string>& wordlist, Master& master)
    {
        for(int i =0;i<10;i++)
        {
            if(wordlist.empty())
                break;
            string now = wordlist[rand()%wordlist.size()];
            int g = master.guess(now);
            vector<string> next;
            for(auto& w:wordlist)
            {
                if(g == isSame(now,w))
                    next.push_back(w);
            }
            wordlist = next;
        }
    }
    
    int isSame(string& s,string& t)
    {
        int res = 0;
        for(int i =0;i<s.size();i++)
        {
            if(s[i] == t[i])
                res++;
        }
        return res;
    }
};
