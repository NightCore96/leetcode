class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) 
    {
        if(nums.empty())
            return {-1,-1};
        int n = nums.size();
        int start = 0,end = n-1;
        int index = -1;
        while(start<=end)
        {
            int mid = start + (end - start)/2;
            if(nums[mid] == target)
            {
                index = mid;
                break;
            }
            else if(nums[mid]<target)
            {
                start = mid + 1;
            }
            else
            {
                end = mid - 1;
            }
        }
        if(index == -1)
            return {-1,-1};
        start = index;
        end = index;
        for(int i=index+1;i<n;i++)
        {
            if(nums[i]==nums[index])
                end++;
        }
        for(int i=index-1;i>=0;i--)
        {
            if(nums[i]==nums[index])
                start--;
        }
        return {start,end};
    }
};