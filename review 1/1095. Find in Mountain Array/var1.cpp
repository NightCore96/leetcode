/**
 * // This is the MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * class MountainArray {
 *   public:
 *     int get(int index);
 *     int length();
 * };
 */

class Solution {
public:
    int findInMountainArray(int target, MountainArray &mountainArr)
    {
        int top = peakIndexInMountainArray(mountainArr);
        
        int l = 0;
        int r = top;
        while(l <= r)
        {
            int mid = l + (r - l )/2;
            if(mountainArr.get(mid)  == target)
            {
                return mid;
            }
            else if(mountainArr.get(mid) > target)
            {
                r = mid - 1;
            }
            else
            {
                l = mid + 1;
            }
        }
        
        l = top;
        r = mountainArr.length() - 1;
        
        while(l <= r)
        {
            int mid = l + (r - l )/2;
            if(mountainArr.get(mid)  == target)
            {
                return mid;
            }
            else if(mountainArr.get(mid) < target)
            {
                r = mid - 1;
            }
            else
            {
                l = mid + 1;
            }
        }
        
        return -1;
    }
    
    int peakIndexInMountainArray(MountainArray &A)
    {
        int left = 0 ,right = A.length()-1;
        while(left<=right)
        {
            int mid = left + (right- left)/2;
            int A_mid = A.get(mid);
            int A_pre = A.get(mid-1);
            int A_next = A.get(mid+1);
            if(A_mid>A_pre&&A_mid>A_next)
            {
                return mid;
            }
            else if(A_mid>A_pre&&A_mid<A_next)
            {
                left = mid;
            }
            else if(A_mid<A_pre&&A_mid>A_next)
            {
                right = mid;
            }
        }
        return left;
    }
};
