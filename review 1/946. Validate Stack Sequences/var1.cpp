class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped)
    {
        stack<int> s;
        int i = 0;
        int j = 0;
        while(i < pushed.size() && j < popped.size())
        {
            if(s.empty())
            {
                s.push(pushed[i]);
                i++;
            }
            if(s.top() == popped[j])
            {
                s.pop();
                j++;
            }
            else
            {
                s.push(pushed[i]);
                i++;
            }
        }
        while(!s.empty()&&j<popped.size())
        {
            if(s.top() != popped[j])
                return false;
            s.pop();
            j++;
        }
        
        return i == pushed.size() && j == popped.size();
    }
};
