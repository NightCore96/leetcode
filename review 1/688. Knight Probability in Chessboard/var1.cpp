class Solution {
public:
    vector<vector<int>> dict = {{1,2},{2,1},{2,-1},{1,-2},{-1,-2},{-2,-1},{-1,2},{-2,1}};
    double knightProbability(int N, int K, int r, int c)
    {
        vector<vector<double>> dp(N,vector<double>(N,0));
        dp[r][c] = 1;
        while(K>0)
        {
            vector<vector<double>> temp(N,vector<double>(N,0));
            for(int i = 0;i < N ;i++)
            {
                for(int j = 0; j < N; j++)
                {
                    for(auto& d:dict)
                    {
                        int x = i+d[0];
                        int y = j+d[1];
                        if(x<0||y<0||x>=N||y>=N)
                            continue;
                        temp[x][y] += dp[i][j] / 8.0;
                    }
                }
            }
            K--;
            dp = temp;
        }
        double res = 0.0;
        for(int i = 0;i < N ;i++)
        {
            for(int j = 0; j < N; j++)
            {
                res += dp[i][j];
            }
        }
        return res;
    }
};
