/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class BSTIterator {
private:
    vector<int> buffer;
    int index;
public:
    BSTIterator(TreeNode* root)
    {
        index = 0;
        helper(root);
    }
    
    /** @return the next smallest number */
    int next()
    {
        int res = buffer[index];
        index++;
        return res;
    }
    
    /** @return whether we have a next smallest number */
    bool hasNext()
    {
        return index < buffer.size();
    }
private:
    void helper(TreeNode* root)
    {
        if(!root)
            return;
        helper(root->left);
        buffer.push_back(root->val);
        helper(root->right);
    }
};

/**
 * Your BSTIterator object will be instantiated and called as such:
 * BSTIterator* obj = new BSTIterator(root);
 * int param_1 = obj->next();
 * bool param_2 = obj->hasNext();
 */
