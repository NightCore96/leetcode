/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int rob(TreeNode* root) {
        if(!root)
        {
            return NULL;
        }
        vector<int> t= get(root);
        return max(t[1],t[0]);
    }
    vector<int> get(TreeNode* root)
    {
        vector<int> reslut(2,0);
        if(!root)
            return reslut;
        vector<int> l=get(root->left);
        vector<int> r=get(root->right);
        reslut[0]=root->val+l[1]+r[1];
        reslut[1]=max(r[0],r[1])+max(l[0],l[1]);
        return reslut;
    }
    
};