class Codec {
public:

    // Encodes a list of strings to a single string.
    string encode(vector<string>& strs)
    {
        string res = "";
        for(string& str:strs)
        {
            res += (str+'\n');
        }
        return res;
    }

    // Decodes a single string to a list of strings.
    vector<string> decode(string s)
    {
        vector<string> res;
        string temp = "";
        for(char& c:s)
        {
            if(c == '\n')
            {
                res.push_back(temp);
                temp = "";
            }
            else
            {
                temp += c;
            }
        }
        return res;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.decode(codec.encode(strs));
