class Solution {
public:
    vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries)
    {
        unordered_map<string,unordered_map<string,double>> graph;
        for(int i = 0; i < equations.size();i++)
        {
            auto e = equations[i];
            graph[e[0]][e[1]] = values[i];
            graph[e[1]][e[0]] = 1.0 / values[i];
        }
        vector<double> res;
        unordered_set<string> visited;
        for(const auto& q:queries)
        {
            double temp = 1.0;
            visited.clear();
            dfs(temp,q[0],q[1],graph,visited);
            res.push_back(temp);
        }
        return res;
    }
    
    bool dfs(double &now,string m,string c,unordered_map<string,unordered_map<string,double>> graph,unordered_set<string>& visited)
    {
        if(visited.count(m))
            return false;
        if(!graph.count(m) || !graph.count(c) )
        {
            now = -1.0;
            return false;
        }
        if(m == c)
        {
            now = 1.0;
            return true;
        }
        if(graph[m].count(c))
        {
            now = now * graph[m][c];
            return true;
        }
        visited.insert(m);
        for(const auto& p:graph[m])
        {
            double temp = now * p.second;
            if(dfs(temp,p.first,c,graph,visited))
            {
                now = temp;
                return true;
            }
        }
        now = -1.0;
        return false;
    }
};
