class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) 
    {
        vector<vector<int>> res;
        int size = nums.size();
        if(size < 4 )
            return res;
        sort(nums.begin(),nums.end());
        for(int i = 0; i< size-3 ; i++)
        {
            if(i>0 && nums[i]==nums[i-1])
                continue;
            if(nums[i]+nums[i+1]+nums[i+2]+nums[i+3]>target) 
                break;
            if(nums[i]+nums[size-3]+nums[size-2]+nums[size-1]<target) 
                continue;
            for(int j = i+1; j< size-2 ; j++)
            {
                if(j>i+1 && nums[j]==nums[j-1])
                    continue;
                if(nums[i]+nums[j]+nums[j+1]+nums[j+2]>target) 
                    break;
                if(nums[i]+nums[j]+nums[size-2]+nums[size-1]<target) 
                    continue;
                int L = j + 1;
                int R = size - 1;
                while(L < R)
                {
                    if(L > j+1 && nums[L]==nums[L-1])
                    {
                        L++;
                        continue;
                    }
                    if(R < size - 1 && nums[R]==nums[R+1])
                    {
                        R--;
                        continue;
                    }
                    int sum  = nums[i] + nums[j] + nums[L] + nums[R];
                    if(sum == target)
                    {
                        res.push_back({nums[i],nums[j],nums[L],nums[R]});
                        R--;
                        L++;
                    }
                    else if(sum > target)
                    {
                        R--;
                    }
                    else
                    {
                        L++;
                    }
                }
            }
        }
        return res;
    }
};