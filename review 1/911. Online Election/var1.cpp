class TopVotedCandidate {
public:
    map<int,int> list;
    //int finalwin;
    TopVotedCandidate(vector<int>& persons, vector<int>& times)
    {
        int win = 0;
        int vote = 0;
        unordered_map<int,int> table;
        for(int i = 0 ;i< persons.size();i++)
        {
            table[persons[i]]++;
            if(vote <= table[persons[i]])
            {
                win = persons[i];
                vote = table[persons[i]];
            }
            list[times[i]] = win;
        }
    }
    
    int q(int t)
    {
        auto it = list.upper_bound(t);
        if(it != list.begin())
        {
            return (--it)->second;
        }
        else
        {
            return (it)->second;
        }
    }
};

/**
 * Your TopVotedCandidate object will be instantiated and called as such:
 * TopVotedCandidate* obj = new TopVotedCandidate(persons, times);
 * int param_1 = obj->q(t);
 */
