class Solution {
public:
    int slidingPuzzle(vector<vector<int>>& board)
    {
        string start = "";
        string goal = "123450";
        int zero = 0;
        for(int i = 0;i<6;i++)
        {
            start += to_string(board[i/3][i%3]);
            if(board[i/3][i%3] == 0)
                zero = i;
        }
        if(start == goal)
            return 0;
        int res = 1;
        queue<pair<string,int>> q;
        q.push({start,zero});
        unordered_set<string> visited;
        vector<vector<int>> dir = {{1,0},{-1,0},{0,1},{0,-1}};
        while(!q.empty())
        {
            int size = q.size();
            for(int i = 0;i<size;i++)
            {
                auto p =q.front();
                q.pop();
                int x = p.second / 3;
                int y = p.second % 3;
                for(auto& d:dir)
                {
                    int nx = x+d[0];
                    int ny = y+d[1];
                    if(nx<0||ny<0||nx>=2||ny>=3)
                        continue;
                    int id = nx*3 + ny;
                    string t = p.first;
                    swap(t[id],t[p.second]);
                    if(t == goal)
                        return res;
                    if(!visited.count(t))
                    {
                        visited.insert(t);
                        q.push({t,id});
                    }
                }
            }
            res++;
        }
        return -1;
    }
};
