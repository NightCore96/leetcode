class Solution {
public:
int swimInWater(vector<vector<int>>& grid)
{
    if(grid.size()==0 || grid[0].size()==0) return 0;

    int n=grid.size();
    int m=grid[0].size();

    typedef pair<int,int> cell;
    priority_queue<cell,vector<cell>,greater<cell>> pq;
    
    vector<vector<bool>> vis(n,vector<bool>(m,false));
    vector<vector<int>> dir = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

    pq.push({grid[0][0],0});
    vis[0][0]=true;

    int maxWait=0;
    while(pq.size() != 0)
    {
        cell rvtx=pq.top();
        pq.pop();

        int x=rvtx.second / m;
        int y=rvtx.second % m;

        maxWait=max(maxWait,rvtx.first);
        if(x==n-1 && y==m-1) break;

        for(int d=0;d<4;d++)
        {
            int r=x + dir[d][0];
            int c=y + dir[d][1];

            if(r>=0 && c>=0 && r<n && c<m && !vis[r][c])
            {
                vis[r][c]=true;
                pq.push({grid[r][c],r * m + c});

            }
        }
    }
    return maxWait;
}
};
