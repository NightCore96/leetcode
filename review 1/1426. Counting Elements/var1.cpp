class Solution {
public:
    int countElements(vector<int>& arr)
    {
        unordered_set<int> table;
        for(int a:arr)
            table.insert(a);
        int res = 0;
        for(int a:arr)
        {
            if(table.count(a+1))
                res++;
        }
        return res;
    }
};
