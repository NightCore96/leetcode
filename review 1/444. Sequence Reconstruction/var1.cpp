class Solution {
public:
    bool sequenceReconstruction(vector<int>& org, vector<vector<int>>& seqs)
    {
        int n = org.size();
        unordered_map<int,int> table;
        vector<bool> ok(n,false);
        for(int i = 0;i<n;i++)
        {
            table[org[i]] = i;
        }
        int cur = 0;
        if(seqs.empty())
            return false;
        bool allempty = true;
        for(auto& s:seqs)
        {
            for(int i = 0;i<s.size();i++)
            {
                allempty = false;
                if(s[i] > n || s[i] < 0)
                    return false;
                if(i == 0)
                    continue;
                int pre = s[i-1];
                if(table[pre] >= table[s[i]])
                    return false;
                if(!ok[s[i]] && table[pre]+1 == table[s[i]])
                {
                    cur++;
                    ok[s[i]] = true;
                }
            }
        }
        return cur == n - 1 && !allempty;
    }
};
