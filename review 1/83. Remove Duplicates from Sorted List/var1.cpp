/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) 
    {
        if(!head)
            return head;
        ListNode *res = new ListNode(0);
        ListNode *newlist = res;
        ListNode *pre = head;
        ListNode *node = head;
        //int now = head->val;
        while(node)
        {
            //cout<<count<<endl;
            if(pre->val != node->val)
            {
                newlist->next = pre;
                pre->next = nullptr;
                newlist = pre;
                pre = node;
            }
            node = node->next;
        }
        newlist->next = pre;
        pre->next = nullptr;
        return res->next;
    }
};