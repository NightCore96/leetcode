 /**
  * Definition for a binary tree node.
  * struct TreeNode {
  *     int val;
  *     TreeNode *left;
  *     TreeNode *right;
  *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
  *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  * };
  */
 class Solution {
 public:
     vector<vector<int>> verticalTraversal(TreeNode* root)
     {
         vector<vector<int>> res;
         if(!root)
             return res;
         map<int,vector<pair<int,int>>> table;
         queue<pair<TreeNode*,int>> q;
         q.push({root,0});
         int level;
         while(!q.empty())
         {
             int size = q.size();
             for(int i = 0;i<size;i++)
             {
                 auto p = q.front();
                 q.pop();
                 table[p.second].push_back({level,p.first->val});
                 if(p.first->left)
                     q.push({p.first->left,p.second - 1});
                 if(p.first->right)
                     q.push({p.first->right,p.second + 1});
             }
             level++;
         }
         for(auto v:table)
         {
             sort(v.second.begin(),v.second.end());
             vector<int> temp;
             for(int i = 0;i<v.second.size();i++)
             {
                 temp.push_back(v.second[i].second);
             }
             res.push_back(temp);
         }
         return res;
     }
 };
