/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() {}

    Node(int _val, Node* _left, Node* _right, Node* _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
*/
class Solution {
public:
    Node* connect(Node* root) 
    {
        Node* temp = new Node(0,NULL,NULL,NULL);
        Node* n = temp;
        Node* res = root;
        temp->next = root;
        while(root)
        {
            if(root->left)
            {
                n->next = root->left;
                n = n->next;
            }
            if(root->right)
            {
                n->next = root->right;
                n = n->next;
            }
            root = root->next;
            if(!root)
            {
                root = temp->next;
                temp->next = nullptr;
                n = temp;
            }
        }
        return res;
    }
};