class Solution {
public:
    int longestPalindrome(string s)
    {
        unordered_map<char,int> table;
        for(auto& c:s)
        {
            table[c]++;
        }
        int res = 0;
        int Odd = 0;
        for(auto& p:table)
        {
            if(p.second % 2 == 0)
            {
                res += p.second;
            }
            else
            {
                Odd = 1;
                res += p.second - 1;
            }
        }
        
        return res + Odd;
    }
};
