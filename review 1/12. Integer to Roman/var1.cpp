class Solution {
public:
    string intToRoman(int num) 
    {
        vector<string> rome = {"M", "CM", "D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        vector<int> number = {1000,  900, 500, 400,100,  90, 50,  40, 10,   9,  5,   4,  1};
        string res ="";
        for(int i=0;i<rome.size();i++)
        {
            int count = num/number[i];
            if(count == 0)
                continue;
            for(int j = 0;j<count;j++)
            {
                res+= rome[i];
            }
            num = num % number[i];
        }
        return res;
    }
};