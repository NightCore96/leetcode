class Solution {
public:
    vector<vector<int>> generate(int numRows) 
    {
        vector<vector<int>> res;
        if(numRows == 0)
            return res;
        vector<int> level;
        for(int i=1; i<=numRows; i++)
        {
            level= vector<int>(i,1);
            for(int j = 1 ; j < i - 1; j++)
            {
                //cout<<i <<" "<<j<<endl;
                level[j] = res[i-2][j] + res[i-2][j-1];
            }
            res.push_back(level);
            level.clear();
        }
        return res;
    }
};