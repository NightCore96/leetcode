class Solution
{
private:
    struct pqcompare
    {
        bool operator()(const vector<int>& a,const vector<int>& b)
        {
            return (a[0]+a[1]) >(b[0]+b[1]);
        }
    };
public:
    vector<vector<int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k)
    {
        vector<vector<int>> res;
        if(nums1.empty()||nums2.empty())
            return res;
        priority_queue<vector<int>,vector<vector<int>>,pqcompare> pq;
        for(const auto& i1:nums1)
        {
            for(const auto& i2:nums2)
            {
                pq.push({i1,i2});
            }
        }
        
        for(int i=0;i<k;i++)
        {
            if(pq.empty())
                break;
            auto v  = pq.top();
            pq.pop();
            res.push_back(v);
        }
        return res;
    }
};
