class Solution {
public:
    bool canJump(vector<int>& nums) 
    {
        if(nums.empty())
            return true;
        int n = nums.size();
        if(n<=nums[0])
            return true;
        int next = nums[0];
        for(int i=0;i<n&&i<=next;i++)
        {
            if(next<i+nums[i])
            {
                next = i+nums[i];
            }
        }
        return next>= n-1;
    }
};