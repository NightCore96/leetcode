class Solution {
public:
    string getPermutation(int n, int k) 
    {
        vector<int> nums(n);
        string res;
        int pcount = 1;
        for(int i=0; i<n; i++) {
            nums[i] = i+1;
            pcount *= i+1;
        }
        k--;
        for(int i=0; i<n; i++) {
            pcount /= n-i;
            int selected = k/pcount;
            res += '0' + nums[selected];
            for(int j=selected; j<n-i-1; j++) nums[j] = nums[j+1];
            k = k%pcount;
        }
        return res;
    }
};