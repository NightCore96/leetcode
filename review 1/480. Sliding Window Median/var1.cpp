class Solution {
public:
    vector<double> medianSlidingWindow(vector<int>& nums, int k)
    {
        multiset<int> w(nums.begin(),nums.begin()+k);//BST
        vector<double> res;
        auto mid = next(w.begin(),k/2);
        int i = k;
        int size= nums.size();
        while(i<=size)
        {
            res.push_back((double(*mid) + *prev(mid, 1 - k%2)) / 2);
            if(i==size)
                break;
            
            w.insert(nums[i]);
            if (nums[i] < *mid)
                mid--;
            
            if (nums[i-k] <= *mid)
            mid++;
            w.erase(w.lower_bound(nums[i-k]));
            i++;
        }
        return res;
    }
    
};
