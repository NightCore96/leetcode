/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) 
    {
        if(inorder.empty()||preorder.empty()||inorder.size()!=preorder.size())
            return nullptr;
        if(inorder.size()==1)
            return new TreeNode(inorder[0]);
        unordered_map<int,int> permap;
        for(int i=0;i<inorder.size();i++)
        {
            permap[inorder[i]] = i;
        }
        int index = 0;
        return helper(preorder, inorder, 0, inorder.size()-1,permap, index);
    }
    
    TreeNode* helper(vector<int>& preorder, vector<int>& inorder, int start, int end,unordered_map<int,int> &permap,int &index)
    {
        if(start>end)
            return nullptr;
        TreeNode* res = new TreeNode(preorder[index++]);
        if (start == end) 
            return res; 
        int i = permap[res->val];
        res->left = helper(preorder, inorder, start, i-1,permap, index);
        res->right = helper(preorder, inorder, i+1, end,permap, index);
        return res;
    }
};