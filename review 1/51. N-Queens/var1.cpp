class Solution {
public:
    vector<vector<string>> solveNQueens(int n)
    {
        vector<vector<string>> res;
        vector<string> queen(n,string(n,'.'));
        helper(queen, 0, res);
        return res;
    }
    void helper(vector<string>& queen, int currow,vector<vector<string>>& res)
    {
        int n = queen.size();
        if(currow==n){
            res.push_back(queen);
            return;
        }
        
        for(int i = 0; i < n;i++){
            if(isvalid(queen, currow, i)){
                queen[currow][i] = 'Q';
                helper(queen, currow+1,res);
                queen[currow][i] = '.';
            }
        }
    }
    bool isvalid(vector<string>& queen,int currow, int col){
        int n = queen.size();
        for(int i = 0; i < currow;i++){
            if(queen[i][col]=='Q') return false;
        }
        for(int i=col-1, j=currow-1; i>=0 && j>=0; i--,j--){
            if(queen[j][i]=='Q') return false;
        }
        for(int i = col+1, j=currow-1;i<n && j>= 0;i++,j--){
            if(queen[j][i]=='Q') return false;
        }
        return true;
    }
};
