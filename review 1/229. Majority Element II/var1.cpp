class Solution {
public:
    vector<int> majorityElement(vector<int>& nums)
    {
        vector<int> res;
        if(nums.empty())
            return res;
        int a = nums[0];
        int b = nums[0];
        int i1 = 0;
        int i2 = 0;
        for(auto &n:nums)
        {
            if(n == a)
            {
                i1++;
            }
            else if(n == b)
            {
                i2++;
            }
            else if(i1 == 0)
            {
                a = n;
                i1++;
            }
            else if(i2 == 0)
            {
                b = n;
                i2++;
            }
            else
            {
                i1--;
                i2--;
            }
        }
        i1 = 0;
        i2 = 0;
        for(auto &n:nums)
        {
            if(n == a)
                i1++;
            else if(n == b)
                i2++;
        }
        if(i1 > nums.size()/3)
            res.push_back(a);
        if(i2 > nums.size()/3)
            res.push_back(b);
        return res;
    }
};
