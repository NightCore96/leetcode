class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) 
    {
        if(nums.empty())
            return {};
        unordered_map<int,int> table;
        for(int i=0;i<nums.size();i++)
        {
            int find = target - nums[i];
            if(table.find(nums[i])!=table.end())
                return {table[nums[i]],i};
            table[find]=i;
        }
        return {};
    }
};