class Solution {
public:
    bool isRectangleOverlap(vector<int>& rec1, vector<int>& rec2)
    {
        return isLineOverlap({rec1[0],rec1[2]},{rec2[0],rec2[2]})&& isLineOverlap({rec1[1],rec1[3]},{rec2[1],rec2[3]});
    }
    
    bool isLineOverlap(pair<int,int> L1, pair<int,int> L2)
    {
        return max(L1.first,L2.first) < min(L1.second,L2.second);
    }
};
