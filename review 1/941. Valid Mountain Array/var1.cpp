class Solution {
public:
    bool validMountainArray(vector<int>& A)
    {
        if(A.size() < 3)
            return false;
        int top  = 0;
        for(int i = 1 ;i < A.size();i++)
        {
            if(A[i] == A[i - 1])
            {
                return false;
            }
            if(A[i] < A[i - 1])
            {
                top = i -1;
                break;
            }
        }
       // cout << top <<endl;
        for(int j = A.size() - 2 ; j>=0;j--)
        {
            if(A[j] == A[j+1])
            {
                return false;
            }
            if(A[j] < A[j+1])
            {
                return top == j+1;
            }
        }
        return false;
    }
};
