class Solution {
public:
    int calculateTime(string keyboard, string word) 
    {
        unordered_map<char,int> table;
        int i = 0;
        for(auto c: keyboard)
        {
            table[c] = i;
            i++;
        }
        int res = 0;
        int pre = 0;
        for(auto c : word)
        {
            res += abs(pre - table[c]);
            pre = table[c];
        }
        return res;
    }
};