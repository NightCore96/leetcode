class Solution {
public:
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor)
    {
        int oldcolor = image[sr][sc];
        if(newColor != oldcolor)
            dfs(image,sr,sc,newColor,oldcolor);
        return image;
    }
    
    void dfs(vector<vector<int>>& image, int sr, int sc, int& newColor,int& oldcolor)
    {
        if(sr < 0 || sc < 0 || sr >= image.size() || sc >= image[0].size() || image[sr][sc] != oldcolor)
            return;
        image[sr][sc] = newColor;
        dfs(image,sr+1,sc,newColor,oldcolor);
        dfs(image,sr-1,sc,newColor,oldcolor);
        dfs(image,sr,sc+1,newColor,oldcolor);
        dfs(image,sr,sc-1,newColor,oldcolor);
    }
};
