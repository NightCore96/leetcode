class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        if(nums.empty())
            return nums;
        vector<int> r;
        int size=nums.size();
        for(int i=0;i<size;i++)
        {
            int m = abs(nums[i]);
            if(nums[m-1]>0)
                nums[m-1]*=-1;
        }
        for(int i=0;i<size;i++)
        {
            if(nums[i]>0)
                r.push_back(i+1);
        }
        return r;
    }
};