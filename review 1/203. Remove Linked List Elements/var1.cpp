/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeElements(ListNode* head, int val)
    {
        if(head == nullptr)
            return head;
        ListNode* pre = nullptr;
        ListNode* node = head;
        while(node != nullptr)
        {
            if(node -> val == val)
            {
                if(node == head)
                {
                    head = node->next;
                }
                else
                {
                    pre->next = node->next;
                }
                node = node->next;
            }
            else
            {
                pre = node;
                node = node->next;
            }
            
        }
        return head;
    }
};
