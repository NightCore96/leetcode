/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxPathSum(TreeNode* root) 
    {
        if(!root)
            return 0;
        int res = INT_MIN;
        find(root,res);
        return res;
    }
    int find(TreeNode* root,int &res)
    {
        if(!root)
            return 0;
        int L = max(find(root->left,res),0);
        int R = max(find(root->right,res),0);
        res = max(L+R+root->val,res);
        return max(L,R)+root->val;
    }
};