class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) 
    {
        unordered_map<string,vector<string>> table;
        for(string str:strs)
        {
            string temp = str;
            sort(str.begin(),str.end());
            table[str].push_back(temp);
        }
        vector<vector<string>> res;
        for(auto p:table)
        {
            res.push_back(p.second);
        }
        return res;
    }
};