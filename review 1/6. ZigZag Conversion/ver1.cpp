class Solution {
public:
    string convert(string s, int numRows) 
    {
        string res = "";
        if(s.size() == 0 || numRows == 0) 
            return res;
        if(numRows == 1) 
            return s;
        int size = s.size();
        int group = numRows * 2 - 2;
        for(int i = 0;i < numRows ;i++)
        {
            for(int j = i ; j< size ;j += group)
            {
                res += s[j];
                if(i!=0&&i!=numRows-1&& (j + group - 2*i) < size)
                    res += s[j + group - 2*i]; 
            }
        }
        return res;
    }
};