class Solution {
public:
    int coinChange(vector<int>& coins, int amount) 
    {
        if(amount == 0 || coins.empty())
            return 0;
        vector<int> dp(amount+1,0);
        int minCoins = INT_MAX;
        for(int coin:coins)
        {
            if(coin <= amount)
                dp[coin] = 1;
            minCoins = min(minCoins,coin);
        }
        for(int i = minCoins ; i<= amount ;i++)
        {
            for(int coin:coins)
            {
                if( i-coin >=minCoins &&  dp[i - coin] != 0)
                {
                    dp[i] = dp[i] == 0? dp[i-coin] + 1 : min(dp[i],dp[i-coin] + 1);
                }
            }
        }
        return dp[amount] == 0 ? -1 : dp[amount]; 
    }
};