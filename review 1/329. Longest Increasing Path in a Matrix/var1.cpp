class Solution {
public:
    int longestIncreasingPath(vector<vector<int>>& matrix)
    {
        if(matrix.empty() || matrix[0].empty())
            return 0;
        int n = matrix.size();
        int m = matrix[0].size();
        vector<vector<int>> dp(n,vector<int>(m,0));
        int res = 1;
        for(int i = 0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                res = max(res,DFS(matrix,i,j,dp));
            }
        }
        return res;
    }
private:
    vector<vector<int>> dir = {{1,0},{-1,0},{0,1},{0,-1}};
    int DFS(vector<vector<int>>& matrix,int x,int y,vector<vector<int>>& dp)
    {
       if(dp[x][y] > 0)
           return dp[x][y];
        int res = 1;
        for(auto &d:dir)
        {
            int next_x = x + d[0];
            int next_y = y + d[1];
            if((next_x < 0) || (next_x >= matrix.size())||
               (next_y < 0) || (next_y >= matrix[0].size())||
              (matrix[x][y] <= matrix[next_x][next_y]))
                continue;
            int path = 1 + DFS(matrix,next_x,next_y,dp);
            res = max(res,path);
        }
        dp[x][y] = res;
        return res;
    }
};
