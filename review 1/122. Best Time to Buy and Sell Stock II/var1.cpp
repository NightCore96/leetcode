class Solution {
public:
    int maxProfit(vector<int>& prices) 
    {
        int days = prices.size();
        if(days<=1)
            return 0;
        int buy = prices[0];
        int res = 0;
        for(int i = 1; i <days;i++) 
        {
            int p = prices[i] - buy; 
            if( p >= 0 )
            {
                res += p;
            }
            buy = prices[i];
        }
        return res;
    }
};