/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) 
    {
        if(!head)
            return head;
        ListNode *res = new ListNode(0);
        ListNode *newlist = res;
        ListNode *pre = head;
        ListNode *node = head;
        //int now = head->val;
        int count = 0;
        while(node)
        {
            //cout<<count<<endl;
            if(pre->val == node->val)
            {
                count++;
            }
            else
            {
                if(count == 1)
                {
                    newlist->next = pre;
                    pre->next = nullptr;
                    newlist = pre;
                }
                count = 1;
                pre = node;
            }
            node = node->next;
        }
        if(count == 1)
        {
            newlist->next = pre;
            pre->next = nullptr;
            newlist = pre;
        }
        return res->next;
    }
};