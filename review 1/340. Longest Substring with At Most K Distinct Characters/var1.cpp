class Solution {
public:
    int lengthOfLongestSubstringKDistinct(string s, int k)
    {
        int res = 0;
        if(s.empty()||k<=0)
            return 0;
        unordered_map<char,int> table;
        int start = 0;
        for(int i = 0;i<s.size();i++)
        {
            if(table.size() < k)
            {
                table[s[i]] = i;
            }
            else
            {
                if(!table.count(s[i]))
                {
                    res = max(res,i - start);
                    int min = INT_MAX;
                    char a;
                    for(auto& p:table)
                    {
                        if(p.second < min)
                        {
                            min = p.second;
                            a = p.first;
                        }
                    }
                    table.erase(a);
                    start = min+1;
                    table[s[i]] = i;
                }
                else
                {
                    table[s[i]] = i;
                }
            }
        }
        int len = s.size() - start;
        res = max(res,len);
        return res;
    }
};
