class Solution {
public:
    bool checkValidString(string s)
    {
        int left = 0;
        int right = 0;
        for(char c: s)
        {
            if(c == '(')
                left++;
            else
                left--;
            if(c == ')')
                right--;
            else
                right++;
            if(right < 0)
                break;
            left = max(0,left);
        }
        
        return left == 0;
    }
};
