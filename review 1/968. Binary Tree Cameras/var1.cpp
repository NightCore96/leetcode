/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int helper(TreeNode* root,int& sum)
    {
        if(root == nullptr)
            return 1;
        int l = helper(root->left,sum);
        int r = helper(root->right,sum);
        if(l ==  0|| r == 0)
        {
            sum++;
            return 2;
        }
        if(l == 2 || r == 2)
        {
            return 1;
        }
        return 0;
        
    }
    int minCameraCover(TreeNode* root)
    {
        int res = 0;
        if(0 == helper(root,res))
            res++;
        return res;
    }
};
