class Solution {
public:
    vector<int> plusOne(vector<int>& digits) 
    {
        int n = digits.size();
        int c = 0;
        digits[n - 1]++;
        for(int i = n -1;i >= 0 ; i--)
        { 
            digits[i] += c;
            c = digits[i] /10;
            digits[i] %= 10; 
            if(c == 0)
                break;
        }
        if(c != 0)
            digits.insert(digits.begin(),c);
        return digits;
    }
};