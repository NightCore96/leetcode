class Solution {
public:
    bool search(vector<int>& nums, int target) 
    {
        if(nums.empty())
            return false;
        int size = nums.size();
        int start = 0;
        int end = size-1;
        while(start < end)
        {
            int mid = start + (end - start) / 2;
            if(nums[mid] == target)
                return true;
            else if(nums[mid] < nums[end])
            {
                if(target <= nums[end] && target > nums[mid])
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid;
                }
            }
            else if (nums[mid] > nums[end])
            {
                if(target <= nums[mid] && target >= nums[start])
                {
                    end = mid;
                }
                else
                {
                    start = mid + 1;
                }
            }
            else
                end--;
        }
        return nums[start] == target;
    }
};