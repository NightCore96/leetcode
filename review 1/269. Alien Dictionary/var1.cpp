class Solution {
public:
    vector<string> generatePalindromes(string s)
    {
        unordered_set<string> res;
        int len = s.size();
        if( len < 2)
            return {s};
        unordered_map<char,int> table;
        for(auto& c:s)
        {
            table[c]++;
        }
        string center = "";
        for(auto& p:table)
        {
            
            if(p.second % 2 != 0)
            {
                if(len % 2 == 0 || center != "")
                {
                    return {};
                }
                center = p.first;
            }
            p.second = p.second / 2;
            
        }
        int lenhelf = len/2;
        for(auto& p:table)
        {
            if(p.second != 0)
                helper(res,table,p.first,lenhelf,"",center);
        }
        
        return vector<string>(res.begin(),res.end());
        
    }
    
    void helper(unordered_set<string> &res,unordered_map<char,int>& table,char key,int& len,string now,string &center)
    {
        
        if(now.size() == len)
        {
            if(center != "")
                now = now + center;
            for(int i = len -1; i >= 0; i--)
                now = now+ now[i];
            res.insert(now);
            return;
        }
        if(table[key] == 0)
            return ;
        table[key]--;
        now = now+key;
        for(auto& p:table)
        {
            //cout<< p.first <<" "<<p.second <<" "<< now<<endl;
            helper(res,table,p.first,len,now,center);
        }
        table[key]++;
    }
    
};
