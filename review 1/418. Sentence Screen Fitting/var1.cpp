class Solution {
public:
    int wordsTyping(vector<string>& sentence, int rows, int cols)
    {
        string all ="";
        for(string s:sentence)
            all+=s+" ";
        int s_size = all.size();
        int index = 0;
        for(int i=0;i<rows;i++)
        {
            index+=cols;
            if(all[index%s_size]==' ')
                index++;
            else
            {
                while(index>0&&all[(index-1)%s_size] != ' ')
                    index--;
            }
        }
        
        return index/s_size;
    }
};
