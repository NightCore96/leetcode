class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) 
    {
        if(s.empty())
            return true;
        int n = s.size();
        unordered_set<string> dict;
        for(auto c:wordDict)
        {
            if(c==s)
                return true;
            dict.insert(c);
        }
        vector<bool>dp (n+1,false);
        dp[0] = true;
        for(int i = 1;i<=n;i++)
        {
            for(int j=0;j<i;j++)
            {
                dp[i]=dp[j]&&(dict.count(s.substr(j,i-j)));
                if(dp[i])
                    break;
            }   
        }
        return dp[n];
    }
};