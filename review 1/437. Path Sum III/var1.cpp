/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int pathSum1(TreeNode* root, int sum)
    {
        int res = 0;
        if(!root)
            return res;
        if(root->val == sum)
            res++;
        return res + pathSum1(root -> left, sum - root->val) + pathSum1(root -> right, sum - root->val);
    }
    
    int pathSum(TreeNode* root, int sum)
    {
        if(!root)
            return 0;
        return pathSum1(root, sum) + pathSum(root -> left, sum) + pathSum(root -> right, sum);
    }
};
