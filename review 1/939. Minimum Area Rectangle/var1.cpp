class Solution {
public:
    int minAreaRect(vector<vector<int>>& points) {
        unordered_map<int, vector<int>> map;
        for (vector<int> & point : points) {
            map[point[0]].push_back(point[1]);
        }
        for (auto &it : map) {
            sort(it.second.begin(), it.second.end());
        }
        int res = INT_MAX;
        for (auto a = map.begin(); a != map.end(); a++) {
            if (a->second.size() < 2) {
                continue;
            }
            for (auto b = next(a); b != map.end(); b++) {
                if (b->second.size() < 2) {
                    continue;
                }
                vector<int> &va = a->second;
                vector<int> &vb = b->second;
                int i = 0, j = 0, lena = va.size(), lenb = vb.size();
                int prev = -1;
                int absgap = abs(b->first - a->first);
                if (res <= absgap) {
                    continue;
                }
                while (i < lena && j < lenb) {
                    if (va[i] < vb[j]) {
                        i ++;
                    } else if (va[i] > vb[j]) {
                        j ++;
                    } else {
                        if (prev != -1) {
                            int area = absgap * (va[i] - va[prev]);
                            if (res > area) {
                                res = area;
                            }
                        }
                        prev = i++;
                        j ++;
                    }
                }
            }
        }

        return res == INT_MAX ? 0 : res;
    }
};
