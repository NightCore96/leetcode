class Solution {
public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target) 
    {
        vector<vector<int>> res;
        int sum = 0;
        vector<int> set;
        helper(res,candidates,target,sum,0,set);
        return res;
    }
    
    void helper(vector<vector<int>> &res,vector<int>& candidates, int &target,int &sum,int pos,vector<int>& set)
    {
        if(sum > target)
            return ;
        if(sum == target)
        {
            res.push_back(set);
            return ;
        }
        for(int i = pos ; i < candidates.size();i++)
        {
            int c= candidates[i];
            set.push_back(c);
            sum += c;
            helper(res,candidates,target,sum,i,set);
            sum -= c;
            set.pop_back();
        }
    }
};