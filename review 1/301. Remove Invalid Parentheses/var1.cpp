class Solution {
public:
    vector<string> removeInvalidParentheses(string s) 
    {
        int R = 0;
        int L = 0;
        unordered_set<string> res;
        for(char c:s)
        {
            if(c=='(')
                L++;
            else if(c==')')
            {
                if(L<=0)
                    R++;
                else
                    L--;
            }
        }
        helper(res,s,0,L,R);
        return vector<string>(res.begin(),res.end());
    }
    
    void helper(unordered_set<string> &res,string s,int index,int L,int R)
    {
        if(L==0&&R==0)
        {
            if(isVaild(s))
                res.insert(s);
            return;
        }
        
        for(int i = index;i<s.size();i++)
        {
            if(L>0&&R==0&&s[i]=='(')
            {
                if(i==index||s[i]!=s[i-1])
                {
                    string temp = s;
                    temp.erase(i,1);
                    helper(res,temp,i,L-1,R);
                }
            }
            else if(R>0&&s[i]==')')
            {
                if(i==index||s[i]!=s[i-1])
                {
                    string temp = s;
                    temp.erase(i,1);
                    helper(res,temp,i,L,R-1);
                }
            }
        }        
    }
    
    bool isVaild(string s)
    {
        int count =0;
        for(char c:s)
        {
            if(c=='(')
                count++;
            else if(c==')')
                count--;
            if(count<0)
                return false;
        }
        return count==0;
    }
};