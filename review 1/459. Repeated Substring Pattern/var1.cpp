class Solution {
public:
    bool repeatedSubstringPattern(string s)
    {
        int n = s.size();
        char back = s.back();
        for(int i = 0;i< n/2;i++)
        {
            if(back == s[i] && n%(i+1) == 0)
            {
                bool isFind = true;
                for(int j = i+1 ; j < n ; j+= (i+1))
                {
                    if(!isFind)
                        break;
                    for(int k = 0 ; k <= i;k++)
                    {
                        if(s[k] != s[k + j])
                        {
                            isFind = false;
                            break;
                        }
                    }
                }
                if(isFind)
                    return true;
            }
        }
        return false;
    }
};
