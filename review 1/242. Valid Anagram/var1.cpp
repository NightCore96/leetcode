class Solution {
public:
    bool isAnagram(string s, string t)
    {
        unordered_map<char,int> table;
        for(auto &n:s)
        {
            table[n]++;
        }
        for(auto &n:t)
        {
            if(!table.count(n))
                return false;
            table[n]--;
            if(table[n]==0)
                table.erase(n);
        }
        return table.empty();
    }
};
