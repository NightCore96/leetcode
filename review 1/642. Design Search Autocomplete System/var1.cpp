class AutocompleteSystem
{
private:
    struct Trie
    {
        unordered_map<char,Trie*> child;
        unordered_set<string> list;
    };
    
    Trie* root;
    Trie* cur;
    bool isEmpty;
    string nowinput;
    unordered_map<string,int> strings;
    void insert(string s)
    {
        Trie* node = root;
        isEmpty = false;
        for(auto& c:s)
        {
            if(!node->child.count(c))
                node->child[c] = new Trie();
            node = node->child[c];
            node->list.insert(s);
        }
    }
public:
    AutocompleteSystem(vector<string>& sentences, vector<int>& times)
    {
        root = new Trie();
        cur = root;
        nowinput = "";
        for(int i = 0; i<times.size();i++)
        {
            strings[sentences[i]] = times[i];
            insert(sentences[i]);
        }
    }
    
    vector<string> input(char c)
    {
        if(c == '#')
        {
            isEmpty = false;
            if(nowinput.size() == 0)
                return {};
            strings[nowinput]++;
            insert(nowinput);
            nowinput = "";
            cur = root;
            return {};
        }
        nowinput += c;
        //cout<< nowinput<<endl;
        if( isEmpty || !cur->child.count(c))
        {
            isEmpty = true;
            return {};
        }
        cur = cur->child[c];
        auto sort = [](pair<int,string>& a,pair<int,string>& b)
        {
            if(a.first == b.first);
                return a.second < b.second;
            return a.first > b.first;
        };
        priority_queue<pair<int,string>,vector<pair<int,string>>,decltype(sort)> pq(sort);
        for(auto& c : cur->list)
        {
            //cout<< strings[c] << " " << c<<endl;
            pq.push({strings[c],c});
        }
        vector<string> res;
        for(int i = 0; i< 3;i++)
        {
            if(pq.empty())
                break;
            //cout << pq.top().first<<endl;
            res.push_back(pq.top().second);
            pq.pop();
        }
        //cout<< "!!!!" <<endl;
        return res;
    }
};

/**
 * Your AutocompleteSystem object will be instantiated and called as such:
 * AutocompleteSystem* obj = new AutocompleteSystem(sentences, times);
 * vector<string> param_1 = obj->input(c);
 */
