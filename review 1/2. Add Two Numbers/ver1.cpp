/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) 
    {
        if(l1 == nullptr)
            return l2;
        if(l2 == nullptr)
            return l1;
        ListNode* n1 = l1;
        ListNode* n2 = l2;
        ListNode*res = l1;
        int c = 0;
        while(n1 != nullptr && n2 != nullptr)
        {
            n1 = n1->next;
            n2 = n2->next;
        }
        if(n2 == nullptr)
        {
            n1 = l1;
            n2 = l2;
            res = l1;
        }
        else
        {
            n1 = l2;
            n2 = l1;
            res =l2;
        }
        ListNode* tail;
        while(n2 != nullptr)
        {
            int n = n1->val+n2->val+c; 
            n1->val = n%10;
            c = n/10;
            tail = n1;
            n1 = n1->next;
            n2 = n2->next;
        }
        while( c > 0)
        {
            
            if(tail->next != nullptr)
            {
                
                int n = c + tail->next->val;
                tail->next->val = n%10;
                c = n/10;
            }
            else
            {
                tail->next = new ListNode(c%10);
                c = c/10;
            }
            tail = tail->next;
        }
        return res;
    }
};