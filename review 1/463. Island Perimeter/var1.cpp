class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid)
    {
        int res = 0;
        for(int i = 0;i< grid.size();i++)
        {
            for(int j = 0;j < grid[0].size();j++)
            {
                if(grid[i][j] == 1)
                    dfs(grid,i,j,res);
            }
        }
        return res;
    }
    
    bool dfs(vector<vector<int>>& grid,int x,int y,int &res)
    {
        if(x < 0 || y < 0 || x >= grid.size() || y >= grid[0].size() || grid[x][y] == 0)
            return true;
        if(grid[x][y] == -1)
            return false;
        grid[x][y] = -1;
        if(dfs(grid,x+1,y,res))
            res++;
        if(dfs(grid,x-1,y,res))
            res++;
        if(dfs(grid,x,y+1,res))
            res++;
        if(dfs(grid,x,y-1,res))
            res++;
        return false;
    }
};
