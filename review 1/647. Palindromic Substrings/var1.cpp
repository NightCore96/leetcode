class Solution {
public:
    int countSubstrings(string s)
    {
        int res = s.size();
        for(int i = 1 ;i < s.size(); i++)
        {
            int l = i -1 , r = i + 1;
            while(l>=0 && r < s.size())
            {
                if(s[l] != s[r])
                    break;
                res++;
                l--;
                r++;
            }
            l = i -1;
            r = i;
            while(l>=0 && r < s.size())
            {
                if(s[l] != s[r])
                    break;
                res++;
                l--;
                r++;
            }
        }
        return res;
    }
};
