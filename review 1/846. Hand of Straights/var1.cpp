class Solution {
public:
    bool isNStraightHand(vector<int>& hand, int W)
    {
        if(hand.empty())
            return true;
        if(hand.size()%W!=0)
            return false;
        int min=0;
        sort(hand.begin(),hand.end());
        int now = hand[0];
        
        while(hand.size()>0)
        {
            now = hand[0];
            hand.erase(hand.begin());
            int j=0;
            for(int i=1;i<W;i++)
            {
                
                while(hand[j]==now)
                    j++;
                if(hand[j]!=now+1)
                    return false;
                now = hand[j];
                hand.erase(hand.begin()+j);
            }
        }
        return true;
    }
};
