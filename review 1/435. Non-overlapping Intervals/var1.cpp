class Solution {
public:
    int eraseOverlapIntervals(vector<vector<int>>& intervals)
    {
        int res = 0;
        auto sortI = [](const vector<int>& a,vector<int>& b)
        {
            return a[1] < b[1];
        };
        sort(intervals.begin(),intervals.end(),sortI);
        vector<vector<int>> temp;
        for(const auto& i:intervals)
        {
            if(temp.empty())
            {
                temp.push_back(i);
            }
            else
            {
                if(max(temp.back()[0],i[0]) < min(temp.back()[1],i[1]))
                {
                    res++;
                }
                else
                {
                    cout << i[0] <<" " << i[1]<<endl;
                    temp.push_back(i);
                }
            }
        }
        return res;
    }
};
