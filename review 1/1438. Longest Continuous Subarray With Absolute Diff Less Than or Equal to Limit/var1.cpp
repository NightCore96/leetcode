class Solution {
public:
    int longestSubarray(vector<int>& nums, int limit)
    {
        deque<pair<int,int>> maxdq,mindq;
        int res =1;
        int start = 0;
        int c = 0;
        for(int i = 0; i < nums.size();i++)
        {
            int end = nums[i];
            while(!maxdq.empty() && maxdq.back().first < end)
                maxdq.pop_back();
            while(!mindq.empty() && mindq.back().first > end)
                mindq.pop_back();
            maxdq.push_back({end,i});
            mindq.push_back({end,i});
            
            while(!maxdq.empty() && !mindq.empty() &&maxdq.front().first - mindq.front().first > limit)
            {
                if(maxdq.front().second == start)
                    maxdq.pop_front();
                if(mindq.front().second == start)
                    mindq.pop_front();
                start++;
            }
            
            res = max(res, i - start + 1);
            
        }
        return  res;
    }
};
