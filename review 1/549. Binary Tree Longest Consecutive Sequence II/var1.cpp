/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int longestConsecutive(TreeNode* root)
    {
        int res = 0;
        helper(root,res);
        return res;
    }
    
    void helper(TreeNode* root,int& res)
    {
        if(!root)
            return;
        int add = 0;
        int sub = 0;
        if(root->left)
        {
            if(root->left && root->val == root->left->val+1)
                add = helper_add(root->left);
            if(root->left && root->val == root->left->val-1)
                sub = helper_sub(root->left);
        }
        if(root->right)
        {
            if(root->right && root->val == root->right->val+1)
                add =max( add, helper_add(root->right));
            if(root->right && root->val == root->right->val-1)
                sub =max ( sub, helper_sub(root->right));
        }
        helper(root->left,res);
        helper(root->right,res);
        res = max (res,1 + add + sub );
    }
    
    int helper_add(TreeNode* root)
    {
        if(!root)
            return 0;
        int l = 0;
        int r = 0;
        if(root->left && root->val == root->left->val+1)
            l = helper_add(root->left);
        if(root->right && root->val == root->right->val+1)
            r = helper_add(root->right);
        return 1+max(l,r);
    }
    
    int helper_sub(TreeNode* root)
    {
        if(!root)
            return 0;
        int l = 0;
        int r = 0;
        if(root->left && root->val == root->left->val-1)
            l = helper_sub(root->left);
        if(root->right && root->val == root->right->val-1)
            r = helper_sub(root->right);
        return 1+max(l,r);
    }
};
