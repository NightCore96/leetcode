class Solution {
public:
    int largestRectangleArea(vector<int>& heights) 
    {
        int res = 0;
        stack<int> buffer;
        for(int i = 0;i<=heights.size();)
        {
            int curhi = i==heights.size()?0:heights[i];
            if(buffer.empty()||curhi>heights[buffer.top()])
            {
                buffer.push(i);
                i++;
            }
            else
            {
                int tophi = heights[buffer.top()];
                buffer.pop();
                int rbound = i-1;
                int lbound = buffer.empty()?0:buffer.top()+1;
                int rect = tophi*(rbound-lbound+1);
                res = max(res,rect);
                
            }
        }
        return res;
    }
};