/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode* head) 
    {
        if(!head)
            return true;
        front = head;
        return helper(head);
    }
    bool helper(ListNode* head)
    {
        if(!head)
            return true;
        if(helper(head->next)&&head->val == front->val)
        {
            front = front->next;
            return true;
        }
        return false;
    }
private:
    ListNode* front;
};