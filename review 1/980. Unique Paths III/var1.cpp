class Solution {
public:
    vector<vector<int>> dir = {{1,0},{-1,0},{0,1},{0,-1}};
    int uniquePathsIII(vector<vector<int>>& grid)
    {
        int res = 0;
        int target = 0;
        int x,y;
        vector<vector<int>> visit(grid.size(),vector<int>(grid[0].size(),0));
        for(int i =0;i<grid.size();i++)
        {
            for(int j =0;j<grid[i].size();j++)
            {
                if(grid[i][j] == 0)
                    target++;
                if(grid[i][j] == 1)
                {
                    x = i;
                    y = j;
                }
            }
        }
        dfs(res,target,x,y,grid,visit);
        return res;
    }
    
    void dfs(int &res,int target,int x,int y,vector<vector<int>>& grid,vector<vector<int>>& visit)
    {
        //cout << x <<" "<< y<<endl;
        if(grid[x][y] == 2)
        {
            if(target == -1)
                res++;
            return ;
        }
        //cout<< "?" <<endl;
        visit[x][y] =1;
        for(const auto& d:dir)
        {
            int xx = x+d[0];
            int yy = y+d[1];
            if(xx<0 || yy<0||xx>=grid.size()||yy>=grid[0].size() || visit[xx][yy] ==1)
                continue;
            if(grid[xx][yy] == 0 || grid[xx][yy] == 2)
                dfs(res,target -1,xx,yy,grid,visit);
        }
        visit[x][y] = 0;
    }
};
