class Solution {
public:
    int minDominoRotations(vector<int>& A, vector<int>& B)
    {
        vector<int> hashA(7,0);
        vector<int> hashB(7,0);
        vector<int> Same(7,0);
        int n = A.size();
        for(int i = 0; i < n;i++)
        {
            hashA[A[i]]++;
            hashB[B[i]]++;
            if(A[i] == B[i])
                Same[A[i]]++;
        }
        int res = INT_MAX;
        for(int i = 1; i <= 6;i++)
        {
            if(hashA[i]+hashB[i] - Same[i] == n)
            {
                res = min(res,min(hashA[i] - Same[i],hashB[i] - Same[i]));
            }
        }
        return res ==INT_MAX? -1 : res;
    }
}; 
