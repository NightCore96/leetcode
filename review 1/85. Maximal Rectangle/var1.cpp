class Solution {
public:
    int maximalRectangle(vector<vector<char>>& matrix) 
    {
        if(matrix.empty())
            return 0;
        int m = matrix.size();
        int n = matrix[0].size();
        vector<int> heights(n,0);
        int res = 0;
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(matrix[i][j]=='0')
                    heights[j]=0;
                else
                    heights[j]++;
            }
            int rect = getRectangle(heights);
            res = max(res,rect);
        }
        return res;
    }
    
    int getRectangle(vector<int> &heights)
    {
        int res = 0;
        stack<int> buffer;
        for(int i = 0; i <= heights.size();)
        {
            int hi = i==heights.size()?0:heights[i];
            if(buffer.empty()||hi>heights[buffer.top()])
            {
                buffer.push(i);
                i++;
            }
            else
            {
                int curhi = heights[buffer.top()];
                buffer.pop();
                int rbound = i-1;
                int lbound = buffer.empty()?0:buffer.top()+1;
                int rect = curhi*(rbound-lbound+1);
                res = max(res,rect);
            }
        }
        return res;
    }
    
};