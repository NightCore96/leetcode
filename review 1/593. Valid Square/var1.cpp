class Solution {
public:

    /*
     * return true if four points can make up a square by any way of formulation.
     * Only three possible scenarios if we iterate the square clockwise by starting from any point(assuming p1)
     * (p1, p2, p4, p3) or
     * (p1, p2, p3, p4) or
     * (p1, p3, p4, p2)
     */
    static bool validSquare(
            const vector<int> &p1,
            const vector<int> &p2,
            const vector<int> &p3,
            const vector<int> &p4) {
        return isValidSquareSorted(p1, p2, p3, p4)
               || isValidSquareSorted(p1, p2, p4, p3)
               || isValidSquareSorted(p1, p3, p4, p2);
    }

    /*
     * return true if these four points can make up a square
     * assuming the four points are (p1, p2, p4, p3) by clockwise iterating
     */
    static bool isValidSquareSorted(
            const vector<int> &p1,
            const vector<int> &p2,
            const vector<int> &p3,
            const vector<int> &p4) {
        return isValidTriangle(p1, p2, p3) && isValidTriangle(p4, p2, p3);
    }

    /*
     * return true if the triangle is isosceles right triangle with
     * (p2, p3) being the hypotenuse.
     */
    static bool isValidTriangle(const vector<int> &p1, const vector<int> &p2, const vector<int> &p3) {
        return squareDistance(p1, p2) == squareDistance(p1, p3)
               && (squareDistance(p1, p2) + squareDistance(p1, p3) == squareDistance(p2, p3))
               && squareDistance(p1, p2) != 0;
    }

    /*
     * calculate square euclidean distance between two point
     */
    static int squareDistance(const vector<int> &p1, const vector<int> &p2) {
        int diffX1 = p2[0] - p1[0];
        int diffY1 = p2[1] - p1[1];
        return diffX1 * diffX1 + diffY1 * diffY1;
    }
};
