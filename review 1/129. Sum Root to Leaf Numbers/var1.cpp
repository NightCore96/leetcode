/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int sumNumbers(TreeNode* root) 
    {
        int res = 0;
        helper(root,0,res);
        return res;
    }
    
    void helper(TreeNode* root,int pre,int &res)
    {
        if(!root)
            return;
        pre *= 10;
        pre += root->val;
        if(!root->left&&!root->right)
        {
            res+= pre;
            return;
        }
        helper(root->left,pre,res);
        helper(root->right,pre,res);
    }
};