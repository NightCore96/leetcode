class Solution {
public:
    int trap(vector<int>& height) 
    {
        if(height.empty())
            return 0;
        vector<int> buffer(height.size(),0);
        int l=0,r=0;
        int res=0;
        for(int i=0;i<buffer.size();i++)
        {
            buffer[i]=l;
            l = max(l,height[i]);
        }
        for(int i=buffer.size()-1;i>=0;i--)
        {
            buffer[i]=min(r,buffer[i]);
            r=max(r,height[i]);
            if(buffer[i]>height[i])
                res+= buffer[i]-height[i];
        }
        return res;
    }
};