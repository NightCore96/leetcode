class Solution {
public:
    int maxVacationDays(vector<vector<int>>& flights, vector<vector<int>>& days)
    {
        int week = days[0].size();
        int cites = flights[0].size();
        vector<vector<int>> dp(week+1,vector<int>(cites,-1));
        dp[0][0]=0;
        for(int i=1;i<=week;i++)
        {
           for(int j=0;j<cites;j++)
           {
               if(dp[i-1][j]<0)
                   continue;
               for(int k=0;k<cites;k++)
               {
                   if(flights[j][k]>0||j==k)
                   {
                       dp[i][k]=max(dp[i][k],dp[i-1][j]+days[k][i-1]);
                   }
               }
           }
        }
        int res = INT_MIN;
        for(int i=0;i<dp[week].size();i++)
        {
            res=max(res,dp[week][i]);
        }
        return res;
    }
};
