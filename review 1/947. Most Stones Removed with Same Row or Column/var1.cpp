class Solution {
public:
    int removeStones(vector<vector<int>>& stones)
    {
        unordered_map<int,vector<int>> mapX;
        unordered_map<int,vector<int>> mapY;
        for(auto& s:stones)
        {
            mapX[s[0]].push_back(s[1]);
            mapY[s[1]].push_back(s[0]);
        }
        int save_point = 0;
        unordered_set<string> visited;
        for(auto& s:stones)
        {
            int per = visited.size();
            dfs(s[0],s[1],mapX,mapY,visited);
            if(per != visited.size())
                save_point++;
            //cout<< visited.size() <<endl;
        }
        return stones.size() - save_point;
    }
    
    void dfs(int x,int y,unordered_map<int,vector<int>>& mapX,unordered_map<int,vector<int>>& mapY,unordered_set<string>& visited)
    {
        string str = to_string(x)+","+to_string(y);
        if(visited.count(str))
            return;
        visited.insert(str);
        for(int i : mapX[x])
        {
            dfs(x,i,mapX,mapY,visited);
        }
        for(int i : mapY[y])
        {
            dfs(i,y,mapX,mapY,visited);
        }
    }
};
