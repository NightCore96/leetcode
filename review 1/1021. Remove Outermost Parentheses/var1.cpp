class Solution {
public:
    string removeOuterParentheses(string S)
    {
        int start = 0;
        int count = 0;
        string res = "";
        for(int i = 0;i<S.size();i++)
        {
            if(S[i] == '(')
            {
                count++;
            }
            else if(S[i] == ')')
            {
                count--;
            }
            
            if(count == 0)
            {
                string temp = S.substr(start + 1, i - start -1);
                res += temp;
                start = i + 1;
            }
        }
        return res;
    }
};
