class Solution {
public:
    int numMatchingSubseq(string S, vector<string>& words)
    {
        int res = 0;
        unordered_map<string,int> hash;
        for(auto& w:words)
        {
            hash[w]++;
        }
        for(auto& p:hash)
        {
            if(isSubseq(S,p.first))
                res += p.second;
        }
        return res;
    }
    
    bool isSubseq(string& S,string T)
    {
        int it = 0;
        for(auto& c:T)
        {
            auto i = S.find_first_of(c,it);
            if( i == string::npos)
                return false;
            it = i+1;
        }
        return true;
    }
};
