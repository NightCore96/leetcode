class Solution {
public:
    bool isValid(string s) 
    {
        stack<char> mystack;
        for(char c:s)
        {
            switch(c)
            {
                    case '[':
                    case '(':
                    case '{':
                    {
                        mystack.push(c);
                        break;
                    }
                    case ']':
                    {
                        if(mystack.empty()||mystack.top()!='[')
                            return false;
                        mystack.pop();
                        break;
                    }
                    case ')':
                    {
                        if(mystack.empty()||mystack.top()!='(')
                            return false;
                        mystack.pop();
                        break;
                    }
                    case '}':
                    {
                        if(mystack.empty()||mystack.top()!='{')
                            return false;
                        mystack.pop();
                        break;
                    }
                default:
                    break;
            }
        }
        
        return mystack.empty();
    }
};