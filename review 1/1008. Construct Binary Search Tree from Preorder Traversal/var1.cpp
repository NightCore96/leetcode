/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* bstFromPreorder(vector<int>& preorder)
    {
        if(preorder.empty())
            return nullptr;
        int rootval = preorder[0];
        TreeNode* root = new TreeNode(rootval);
        vector<int> left;
        vector<int> right;
        for(int i = 1; i< preorder.size();i++)
        {
            if(preorder[i]< rootval)
                left.push_back(preorder[i]);
            else
                right.push_back(preorder[i]);
        }
        root->left = bstFromPreorder(left);
        root->right = bstFromPreorder(right);
        return root;
    }
};
