class Solution {
public:
    vector<string> fullJustify(vector<string>& words, int maxWidth) 
    {
        vector<string> res;
        queue<string> list;
        int now = 0;
        int i = 0;
        while(i < words.size())
        {
            int size = words[i].size();
            if(now + size + list.size() <= maxWidth)
            {
                now += size;
                list.push(words[i]);
                i++;
            }
            else
            {
                int space = maxWidth - now;
                now = 0;
                int each = 0;
                int extra = 0;
                if(list.size() > 1)
                {
                    each = space / (list.size()-1);
                    extra = space % (list.size()-1);
                }
                //cout<<each<<" "<<extra<<endl;
                string newstr = list.front();
                list.pop();
                while(!list.empty())
                {
                    for(int i = 0; i < each;i++)
                        newstr += " ";
                    if(extra > 0)
                    {
                        newstr += " ";
                        extra--;
                    }
                    newstr += list.front();
                    list.pop();
                }
                for(int i = newstr.size();i<maxWidth;i++)
                {
                    newstr += " ";
                }
                res.push_back(newstr);
            }
        }
        if(!list.empty())
        {
            string newstr = list.front();
            list.pop();
            while(!list.empty())
            {
                newstr += " ";
                newstr += list.front();
                list.pop();
            }
            for(int i = newstr.size();i<maxWidth;i++)
            {
                newstr += " ";
            }
            res.push_back(newstr);
        }
        return res;
    }
};