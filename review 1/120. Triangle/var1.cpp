class Solution {
public:
    int minimumTotal(vector<vector<int>>& triangle) 
    {
        int res = INT_MAX;
        int end = triangle.size();
        int sum = 0;
        if(end == 0)
            return 0;
        if(end == 1)
            return triangle[0][0];
        for(int i = 1; i <end ;i++)
        {
            for(int j = 0; j<= i;j++)
            {
                if(j==0)
                {
                    triangle[i][j] += triangle[i-1][j];
                }
                else if(j==i)
                {
                    triangle[i][j] += triangle[i-1][i-1];
                }
                else
                {
                    triangle[i][j] += min(triangle[i-1][j-1],triangle[i-1][j]);
                }
                if(i == end - 1)
                {
                    res = min(res,triangle[i][j]);
                }
            }
        }
        return res;
    }
};