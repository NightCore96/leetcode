class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) 
    {
        vector<int> res;
        if(nums.empty())
            return res;
        int size = nums.size();
        res = vector<int>(size,1);
        int start = 1,end = 1;
        for(int i =0; i <size ;i++)
        {
            res[i] *= start;
            start *= nums[i];
            res[size-1-i] *= end;
            end *= nums[size-1-i];
        }
        return res;
    }
};