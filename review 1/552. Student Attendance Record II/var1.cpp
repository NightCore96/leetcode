class Solution {
public:
    int checkRecord(int n)
    {
        int mod = 1e9 + 7;
        vector<int> p(n+1,0),l(n+1,0),a(n+1,0);
        if(n>=1)
        {
            p[1] = 1;
            l[1] = 1;
            a[1] = 1;
        }
        if(n>=2)
        {
            p[2] = 3; // pp lp ap
            l[2] = 3; // pl al ll
            a[2] = 2; // pa la
        }
        if(n >= 3)
        {
            a[3] = 4;
        }
        for(int i = 3; i<=n ;i++)
        {
            p[i] = ((p[i-1] % mod + l[i-1] % mod ) % mod + a[i-1] % mod) % mod;
            l[i] = ((p[i-1] % mod + p[i-2] % mod ) % mod +( a[i-1] % mod + a[i-2] % mod) % mod ) % mod;
            if( i > 3)
                a[i] = ((a[i-1] % mod + a[i-2] % mod) % mod + a[i-3] % mod) % mod;
        }
        return ((a[n] % mod+ l[n] % mod) % mod  + p[n] % mod) % mod;
    }
};
