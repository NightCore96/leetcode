class Solution {
public:
    int minSumOfLengths(vector<int>& arr, int target)
    {
        vector<int> sums(1,0);
        int sum = 0;
        for(const auto& a: arr)
        {
            sum += a;
            sums.push_back(sum);
        }
        
        unordered_map<int,int> table;
        vector<int> dp(arr.size() + 1,0);
        int res = INT_MAX;
        for(int i = 0; i < sums.size();i++)
        {
            int diff = sums[i] - target;
            dp[i] = i > 0 ? dp[i-1] : 0;
            if(table.count(diff))
            {
                int len = i - table[diff];
                if(dp[table[diff]] > 0)
                    res = min(res, len + dp[table[diff]]);
                dp[i] = dp[i] == 0 ? len : min(dp[i],len);
            }
            table[sums[i]] = i;
        }
        
        
        return res == INT_MAX ?  -1 : res;
    }
};
