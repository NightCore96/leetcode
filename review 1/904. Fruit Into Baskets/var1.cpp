class Solution {
public:
    int totalFruit(vector<int>& tree)
    {
        int max_b = 2;
        unordered_map<int,int> basket;
        int res = 0;
        int start = 0;
        for(int i = 0;i< tree.size();i++)
        {
            int t = tree[i];
            if(basket.size() < max_b)
            {
                basket[t] = i;
                 //cout << t <<"?"<< endl;
            }
            else
            {
                if(basket.count(t))
                {
                    basket[t] = i;
                    //cout << t << endl;
                }
                else
                {
                    int min_id;
                    int min_pos = INT_MAX;
                    for(auto& p:basket)
                    {
                        if(p.second < min_pos)
                        {
                            min_pos = p.second;
                            min_id = p.first;
                        }
                    }
                    basket.erase(min_id);
                    //cout <<min_id<<" "<< i << " " << start <<endl;
                    res = max(res,i-start);
                    start = min_pos+1;
                    basket[t] = i;
                }
            }
        }
        int len = tree.size()-start;
        res = max(res,len);
        return res;
    }
};
