class Solution {
public:
    string minWindow(string s, string t) 
    {
        int hashtable[256] = {0};
        for(auto c:t)
        {
            hashtable[c]++;
        }
        int count = 0;
        string res = "";
        int minLen = INT_MAX;
        int start = 0;
        for(int i = 0;i<s.size();i++)
        {
            if(--hashtable[s[i]]>=0)
                count++;
            while(count==t.size())
            {
                if(minLen>i-start+1)
                {
                    minLen = i-start+1;
                    res = s.substr(start,minLen);
                }
                 if(++hashtable[s[start]]>0)
                    count--;
                start++;
            }
        }
        return res;
    }
};