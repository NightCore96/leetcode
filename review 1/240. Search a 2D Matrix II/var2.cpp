class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) 
    {
        if(matrix.empty()||matrix[0].empty())
            return false;
        int h = matrix.size();
        int w = matrix[0].size();
        int x = h-1;
        int y = 0;
        
        while(x >= 0 && y < w)
        {
            if(matrix[x][y] == target)
                return true;
            else if(matrix[x][y] < target)
                y++;
            else
                x--;
        }
        return false;
    }
};