class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) 
    {
        if(matrix.empty()||matrix[0].empty())
            return false;
        int h = matrix.size();
        int w = matrix[0].size();
        
        for(int i = 0 ; i < h ;i++)
        {
            int start = 0;
            int end = w - 1;
            while(start <= end)
            {
                int mid = (end - start)/2 + start;
                if(matrix[i][mid] == target)
                    return true;
                else if(matrix[i][mid] < target)
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid - 1;
                }
            }
        }
        
        for(int i = 0 ; i < w ;i++)
        {
            int start = 0;
            int end = h - 1;
            while(start <= end)
            {
                int mid = (end - start)/2 + start;
                if(matrix[mid][i] == target)
                    return true;
                else if(matrix[mid][i] < target)
                {
                    start = mid + 1;
                }
                else
                {
                    end = mid - 1;
                }
            }
        }
        return false;
    }
};