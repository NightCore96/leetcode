class Solution {
public:
    int expressiveWords(string S, vector<string>& words)
    {
        int res = 0;
        for(auto& w: words)
        {
            if(isSame(S,w))
                res++;
        }
        return res;
    }
    
    bool isSame(string& s,string& word)
    {
        int i = 0, j = 0;
        int m = s.size(),n = word.size();
        while(i < m)
        {
            if(j < n && s[i] == word[j])
            {
                j++;
            }
            else if(i > 0 && s[i - 1] == s[i] && i < m - 1 && s[i] == s[ i + 1]) // more 3
            {
                i++;
            }
            else if(!(i>1 && s[i - 2] == s[i] && s[i - 1] == s[i]))
            {
                break;
            }
            i++;
        }
        return (i == m && j == n);
    }
};
