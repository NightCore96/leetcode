/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int treeHieght(TreeNode* root)
    {
        if(!root)
            return 0;
        return 1 + max(treeHieght(root->left),treeHieght(root->right));
    }
    bool isBalanced(TreeNode* root) 
    {
        if(!root)
            return true;
        int l = treeHieght(root->left);
        int r = treeHieght(root->right);
        if(abs(l-r)>1)
            return false;
        return isBalanced(root->left)&&isBalanced(root->right);
    }
};