class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) 
    {
        
        int m = INT_MIN;
        vector<int> res;
        if(nums.empty())
            return res;
        int size = nums.size();
        for(int i = 0; i < k;i++)
        {
            m = max(nums[i],m);
        }
        res.push_back(m);
        for(int i = 1 ; i < size - k + 1 ;i++)
        {
            if(m == nums[i - 1]) //remove head;
            {
                m = nums[i];
               for(int j = i; j < i - 1 + k; j++) 
               {
                   m = max(m,nums[j]);
               }
            }
            if(m < nums[i + k - 1]) //add tail
            {
                m = nums[i + k - 1];
            }
            res.push_back(m);    
        }
        return res;
    }
};