class Solution {
public:
    int splitArray(vector<int>& nums, int m)
    {
        long long right = 0;
        int left = INT_MIN;
        for(const auto& n:nums)
        {
            right+= n;
            left = max(left,n);
        }
        
        while(left < right)
        {
            
            long long mid =left + (right - left) / 2;
            //cout<< left <<" "<< right << " "<<mid<<endl;
            if(isOK(nums,m,mid))
                right = mid;
            else
                left = mid + 1;
                
        }
        return left;
        
    }
    
    bool isOK(vector<int>& nums, int m , long long sum)
    {
        long long cur = 0;
        int total = 1;
        for(const auto& n:nums)
        {
            cur+= n;
            if(cur > sum)
            {
                total++;
                cur = n;
                if(total > m)
                    return false;
            }
        }
        return true;
    }
};
