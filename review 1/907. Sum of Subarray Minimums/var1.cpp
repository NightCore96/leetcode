class Solution {
public:
    int sumSubarrayMins(vector<int>& A)
    {
        int mod = 1e9 + 7;
        int res = 0;
        stack<int> stk;
        for(int i = 0;i<= A.size();i++)
        {
            int cur =  i == A.size() ? 0 : A[i];
            while(!stk.empty() && A[stk.top()] > cur)
            {
                int temp = stk.top();
                stk.pop();
                int left = temp - (stk.empty() ? -1 : stk.top());
                int right = i - temp;
                res = (res + A[temp] * left * right) % mod;
            }
            stk.push(i);
        }
        return res;
    }
};
