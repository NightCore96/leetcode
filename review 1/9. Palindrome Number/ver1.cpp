class Solution {
public:
    bool isPalindrome(int x) 
    {
        if(x<0)
            return false;
        return x==reverse(x);
    }
    int reverse(int x) 
    {
        int res = 0;
        while(x != 0)
        {
            //cout<<res;
            int mod = x%10;
            if(res > INT_MAX/10||res < INT_MIN/10||(res == INT_MIN/10 && mod < -8)||(res == INT_MAX/10 && mod > 7))
                return 0;
            res = res * 10 + mod;
            x = x/10;
        }
        return res;
    }
};