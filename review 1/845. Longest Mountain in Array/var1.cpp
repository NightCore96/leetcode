class Solution {
public:
    int longestMountain(vector<int>& A)
    {
        int res =  0;
        if(A.size() < 3)
            return 0;
        for(int i=1; i<A.size()-1;i++)
        {
            if(A[i] > A[i-1] && A[i] > A[i+1])
            {
                int len = 0;
                int l = i;
                while(l>0 &&A[l] > A[l-1])
                {
                    l--;
                    len ++;
                }
                int r = i;
                while(r<A.size()-1&&A[r] > A[r+1])
                {
                    r++;
                    len ++;
                }
                len++;
                res = max(res,len);
            }
        }
        return res;
    }
};
