
class Solution {
public:
    int confusingNumberII(int N) {
        int count=0;
        search(0,N,count);
        return count;
    }
    
    void search(long long cur, int N, int& count)
    {
        if(cur>N) return;
        if(cur!=rotate(cur)) count++;
        if(cur>0) search(cur*10,N,count);
        search(cur*10+1,N,count);
        search(cur*10+6,N,count);
        search(cur*10+8,N,count);
        search(cur*10+9,N,count);
    }
    
    long long rotate(long long n)
    {
        long long m=0;
        while(n>0)
        {
            int x=n%10;
            if(x==6) x=9;
            else if(x==9) x=6;
            m*=10;
            m+=x;
            n/=10;
        }
        return m;
    }
};
