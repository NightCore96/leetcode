/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    vector<Interval> merge(vector<Interval>& intervals) 
    {
        vector<Interval> res;
        auto sortInterval = [](Interval &a,Interval &b){
            return a.start<b.start;
        };
        sort(intervals.begin(),intervals.end(),sortInterval);
        for(auto i:intervals)
        {
            if(res.empty())
                res.push_back(i);
            else
            {
                auto b = res.back();
                if(max(i.start,b.start)<=min(i.end,b.end))
                {
                    res.back().start= min(i.start,b.start);
                    res.back().end = max(i.end,b.end);
                }
                else
                {
                    res.push_back(i);
                }
            }
        }
        return res;
    }
};