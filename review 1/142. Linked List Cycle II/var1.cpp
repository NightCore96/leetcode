/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) 
    {
        if(!head)
            return NULL;
        ListNode *low = head;
        ListNode *fast = head;
        while(low&&fast&&fast->next)
        {
            low = low->next;
            fast = fast->next->next;
            if(low == fast)
            {
                low = head;
                while(low!=fast)
                {
                    if(fast == head)
                        return fast;
                    low = low->next;
                    fast = fast->next;
                }
                return low;
            }
        }
        return NULL;
    }
};