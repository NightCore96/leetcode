class Solution {
public:
    string reorganizeString(string S)
    {
        string res = "";
        unordered_map<char,int> table;
        for(char& c:S)
        {
            table[c]++;
        }
        priority_queue<pair<int,char>> pq;
        for(auto&p : table)
        {
            pq.push({p.second,p.first});
        }
        while(!pq.empty())
        {
            auto p = pq.top();
            pq.pop();
            if(pq.empty())
            {
                res += p.second;
                break;
            }
            auto q = pq.top();
            pq.pop();
            while(p.first > 0 && q.first > 0)
            {
                res += p.second;
                res += q.second;
                p.first--;
                q.first--;
            }
            if(p.first > 0)
                pq.push(p);
            if(q.first > 0)
                pq.push(q);
        }
        return S.size() == res.size() ? res : "";
    }
    
   
};
