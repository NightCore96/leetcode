class Solution {
public:
    bool isInterleave(string s1, string s2, string s3) 
    {
        if(s1.empty()&&s2.empty())
            return s3.empty();
        int size1 = s1.size();
        int size2 = s2.size();
        if(size1+size2 != s3.size())
            return false;
        vector<vector<bool>> dp(size1+1,vector<bool>(size2+1,false));
        dp[0][0] = true;
        for(int i = 1; i <= size1; i++)
        {
            dp[i][0] = dp[i - 1][0] && s1[i - 1] == s3[i - 1];
        }
        for(int i = 1; i <= size2; i++)
        {
            dp[0][i] = dp[0][i - 1] && s2[i - 1] == s3[i - 1];
        }
        
        for(int i = 1; i <= size1; i++)
        {
            for(int j = 1; j <= size2; j++)
            {
                dp[i][j] =  (dp[i - 1][j] && s1[i - 1] == s3[i - 1 + j])||(dp[i][j - 1] && s2[j - 1] == s3[i - 1 + j]);
            }
        }
        return dp[size1][size2];
    }
};