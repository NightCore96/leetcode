class Solution {
public:
    int minimumSemesters(int N, vector<vector<int>>& relations)
    {
        unordered_map<int,vector<int>> graph;
        for(const auto& r:relations)
        {
            graph[r[1]].push_back(r[0]);
        }
        vector<int> visited(N+1,0);
        vector<int> dp(N+1,1);
        int res = 1;
        for(int i = 1;i<=N;i++)
        {
            if(graph.count(i) && !dfs(graph,i,visited,dp))
                return -1;
            res = max(res,dp[i]);
        }
        return res;
    }
    
    bool dfs(unordered_map<int,vector<int>>& graph,int now,vector<int>& visit,vector<int>& dp)
    {
        if(visit[now] == 1)
            return false;
        if(visit[now] == -1)
            return true;
        visit[now] = 1;
        for(const auto& v:graph[now])
        {
            if(!dfs(graph,v,visit,dp))
                return false;
            dp[now] = max(dp[now],dp[v]+1);
        }
        visit[now] = -1;
        return true;
    }
};
