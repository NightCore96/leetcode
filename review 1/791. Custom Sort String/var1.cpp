class Solution {
public:
    string customSortString(string S, string T)
    {
        static unordered_map<char,int> list;
        for(int i = 0;i< S.size();i++)
        {
            list[S[i]] = i;
        }
        auto sortS = [](char& a, char& b)
        {
            if(!list.count(a)&&!list.count(b))
                return a < b;
            if(!list.count(a))
                return false;
            if(!list.count(b))
                return true;
            return list[a] < list[b];
        };
        sort(T.begin(),T.end(),sortS);
        return T;
    }
};
