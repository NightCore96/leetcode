class Solution {
public:
    vector<int> deckRevealedIncreasing(vector<int>& deck) 
    {
        vector<int> res;
        if(deck.empty())
            return res;
        int size = deck.size();
        sort(deck.begin(),deck.end());
        queue<int> q;
        for(int i =0 ; i < size;i++)
        {
            q.push(i);
        }
        res = vector<int>(size);
        for(int card : deck)
        {
            res[q.front()] = card;
            q.pop();
            if(!q.empty())
            {
                q.push(q.front());
                q.pop();
            }
                
        }
        return res;
    }
};