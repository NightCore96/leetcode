class Solution {
public:
    vector<string> findItinerary(vector<vector<string>> tickets)
    {
        unordered_map<string,priority_queue<string,vector<string>,greater<string>>> airline;
        for(auto p:tickets)
        {
            airline[p[0]].push(p[1]);
        }
        vector<string> res;
        dfs(res,airline,"JFK");
        return vector<string>(res.rbegin(),res.rend());
    }
    
    void dfs(vector<string> &res,unordered_map<string,priority_queue<string,vector<string>,greater<string>>> &airline,string now)
    {
        
        while(!airline[now].empty())
        {
            string next = airline[now].top();
            airline[now].pop();
            dfs(res,airline,next);
        }
        res.push_back(now);
    }
};
