class Solution {
public:
    vector<int> asteroidCollision(vector<int>& asteroids)
    {
        vector<int> res;
        stack<int> s;
        for(auto& a:asteroids)
        {
            bool isLive = true;
            while(!s.empty() && a < 0 && s.top() > 0)
            {
                if(-1 * a < s.top())
                {
                    isLive = false;
                    break;
                }
                else if(-1 * a == s.top())
                {
                    isLive = false;
                    s.pop();
                    break;
                }
                else
                {
                    s.pop();
                }
            }
            if(isLive)
                s.push(a);
        }
        while(!s.empty())
        {
            res.insert(res.begin(),s.top());
            s.pop();
        }
        return res;
    }
};
