class Solution {
public:
    unordered_set<int> table;
    bool isHappy(int n) {
        if(n == 1)
            return true;
        if(n == 0 || table.count(n))
            return false;
        table.insert(n);
        int next = 0;
        while( n > 0 )
        {
            next += (n % 10)*(n % 10);
            n = n / 10;
        }
        return isHappy(next);
    }
};