class Solution
{
private:
    vector<int> original;
public:
    Solution(vector<int>& nums) {
        original = nums;
    }
    
    /** Resets the array to its original configuration and return it. */
    vector<int> reset()
    {
        return original;
    }
    
    /** Returns a random shuffling of the array. */
    vector<int> shuffle()
    {
        vector<int> res;
        vector<int>  temp = original;
        while(!temp.empty())
        {
            int i = rand() % temp.size();
            int n = temp[i];
            swap(temp[i],temp[temp.size() - 1]);
            res.push_back(n);
            temp.pop_back();
        }
        return res;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(nums);
 * vector<int> param_1 = obj->reset();
 * vector<int> param_2 = obj->shuffle();
 */
