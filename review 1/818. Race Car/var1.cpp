class Solution {
public:
    int racecar(int target)
    {
        int res = 0;
        queue<pair<int,long long>> q;
        unordered_set<string> visited;
        q.push({0,1});
        while(!q.empty())
        {
            int size = q.size();
            for(int i=0;i<size;i++)
            {
                pair<int,long long> p = q.front();
                q.pop();
                if(p.first==target)
                {
                    return res;
                }
                pair<int,long long> A = {p.first+p.second,p.second*2};
                q.push(A);
                if(p.second > 0 && (p.first + p.second) > target)
                    q.push({p.first, -1});
                else if(p.second <= 0 && (p.first + p.second) < target)
                    q.push({p.first, 1});
            }
            res++;
        }
        return -1;
        
    }
    
    
};
