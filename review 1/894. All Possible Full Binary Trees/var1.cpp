/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<TreeNode*> allPossibleFBT(int N)
    {
        if(N < 1 || N % 2 == 0)
            return {};
        if(N == 1)
            return {new TreeNode(0)};
        //unordered_map<int,vector<TreeNode*>) map;
        vector<TreeNode*> res;
        for(int i = 1;i<=N-2; i+=2)
        {
            int r = N - 2 - i + 1;
            vector<TreeNode*> left;
            vector<TreeNode*> right;
            left = allPossibleFBT(i);
            right = allPossibleFBT(r);
            for(int i = 0;i<left.size();i++)
            {
                for(int j =0;j<right.size();j++)
                {
                    TreeNode* root = new TreeNode(0,left[i],right[j]);
                    res.push_back(root);
                }
            }
        }
        return res;
    }
};
