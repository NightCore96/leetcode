class Solution {
public:
    bool isMatch(string s, string t) 
    {
        int i = s.length();
        int j = t.length();
        return helper(s,t,i,j);
    }
    // call by reference and don't copy data
    bool helper(string &s,string &t,int i,int j)
    {
        if(i==0&&j==0)
            return true;
        if(i!=0&&j==0)
            return false;
        if(i==0&&j!=0)
        {
            if(t[j-1]=='*')
                return helper(s,t,i,j-2);
            return false;
        }
        
        if(s[i-1]==t[j-1]||t[j-1]=='.')
            return helper(s,t,i-1,j-1);
        else if(t[j-1]=='*')
        {
            if(helper(s,t,i,j-2))
                return true;
            if(s[i-1]==t[j-2]||t[j-2]=='.')
                return helper(s,t,i-1,j);
            return false;
        }
        return false;
    }
    
    
};