class Solution {
public:
    vector<vector<int>> palindromePairs(vector<string>& words) {
        vector<vector<int>> result;
        unordered_map<string,int> hash;
        if(words.empty())
            return result;
        for(int i=0;i<words.size();i++)
        {
            hash.insert(pair<string,int>(words[i],i));
        }
        if(hash.find("")!=hash.end())
        {
            for(int i=0;i<words.size();i++)
            {
               if(isPalindrome(words[i])&&i!=hash[""])
               {
                   result.push_back({i,hash[""]});
               }
            }
        }
        for(int i=0;i<words.size();i++)
        {
            for(int j=0;j<words[i].size();j++)
            {
                string s = words[i].substr(j+1);
                string t = words[i].substr(0,j+1);
                if(isPalindrome(s))
                {
                    reverse (t.begin(),t.end());
                    if(hash.find(t)!=hash.end())
                    {
                        if(i!=hash[t])
                            result.push_back({i,hash[t]});
                    }
                }
                if(isPalindrome(t))
                {
                    reverse (s.begin(),s.end());
                    if(hash.find(s)!=hash.end())
                    {
                        if(i!=hash[s])
                            result.push_back({hash[s],i});
                    }
                }
            }
        }
        
        return result;
    }
    
    bool isPalindrome(string s)
    {
        if(s=="")
           return true;
        int i=0,j=s.length()-1;
        while(i<=j)
        {
            if(s[i]!=s[j])
                return false;
            i++;
            j--;
        }
        return true;
    }
};
