/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int helper(TreeNode* root , int &res)
    {
        if(!root)
            return 0;
        int l = helper(root->left,res) ;
        int r = helper(root->right,res);
        int maxh = max(l,r);
        res =max(res, l+r);
        return 1 + maxh;
    }
    int diameterOfBinaryTree(TreeNode* root)
    {
        int res = 0;
        helper(root,res);
        return res;
    }
};
