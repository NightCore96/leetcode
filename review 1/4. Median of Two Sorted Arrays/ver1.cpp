class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) 
    {
        vector<int> nums(nums1.size()+nums2.size(),0);
        int i1=0,i2=0,i=0;
        while(i1<nums1.size()&&i2<nums2.size())
        {
            nums[i++] = (nums1[i1]<=nums2[i2])?nums1[i1++]:nums2[i2++]; 
        }
        while(i1<nums1.size())
            nums[i++] = nums1[i1++];
        while(i2<nums2.size())
            nums[i++] = nums2[i2++];
        if(nums.size()%2!=0)
        {
            int mid = nums.size()/2;
            return nums[mid];
        }
        else
        {
            return (nums[(nums1.size()+nums2.size())/2-1]+nums[(nums1.size()+nums2.size())/2])/2.0;
        }
    }
};