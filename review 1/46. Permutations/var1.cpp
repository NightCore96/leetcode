class Solution {
public:
    vector<vector<int>> permute(vector<int>& nums) 
    {
        vector<vector<int>> result;
        returnIndex(0,nums.size(),nums,result);
        return result;
    }
    void returnIndex(int n,int size,vector<int>& nums, vector<vector<int>> &result)
    {
        if(n==size)
        {
            result.push_back(nums);
            return;
        }
        for(int i = n; i <size;i++)
        {
            swap(nums[n],nums[i]);
            returnIndex(n+1,size,nums,result);
            swap(nums[n],nums[i]);
        }
    }
};