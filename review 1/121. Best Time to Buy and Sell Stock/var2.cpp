class Solution {
public:
    int maxProfit(vector<int>& prices) 
    {
        if(prices.empty())
            return 0;
        int size = prices.size();
        int res = 0;
        int m = prices[0];
        for(int i=1;i<size;i++)
        {
           m = min(m,prices[i]);
           if(prices[i]-m>res)
           {
               res = prices[i]-m;
           }
        }
        
        return res;
    }
};