class Solution {
public:
    int maxProfit(vector<int>& prices) 
    {
        if(prices.empty())
            return 0;
        int size = prices.size();
        int res = INT_MIN;
        for(int i=1;i<size;i++)
        {
            for(int j = i-1;j>=0;j--)
            {
                int b = prices[i]-prices[j];
                res = max(res,b);
            }
        }
        
        return res<0?0:res;
    }
};