/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int K) 
    {
        ListNode* pre = head;
        for(int i = 0;i < K; i++)
        {
            if(!pre)
                return head;
            pre = pre->next;
        }
        ListNode* node = head;
        ListNode* next = head->next;
        for(int i = 0;i < K; i++)
        {
            next = node->next;
            node->next = pre;
            pre = node;
            node = next;
        }
        head->next = reverseKGroup(head->next,K);
        return pre;
    }
};