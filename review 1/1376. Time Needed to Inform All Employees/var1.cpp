class Solution {
public:
    int numOfMinutes(int n, int headID, vector<int>& manager, vector<int>& informTime)
    {
        int res;
        unordered_map<int,vector<int>> flow;
        for(int i = 0; i < n;i++)
        {
            int boss = manager[i];
            if(boss == -1)
                continue;
            flow[boss].push_back(i);
        }
        unordered_set<int> visited;
        
        return dfs(flow,headID,visited,informTime);
    }
    
    int dfs(unordered_map<int,vector<int>>& flow,int now,unordered_set<int>& visited,vector<int>& informTime)
    {
        if(visited.count(now))
            return 0;
        int timer = 0;
        visited.insert(now);
        if(!flow.count(now))
            return 0;
        for(const auto& i:flow[now])
        {
            timer = max(timer,dfs(flow,i,visited,informTime));
        }
        return informTime[now] + timer;
    }
};
