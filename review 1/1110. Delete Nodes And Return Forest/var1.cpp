/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& to_delete) 
    {
        vector<TreeNode*> res;
        if(root == nullptr)
             return res;
        TreeNode* head = root;
        unordered_set<int> table;
        for(auto i : to_delete)
        {
            table.insert(i);
        }
        queue<TreeNode*> q;
        q.push(head);
        while(!q.empty())
        {
            for(int i = q.size() ; i > 0 ; i--)
            {
                auto node = q.front();
                q.pop();
                if(table.count(node->val))
                {
                    if(node->left)
                    {
                        q.push(node->left);
                    }
                    if(node->right)
                    {
                       q.push(node->right); 
                    }
                    node-> left = nullptr;
                    node-> right = nullptr;
                    //delete node; 
                }
                else
                {
                    res.push_back(node);
                    vector<TreeNode*> list;
                    helper(nullptr,node,table,list);
                    for(auto tn : list)
                    {
                        q.push(tn);
                    }
                }
            }
        }
        return res;
    }
    
    void helper(TreeNode* pre,TreeNode* root,unordered_set<int>& table,vector<TreeNode*> &list)
    {
        if(root == nullptr)
             return;
        if(table.count(root->val))
        {
            if(root->left)
            {
                list.push_back(root->left);
            }
            if(root->right)
            {
               list.push_back(root->right); 
            }
            if(pre != nullptr)
            {
                if(pre->left == root)
                    pre->left = nullptr;
                else
                    pre->right = nullptr;
            }
            root->left = nullptr;
            root->right = nullptr;
            //delete root;
        }
        else
        {
            helper(root,root->left,table,list);
            helper(root,root->right,table,list);
        }
    }
};