class Solution {
public:
    int firstMissingPositive(vector<int>& nums) 
    {
        int lessZero = 0;
        int size = nums.size();
        for(int i = 0;i < size; i++)
        {
            if(nums[i]<=0)
            {
                swap(nums[i],nums[lessZero]);
                lessZero++;
            }
        }
        if(lessZero == size)
            return 1;
        for(int i = lessZero; i < size; i++)
        {
            int j  = abs(nums[i]);
            if(j > size)
                continue;
            j = j + lessZero - 1 ;
            if(j < size && nums[j] >0)
                nums[j] = -nums[j];
        }
        for(int i = lessZero; i < size; i++)
        {
            if(nums[i] > 0)
                return i - lessZero + 1;
        }
        return size - lessZero + 1;
    }
};