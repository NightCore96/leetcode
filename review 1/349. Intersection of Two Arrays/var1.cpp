class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2)
    {
        unordered_set<int> hash(nums1.begin(),nums1.end());
        unordered_set<int> res;
        for(auto& n:nums2)
        {
            if(hash.count(n))
                res.insert(n);
        }
        return vector<int>(res.begin(),res.end());
    }
};
