class Solution {
public:
    string findLongestWord(string s, vector<string>& d)
    {
        string res = "";
        sort(d.begin(),d.end(),
             [](string& a,string& b)
             {
                 if(a.size() == b.size())
                     return a < b;
                 return a.size() > b.size();
             });
        for(string& t:d)
        {
            int j = 0;
            for(char& c:s)
            {
                if(c == t[j])
                    j++;
            }
            if( j == t.size())
                return t;
        }
        return res;
    }
    
};
