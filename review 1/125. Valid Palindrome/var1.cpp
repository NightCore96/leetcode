class Solution {
public:
    bool isPalindrome(string s) 
    {
        if(s.empty())
            return true;
        int size = s.size();
        int i = 0;
        int j = size - 1;
        while(i<=j)
        {
            if(!isalpha(s[i])&&!isdigit(s[i]))
            {
                i++;
                continue;
            }
            if(!isalpha(s[j])&&!isdigit(s[j]))
            {
                j--;
                continue;
            }
            char theI = s[i];
            char theJ = s[j];
            if((s[i]>='A'&&s[i]<='Z'))
            {
                theI= theI-'A'+'a';
            }
            if((s[j]>='A'&&s[j]<='Z'))
            {
                theJ= theJ-'A'+'a';
            }
            if(theI!=theJ)
                return false;
            i++;
            j--;
        }
        return true;
    }
};