class Solution {
public:
    int maxEvents(vector<vector<int>>& events) 
    {
        auto sorte = [](vector<int> &a , vector<int> &b)
        {
            return a[1] < b[1];
        };
        sort(events.begin(),events.end(),sorte);
        unordered_set<int> table;
        for(auto e:events)
        {
            for(int i = e[0] ;i <=e[1] ; i++)
            {
                if(!table.count(i))
                {
                    table.insert(i);
                    break;
                }
                    
            }
        }
        return table.size();
    }
};