class Solution {
public:

    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) 
    {
        sort(people.begin(),people.end(),[](const auto &a,const auto &b)
        {
             return ( a[0] < b[0] ) ? true : (a[0] == b[0] and a[1] > b[1]) ; 
        });
        vector<vector<int>> res(people.size());
        for(auto p:people)
        {
            int cnt = p[1];
            for(int i = 0 ; i < people.size() ; i ++)
            {
                if( cnt == 0 and res[i].size() == 0 ) 
                {
                    res[i] = p;
                    break;
                }
                if(res[i].size() == 0)
                   cnt--;
            }
        }
        return res;
    }
};