class Solution {
public:
    int maxSubArray(vector<int>& nums) 
    {
        int Mx = INT_MIN;
        int underZero = 0;
        int n = nums.size();
        int sum = 0;
        int res = INT_MIN;
        for(int n:nums)
        {
            if(n<0)
            {
                underZero++;
                Mx = max(Mx,n);
            }
            sum += n;
            sum = max(0,sum);
            res = max(res,sum);
        }
        if(underZero==n)
            return Mx;
        return res;
    }
};