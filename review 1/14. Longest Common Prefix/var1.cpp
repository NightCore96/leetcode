class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) 
    {
        if(strs.empty())
            return "";
        string res = strs[0];
        for(string str:strs)
        {
            if(res.empty())
                break;
            for(int i = 0;i < res.size();i++)
            {
                if(str[i] != res[i])
                {
                    res.resize(i);
                    break;
                }
            }
        }
        return res;
    }
};