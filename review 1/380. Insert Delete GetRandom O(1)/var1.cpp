class RandomizedSet
{
private:
    unordered_set<int> buffer;
    
public:
    /** Initialize your data structure here. */
    RandomizedSet() {
        
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    bool insert(int val)
    {
        if(buffer.count(val))
            return false;
        buffer.insert(val);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    bool remove(int val)
    {
        if(!buffer.count(val))
            return false;
        buffer.erase(val);
        return true;
    }
    
    /** Get a random element from the set. */
    int getRandom()
    {
        auto it = buffer.begin();
        int i = rand() % buffer.size();
        advance(it,i);
        int res = *it;
        return res;
    }
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */
