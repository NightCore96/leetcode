class Solution {
public:
    string countAndSay(int n) 
    {
        if(n == 1)
             return "1";
        if(n == 2)
             return "11";
        string pre = countAndSay(n - 1);
        string res = "";
        char c = pre[0];
        int count = 0;
        for(auto &p:pre)
        {
            if(p != c)
            {
                res += to_string(count)+c;
                c = p;
                count = 1;
            }
            else
            {
                count++;
            }
        }
        res += to_string(count)+c;
        return res;
    }
};