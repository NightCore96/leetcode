/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) 
    {
        if(!root)
            return {};
        vector<vector<int>> res;
        queue<pair<TreeNode*,int>> q;
        vector<int> level;
        q.push({root,0});
        while(!q.empty())
        {
            auto p = q.front();
            q.pop();
            if(p.second > res.size())
            {
                if((res.size()+1) % 2 == 0)
                    reverse(level.begin(),level.end());
                res.push_back(level);
                level.clear();
            }
            level.push_back(p.first->val);
            if(p.first->left)
                q.push({p.first->left,p.second + 1});
            if(p.first->right)
                q.push({p.first->right,p.second + 1});
        }
        if((res.size()+1) % 2 == 0)
            reverse(level.begin(),level.end());
        res.push_back(level);
        return res;
    }
};