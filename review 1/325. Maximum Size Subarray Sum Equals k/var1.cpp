class Solution {
public:
    int maxSubArrayLen(vector<int>& nums, int k)
    {
        
        int res = INT_MIN;
        int sum = 0;
        vector<int> sums;
        sums.push_back(sum);
        for(int& n:nums)
        {
            sum+= n;
            sums.push_back(sum);
        }
        unordered_map<int,int> list;
        for(int i = 0 ; i < sums.size();i++)
        {
            //cout<<sums[i]<<endl;
            //sums[i] - sums[j]  = k
            int target = sums[i] - k;
            //cout<<sums[i] << " "<< target <<endl;
            if(list.count(target))
            {
               res = max( i - list[target] , res );
            }
            if(!list.count(sums[i]))
            {
                list[sums[i]] = i;
            }
        }
        return res == INT_MIN ? 0 : res;
    }
};
