class Solution {
public:
    bool isMatch(string s, string p) 
    {
        int i = 0;
        int j = 0;
        int stari = -1, starj = -1;
        while(i < s.size())
        {
            if(s[i]==p[j]||p[j]=='?')
            {
                i++;
                j++;
            }
            else if(p[j]=='*')
            {
                stari = i;
                starj = j;
                j++;
            }
            else if(stari >= 0)
            {
                i = stari+1;
                stari++;
                j = starj + 1;
            }
            else 
                return false;
        }
        while(j<p.size()&&p[j] == '*')
            j++;
        return j == p.size();
    }
};