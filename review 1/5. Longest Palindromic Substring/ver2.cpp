class Solution {
public:
    string longestPalindrome(string s) {
        int center = 0;
        string expand = "#";
        for (int i = 0; i < s.size(); i++) {
            expand += s[i];
            expand += "#";
        }
        vector<int> radium(expand.size(), 0);
        int idx = 0, len = 0;
        for (int i = 1; i < expand.size(); i++) {
            if (i <= center + radium[center]) {
                int mirror = 2 * center - i;
                radium[i] = min(radium[mirror], mirror - (center - radium[center]));
                if (mirror - radium[mirror] > center - radium[center]) 
                    continue;
            }
            while (i - radium[i] >= 0 && i + radium[i] < expand.size() && expand[i - radium[i]] == expand[i + radium[i]])
                radium[i]++;
            radium[i]--;
            if (i + radium[i] > center + radium[center])
                center = i;
            int tmplen = i % 2 == 0 ? radium[i] / 2 * 2 : radium[i] / 2 * 2 + 1;
            if (tmplen > len) {
                idx = i;
                len = tmplen;
            }
        }
        return s.substr(idx / 2 - len / 2, len);
    }
};