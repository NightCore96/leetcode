class Solution {
public:
    string longestPalindrome(string s) 
    {
        int size = s.size();
        if(size==0)
            return "";
        string temp = "";
        
        for(auto c:s)
        {
            temp +="#";
            temp +=c;
        }
        temp += "#";
        int max = INT_MIN;
        int maxi = 0;
        
        for(int i=0;i<temp.size();i++)
        {
            int len = 0;
            for(int j=0;j<temp.size();j++)
            {
                int r = i-j;
                int l = i+j;
                if(r<0||l>=temp.size()||temp[l]!=temp[r])
                    break;
                len++;
            }
            if(len>max)
            {
                maxi = i;
                max = len;
            }
        }
        string res = "";
        if(temp[maxi]!='#')
            res = temp[maxi];
        for(int i=1;i<max;i++)
        {
            if(temp[maxi-i] !='#' )
                res = temp[maxi-i]+res;
            if(temp[maxi+i] !='#' )
                res = res +temp[maxi+i];
            //cout<<res<<endl;
        }
        return res;
    }
};