class Solution {
public:
    string removeDuplicates(string S)
    {
        stack<char> stk;
        for(int i = 0;i <S.size();i++)
        {
            if(!stk.empty() && stk.top() == S[i])
                stk.pop();
            else
                stk.push(S[i]);
        }
        string res ="";
        while(!stk.empty())
        {
            res += stk.top() ;
            stk.pop();
        }
        reverse(res.begin(),res.end());
        return res;
    }
};
