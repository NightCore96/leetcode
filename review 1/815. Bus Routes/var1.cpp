class Solution {
public:
    int numBusesToDestination(vector<vector<int>>& routes, int S, int T)
    {
        if(S == T)
            return 0;
        unordered_map<int,unordered_map<int,int>> map;
        for(int i = 0;i<routes.size();i++)
        {
            for(int j = 0; j < routes[i].size();j++)
            {
                int station = routes[i][j];
                map[station][i] = j;
            }
        }
        int res = 0;
        queue<int> q;
        unordered_set<int> visited;
        q.push(S);
        //visited.insert(S);
        while(!q.empty())
        {
            int size = q.size();
            for(int i = 0;i< size;i++)
            {
                auto p = q.front();
                q.pop();
                for(auto& i:map[p])
                {
                    int bus = i.first;
                    if(visited.count(bus))
                        continue;
                    //cout << bus <<endl;
                    visited.insert(bus);
                    for(auto& st:routes[bus])
                    {
                        if(st == T)
                            return res + 1;
                        q.push(st);
                    }
                }
            }
            res++;
        }
        return -1;
    }
};
