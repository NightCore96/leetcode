class Solution {
public:
    int lengthOfLIS(vector<int>& nums) 
    {
        vector<int> LIS;
        for(int i: nums)
        {
            if(LIS.empty())
                LIS.push_back(i);
            else
            {
                if(LIS.back() < i)
                    LIS.push_back(i);
                else
                {
                    for(int n=0;n<LIS.size();n++)
                    {
                        if(i <= LIS[n])
                        {
                            LIS[n]=i;
                            break;
                        }
                    }
                }
            }
        }
        return LIS.size();
    }
};