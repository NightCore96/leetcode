class Solution {
public:
    vector<vector<int>> getSkyline(vector<vector<int>>& buildings)
    {
        vector<vector<int>> res;
        vector<vector<int>> point;
        int size = buildings.size();
        if(size == 0)
            return res;
        for(auto &b:buildings)
        {
            point.push_back({b[0],-b[2]});
            point.push_back({b[1],b[2]});
        }
        sort(point.begin(),point.end());
        int pre = 0, cur = 0;
        multiset<int> m;
        m.insert(0);
        for (auto &a : point)
        {
            if (a[1] < 0)
                m.insert(-a[1]);
            else
                m.erase(m.find(a[1]));
            cur = *m.rbegin();
            if (cur != pre)
            {
                pre = cur;
                res.push_back({a[0], cur});
            }
        }
        return res;
    }
};
