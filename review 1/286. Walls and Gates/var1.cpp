class Solution {
public:
    void wallsAndGates(vector<vector<int>>& rooms)
    {
        //unordered_set<int> visit;
        for(int i = 0; i < rooms.size();i++)
        {
            for(int j = 0; j < rooms[0].size();j++)
            {
                if(rooms[i][j] == 0)
                {
                    rooms[i][j] = 1;
                    DFS(rooms,i,j,0);
                }
            }
        }
    }
    
    void DFS(vector<vector<int>>& rooms,int x,int y,int step)
    {
        if(x < 0 || x >= rooms.size() || y < 0 || y >= rooms[0].size())
            return;
        if(rooms[x][y] <= step)
            return;
        if(rooms[x][y] > step)
            rooms[x][y] = step;
        DFS(rooms,x+1,y,step+1);
        DFS(rooms,x-1,y,step+1);
        DFS(rooms,x,y+1,step+1);
        DFS(rooms,x,y-1,step+1);
    }
};
