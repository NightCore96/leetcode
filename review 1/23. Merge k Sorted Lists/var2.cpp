/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) 
    {
        
        ListNode* newlist = new ListNode(0);
        ListNode* head;
        head = newlist;
        if(l1==nullptr&&l2==nullptr)
            return nullptr;
        while(l1!=nullptr||l2!=nullptr)
        {
            int temp;
            if(l1==nullptr)
            {
                temp = l2->val;
                l2=l2->next;
            }
            else if(l2==nullptr)
            {
                temp = l1->val;
                l1=l1->next;
            }
            else
            {
                if(l1->val < l2->val)
                {
                    temp = l1->val;
                    l1=l1->next;
                }
                else
                {
                    temp = l2->val;
                    l2=l2->next;
                }
            }
            newlist->next = new ListNode(temp);
            newlist = newlist->next;
        }
        ListNode* res;
        res = head->next;
        delete head;
        return res;
    }
    
    ListNode* mergeKLists(vector<ListNode*>& lists) 
    {
        if(lists.empty())
            return NULL;
        int n = lists.size()-1;
        while(n>0)
        {
            int j = n;
            int i = 0;
            while(i<j)
            {
                lists[i] = mergeTwoLists(lists[i],lists[j]);
                i++;
                j--;
                if(i>=j)
                    n = j;
                    
            }
        }
        return lists[0];
    }
};