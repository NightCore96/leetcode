/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) 
    {
        if(lists.empty())
            return NULL;
        priority_queue<int,vector<int>,greater<int>> pq;
        for(auto list:lists)
        {
            while(list)
            {
                pq.push(list->val);
                list = list->next;
            }
        }
        if(pq.empty())
            return NULL;
        ListNode* head = new ListNode(pq.top());
        pq.pop();
        ListNode* res =head;
        while(!pq.empty())
        {
            head->next = new ListNode(pq.top());
            pq.pop();
            head = head->next;
        }
        
        return res;
            
    }
};