class Solution {
public:
    bool isScramble(string s1, string s2) 
    {
        if(s1.size()!=s2.size())
            return false;
        if(s1==s2)
            return true;
        int size = s1.size();
        int hash[26] = {0};
        for(int i = 0;i<size;i++)
        {
            hash[s1[i] - 'a']++;
            hash[s2[i] - 'a']--;
        }
        for(int i = 0;i<26;i++)
        {
           if( hash[i] != 0)
               return false;
        }
    
        for(int i=1;i<size;i++)
        {
            if(isScramble(s1.substr(0,i), s2.substr(0,i))&&isScramble(s1.substr(i),s2.substr(i)))
                return true;
            if(isScramble(s1.substr(0,i),s2.substr(size-i))&&isScramble(s1.substr(i),s2.substr(0,size-i)))
                return true;
        }
        return false;
    }
};