class Solution {
public:
    bool isIsomorphic(string s, string t) {
        unordered_map<char, char> m;
        unordered_set<char> taken;
        for (int i = 0 ; i < s.size() ; i++)
        {
            if (!m.count(s[i]))
            {
                if (taken.count(t[i]))
                    return false;
                m[s[i]] = t[i];
                taken.insert(t[i]);
            }
            else
            {
                if (m[s[i]] != t[i])
                    return false;
            }
        }
        return true;
    }
};
