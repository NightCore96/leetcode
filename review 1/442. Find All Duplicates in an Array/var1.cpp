class Solution {
public:
    vector<int> findDuplicates(vector<int>& nums)
    {
        vector<int> res;
        for(int i = 0; i < nums.size();i++)
        {
            int c = abs(nums[i]);
            if(nums[c - 1] < 0)
            {
                res.push_back(c);
            }
            else
            {
                nums[c - 1] = nums[c - 1] * -1;
            }
        }
        return res;
    }
};
