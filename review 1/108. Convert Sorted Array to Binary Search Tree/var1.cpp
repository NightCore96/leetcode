/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* sortedArrayToBST(vector<int>& nums) 
    {
        if(nums.empty())
            return nullptr;
        int size = nums.size();
        if(size == 1)
            return new TreeNode(nums[0]);
        
        int mid = size / 2;
        TreeNode* root = new TreeNode(nums[mid]);
        vector<int> left;
        vector<int> right;
        for(int i = 0; i < mid ; i++)
        {
            left.push_back(nums[i]);
        }
        for(int i = mid + 1; i < size ; i++)
        {
             right.push_back(nums[i]);
        }
        root->left = sortedArrayToBST(left);
        root->right = sortedArrayToBST(right);
        return root;
    }
};