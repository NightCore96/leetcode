class Solution {
public:
    int calculate(string s)
    {
        int len = s.length();
        if(len<=0)
            return 0;
        s = s + '\n';
        int index = 0;
        stack<int> numstack;
        stack<char> opstack;
        int count = -1;
        while(index<=len)
        {
            char c = s[index];
            if(c>='0'&&c<='9')
            {
                if(count<0)
                    count = c-'0';
                else
                {
                    count = count * 10 + (c-'0');
                }
            }
            else
            {
                if(count>=0)
                {
                    if(!opstack.empty()&&(opstack.top()=='/'|| opstack.top()=='*'))
                    {
                        char op = opstack.top();
                        int t = numstack.top();
                        numstack.pop();
                        opstack.pop();
                        if(op=='/')
                        {
                            count = t/count;
                        }
                        else if(op=='*')
                        {
                            count = t*count;
                        }
                        
                    }
                    numstack.push(count);
                    count = -1;
                }
                if(c=='+')
                {
                    opstack.push(c);
                }
                else if (c=='-')
                {
                    opstack.push(c);
                }
                else if (c=='*')
                {
                     opstack.push(c);
                }
                else if (c=='/')
                {
                     opstack.push(c);
                }
            }
            index++;
        }
        int total = 0;
        while(numstack.size()!=1)
        {
            int t = numstack.top();
            numstack.pop();
            
            if(!opstack.empty()&&opstack.top() =='+')
            {
                total += t;
                opstack.pop();
            }
            else if(!opstack.empty()&&opstack.top() =='-')
            {
                total +=  (-1*t);
                opstack.pop();
            }
        }
        return numstack.top()+total;
    }
};
