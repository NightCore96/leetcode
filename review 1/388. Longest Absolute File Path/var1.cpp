class Solution
{
public:
    int lengthLongestPath(string input)
    {
        unordered_map<int,int> table;
        input += '\n';
        int res = INT_MIN;
        int start = 0;
        int len = 0;
        for(int i = 0;i<input.size();i++)
        {
            switch(input[i])
            {
                case '\n':
                    {
                        string file = input.substr(start,len);
                        start = i + 1;
                        len = 0;
                        int level = file.find_last_of('\t') + 1;
                        int pre = 0;
                        if(table.count(level - 1))
                            pre = table[level - 1];
                        int path = pre + file.substr(level).size();
                        if(file.find('.') != string::npos )
                        {
                            
                            res = max(res,path);
                        }
                        else
                        {
                            table[level] = path + 1; // add '/'
                        }
                        break;
                    }
                default:
                    {
                        len++;
                        break;
                    }
            }
        }
        return res == INT_MIN ? 0 : res;
    }
};
