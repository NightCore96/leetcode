class Solution {
public:
    vector<int> hitBricks(vector<vector<int>>& grid, vector<vector<int>>& hits)
    {
        for(auto& h:hits)
        {
            if(grid[h[0]][h[1]] == 1)
                grid[h[0]][h[1]] = 0;
            else
            {
                h[0] = -1;
                h[1] = -1;
            }
        }
        vector<vector<bool>> visit(grid.size(),vector<bool>(grid[0].size(),false));
        int brick = 0;
        for(int i = 0 ;i < grid[0].size();i++)
        {
            dfs(brick,grid,0,i,visit);
        }
        vector<int> res(hits.size(),0);
        for(int i = hits.size()-1 ;i >= 0 ;i--)
        {
            if(hits[i][0] == -1 && hits[i][1] == -1)
                continue;
            int x = hits[i][0];
            int y = hits[i][1];
            int old = brick;
            grid[x][y]=1;
            if((x-1>=0&&visit[x-1][y])||
               (x+1<grid.size()&&visit[x+1][y])||
               (y-1>=0&&visit[x][y-1])||
               (y+1<grid[0].size()&&visit[x][y+1]||
               x==0))
            {
                dfs(brick,grid,x,y,visit);
                res[i] = brick - old -1;
            }

        }
        return res;
    }
    
    void dfs(int& res,vector<vector<int>>& grid,int x,int y,vector<vector<bool>>& visit)
    {
        if(x<0||y<0||x >= grid.size()||y >= grid[0].size())
            return;
        if(visit[x][y]||grid[x][y] == 0)
            return;
        //cout << x <<" " << y <<endl;
        visit[x][y] = true;
        res++;
        dfs(res,grid,x+1,y,visit);
        dfs(res,grid,x-1,y,visit);
        dfs(res,grid,x,y+1,visit);
        dfs(res,grid,x,y-1,visit);
    }
};
