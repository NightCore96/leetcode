/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if(!head)
            return NULL;
        vector<ListNode*> per;
        ListNode* index = head;
        while(index)
        {
            per.push_back(index);
            index=index->next;
        }
        int size= per.size();
        if(n==size)
        {
            index =head->next;
            delete head;
            return index;    
        }
        per.push_back(NULL);
        per[size-n-1]->next = per[size-n+1];
        index=per[size-n];
        delete index;
        return head;
    }
};