class Solution {
public:
    bool hasGroupsSizeX(vector<int>& deck)
    {
        unordered_map<int,int> table;
        for(auto& d:deck)
        {
            table[d]++;
        }
        int X = 0;
        for(auto& p: table)
        {
            X = GCD(p.second,X);
        }
        if(X == 1)
            return false;
        return true;
    }
    
    int GCD(int i,int j)
    {
        while(j > 0)
        {
            int mod = i % j;
            i = j;
            j = mod;
        }
        return i;
    }
};
