class Solution {
public:
    double minAreaFreeRect(vector<vector<int>>& points)
    {
        unordered_map<int,unordered_map<string,vector<pair<vector<int>,vector<int>>>>> map;
        for(int i = 0;i<points.size();i++)
        {
            for(int j = i+1;j<points.size();j++)
            {
                const auto& p1 = points[i];
                const auto& p2 = points[j];
                auto X = (p1[0] + p2[0])/2.0;
                auto Y = (p1[1] + p2[1])/2.0;
                auto len = (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
                string str = to_string(X)+","+to_string(Y);
                map[len][str].push_back({p1,p2});
            }
        }
        double res = INT_MAX;
        for(const auto& len:map)
        {
            for(auto& p:len.second)
            {
                const auto& line = p.second;
                for(int i = 0;i<line.size();i++)
                {
                    for(int j = i + 1;j<line.size();j++)
                    {
                        const auto& l1 = line[i].first;
                        const auto& l2 = line[j].first;
                        const auto& l3 = line[j].second;
                        auto h = sqrt((l1[0] - l2[0])*(l1[0] - l2[0]) + (l1[1] - l2[1])*(l1[1] - l2[1]));
                        auto w = sqrt((l1[0] - l3[0])*(l1[0] - l3[0]) + (l1[1] - l3[1])*(l1[1] - l3[1]));
                        res = min(res,h*w);
                    }
                }
            }
        }
        return res == INT_MAX ? 0.0 : res;
    }
};
