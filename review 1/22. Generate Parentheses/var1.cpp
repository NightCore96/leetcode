class Solution {
public:
    vector<string> generateParenthesis(int n) 
    {
        vector<string> res;
        string now ="";
        helper(res,0,0,n,now);
        return res;
    }
    
    void helper(vector<string> &res,int L,int R,int N,string &now)
    {
        if(L==N&&R==N)
        {
            res.push_back(now);
            return;
        }
        if(L<N)
        {
            string temp = now + "(";
            helper(res,L+1,R,N,temp);
        }
        if(R<L)
        {
            string temp = now + ")";
            helper(res,L,R+1,N,temp);
        }
    }
};