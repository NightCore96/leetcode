class StreamChecker
{
private:
    class TrieNode
    {
    public:
        unordered_map<char,TrieNode*> childs;
        bool isEnd;
        TrieNode()
        {
            isEnd = false;
        }
    };
    TrieNode* root;
    vector<char> buffer;
    int maxSize;
public:
    StreamChecker(vector<string>& words)
    {
        root = new TrieNode();
        maxSize = 0;
        for(string w: words)
        {
            int len = w.size();
            maxSize = max(maxSize,len);
            TrieNode* node = root;
            for(int i = w.size() -1;i>=0;i--)
            {
                if(!node->childs.count(w[i]))
                {
                    node->childs[w[i]] = new TrieNode();
                }
                node = node->childs[w[i]];
                if(i == 0)
                    node->isEnd = true;
            }
        }
    }
    
    bool query(char letter)
    {
        buffer.push_back(letter);
        if(buffer.size() > maxSize)
        {
            buffer.erase(buffer.begin());
        }
        TrieNode* node = root;
        for(int i = buffer.size()-1;i>=0;i--)
        {
            if(!node->childs.count(buffer[i]))
            {
                return false;
            }
            node = node->childs[buffer[i]];
            if(node->isEnd)
                return true;
        }
        return false;
    }
};

/**
 * Your StreamChecker object will be instantiated and called as such:
 * StreamChecker* obj = new StreamChecker(words);
 * bool param_1 = obj->query(letter);
 */
