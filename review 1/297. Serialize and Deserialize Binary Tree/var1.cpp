/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Codec {
public:
    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string result = "[";
        if(!root)
        {
            result +="null]";
            return result;
        }
        int index = 0;
        int pre_index = -1;
        queue<pair<TreeNode*,int>> q;
        q.push(pair<TreeNode*,int>(root,index));
        while(!q.empty())
        {
            pair<TreeNode*,int> top=q.front();
            q.pop();
            int Manynull=top.second-pre_index-1;
            for(int i=0;i<Manynull;i++)
            {
                result=result+",null";    
            }
            if(top.second>0)
                result=result+",";
            result=result+to_string(top.first->val);
            pre_index=top.second;
            index++;
            if(top.first->left)
                q.push(pair<TreeNode*,int>(top.first->left,index));
            index++;
            if(top.first->right)
                q.push(pair<TreeNode*,int>(top.first->right,index));
        }
        result=result+"]";
        //cout<<result<<endl;
        return result;
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
       if(data=="[null]")
           return NULL;
        int size = data.size();
        data = data.substr(1,size-2);
        vector<string> datalist = split(data,",");
        int id = 0;
        string d = datalist[id];
        queue<TreeNode*> q;
        TreeNode* root = new TreeNode(stoi(d));
        q.push(root);
        id++;
        while(!q.empty())
        {
            auto n = q.front();
            q.pop();
            if(id<datalist.size()&&datalist[id]!="null")
            {
                n->left = new TreeNode(stoi(datalist[id]));
                q.push(n->left);
            }
            id++;
            if(id<datalist.size()&&datalist[id]!="null")
            {
                n->right = new TreeNode(stoi(datalist[id]));
                q.push(n->right);
            }
            id++;
        }
        return root;
    }
private:
    vector<string> split( const string &str, const string &delimiters) 
    {
        vector<string> elems;
        string::size_type pos, prev;
        pos = str.find(delimiters);
        prev = 0;
        
        while ( pos != string::npos ) 
        {
            elems.push_back( str.substr(prev, pos - prev) );
            prev = pos + delimiters.size();
            pos = str.find(delimiters,prev);
        }
        if ( prev != str.size() ) 
            elems.push_back(str.substr(prev));
        return elems;
    }
};

// Your Codec object will be instantiated and called as such:
// Codec codec;
// codec.deserialize(codec.serialize(root));