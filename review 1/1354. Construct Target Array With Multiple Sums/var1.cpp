class Solution {
public:
    bool isPossible(vector<int>& target) 
    {
        long long sum = 0;
        priority_queue<long long> pq;
        for (const int& i: target) {
            pq.push(i);
            sum += i;
        }
        while(pq.top()!= 1)
        {
            long long top = pq.top(); //max in target
            pq.pop();
            long long n = top - (sum-top);
            if( n < 1 )
                return false;
            pq.push(n);
            sum = top;
        }
        return true;
    }
       
};