/**
 * The read4 API is defined in the parent class Reader4.
 *     int read4(char *buf);
 */

class Solution {
public:
    /**
     * @param buf Destination buffer
     * @param n   Number of characters to read
     * @return    The number of actual characters read
     */
    char *cur_buff = new char[4];
    int cur_pt = 0, cur_cnt = 0;
    public:
    int read(char *buf, int n) {
        int rnt = 0;
        while(rnt < n){
            if(cur_cnt > 0){
                *(buf+rnt) = cur_buff[cur_pt];
                rnt++; cur_cnt--;
                cur_pt = (cur_pt + 1) % 4;
            }else{
                cur_cnt = read4(cur_buff);
                cur_pt = 0;
                if(cur_cnt == 0) break;
            }
        }
        return rnt;
    }
};
