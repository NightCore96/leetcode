public class Solution {
    public void Solve(char[][] board) 
    {
        var m = board.Length;
        if(m == 0)
            return;
        var n = board[0].Length;
        if(n == 0)
            return;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j< n; j++)
            {
                if((i == 0 ||i == m -1 ||j == 0 ||j == n -1)&&board[i][j] == 'O')
                {
                    helper(board,i,j);
                }
            }
        }
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j< n; j++)
            {
                if(board[i][j] == 'C')
                {
                    board[i][j] = 'O';
                }
                else
                {
                    board[i][j] = 'X';
                }
            }
        }
    }
    
    private void helper(char[][] board,int i ,int j)
    {
        var m = board.Length;
        var n = board[0].Length;
        if(i < 0 ||i > m -1 ||j < 0 ||j > n -1)
            return ;
        if(board[i][j] != 'O')
            return ;
        board[i][j] = 'C';
        helper(board,i+1,j);
        helper(board,i-1,j);
        helper(board,i,j+1);
        helper(board,i,j-1);
    }
}