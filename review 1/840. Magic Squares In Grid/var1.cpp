class Solution {
public:
    int numMagicSquaresInside(vector<vector<int>>& grid)
    {
        int res = 0;
        if(grid.size() < 3 || grid[0].size() < 3)
            return res;
        for(int i = 1 ; i < grid.size() - 1;i++)
        {
            for(int j= 1 ; j< grid[0].size() - 1;j++)
            {
                if(grid[i][j] == 5 && isMagic(grid,i,j))
                    res++;
            }
        }
        return res;
    }
    
    bool isMagic(vector<vector<int>>& gird ,int x, int y)
    {
        unordered_set<int> s;
        for(int i = x -1 ; i <= x +1 ;i++)
        {
            if(gird[i][y-1] + gird[i][y] + gird[i][y+1] != 15)
                return false;
            s.insert(gird[i][y-1]);
            s.insert(gird[i][y+1]);
            s.insert(gird[i][y]);
        }
        for(int i = 1;i<=9;i++)
        {
            if(!s.count(i))
                return false;
        }
        
        for(int i = y -1 ; i <= y +1 ;i++)
        {
            
            if(gird[x-1][i] + gird[x][i] + gird[x+1][i] != 15)
                return false;
        }
         
        if(gird[x-1][y-1]+gird[x][y]+gird[x+1][y+1] != 15)
            return false;
        
        if(gird[x+1][y-1]+gird[x][y]+gird[x-1][y+1] != 15)
            return false;
        
        return true;
    }
};
