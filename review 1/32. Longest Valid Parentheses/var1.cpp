class Solution {
public:
    int longestValidParentheses(string s) {
        int size=s.length();
        int MAX=0;
        int start = 0;
        int L=0,R=0;
        for(int i=0;i<size;i++)
        {
           char a=s[i];
           if(a=='(')
           {
               L++;
           }
           if(a==')')
           {
               R++;
           }
           if(R>L)
           {
              L=0;
              R=0;
           }
           if(R==L)
            MAX=max(MAX,R*2);
        }
        L=0;
        R=0;
        for(int i=size-1;i>=0;i--)
        {
           char a=s[i];
           if(a=='(')
           {
               L++;
           }
           if(a==')')
           {
               R++;
           }
           if(R<L)
           {
              L=0;
              R=0;
           }
           if(R==L)
            MAX=max(MAX,R*2);
        }
        return MAX;
    }
};