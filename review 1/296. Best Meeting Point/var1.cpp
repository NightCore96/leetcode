class Solution {
public:
    int minTotalDistance(vector<vector<int>>& grid)
    {
        if(grid.empty()||grid[0].empty())
            return -1;
        int n = grid.size();
        int m = grid[0].size();
        vector<int> people_X;
        vector<int> people_Y;
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(grid[i][j]==1)
                {
                    people_X.push_back(i);
                    people_Y.push_back(j);
                }
            }
        }
        
        return MDis(people_X)+MDis(people_Y);
    }
    
    
    int MDis(vector<int> &arr)
    {
        sort(arr.begin(),arr.end());
        int res  = 0;
        int start = 0;
        int end = arr.size()-1;
        while(start<end)
        {
            res +=arr[end]-arr[start];
            end--;
            start++;
        }
        return res;
    }
    
};
