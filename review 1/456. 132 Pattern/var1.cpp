class Solution {
public:
    bool find132pattern(vector<int>& nums)
    {
        if(nums.size() < 3)
            return false;
        int Mi = INT_MAX;
        vector<int> m;
        for(int& i:nums)
        {
            Mi = min(Mi,i);
            m.push_back(Mi);
        }
        stack<int> s;
        for(int i = nums.size() - 1 ; i>=0;i--)
        {
            if(nums[i] > m[i])
            {
                while(!s.empty()&& s.top() <= m[i])
                    s.pop();
                if(!s.empty() && s.top() < nums[i])
                    return true;
                s.push(nums[i]);
            }
        }
        return false;
    }
};
