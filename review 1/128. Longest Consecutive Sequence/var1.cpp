class Solution {
public:
    int longestConsecutive(vector<int>& nums) 
    {
        unordered_set<int> hash;
        for(int i=0;i<nums.size();i++)
        {
            hash.insert(nums[i]);
        }
        int res=0;
        for(int i=0;i<nums.size();i++)
        {
            if(hash.count(nums[i]-1))
                continue;
            int nextnum=nums[i]+1;
            int count =1;
            while(hash.count(nextnum))
            {
                count++;
                nextnum++;
            }
            res=max(res,count);
        }
        return res;
    }
};