class Solution {
private:
    unordered_map<char,unordered_set<char>> graph;
    bool isUtil(string a,string b)
    {
        for(int i = 0; i < min(a.size(),b.size()); i++)
        {
            if(a[i]!= b[i])
            {
                graph[a[i]].insert(b[i]);
                return true;
            }
        }
        //abc ab
        if(a.size() > b.size())
            return false;
        return true;
    }
    
    bool DFS(unordered_map<char,int> &visit,char now,string& res)
    {
        visit[now] = 1;
        for(auto &c:graph[now])
        {
            if((visit[c] == 1) || (visit[c] == 0 &&!DFS(visit,c,res)))
                return false;
        }
        res = now + res;
        visit[now] = 2;
        return true;
    }
    
public:
    string alienOrder(vector<string>& words)
    {
        unordered_set<char> list;
        for(auto &c :words[0])
            list.insert(c);
        for(int i = 1; i< words.size();i++)
        {
            for(auto &c:words[i])
                list.insert(c);
            if(!isUtil(words[i - 1],words[i]))
                return "";
        }
        
        if(graph.empty() && !words.empty())
            return words[words.size() - 1];
        //cout<<"gogo"<<endl;
        unordered_map<char,int> visit;
        for(auto& c:list)
        {
            //cout<<c<<endl;
            visit[c] = 0;
        }
        string res ="";
        for(auto& p:visit)
        {
            if(p.second == 0 && graph.count(p.first))
            {
                if(!DFS(visit,p.first,res))
                    return "";
            }
        }
        
        //reverse(res.begin(),res.end());
        unordered_set<char> n;
        for(auto &c : res)
            n.insert(c);
        for(auto &c : list)
        {
            if(!n.count(c))
            res+=c;
        }
        
        return res;
    }
};
