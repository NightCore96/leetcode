class MedianFinder {
public:
    /** initialize your data structure here. */
    MedianFinder()
    {
    }
    
    void addNum(int num)
    {
        if(MAXList.empty()&&MinList.empty())
        {
            MinList.push(num);
            return;
        }
        if(num>MinList.top())
        {
            int t=MinList.top();
            MinList.pop();
            MAXList.push(t);
            MinList.push(num);
            if(MinList.size()<MAXList.size())
            {
                 int t=MAXList.top();
                 MAXList.pop();
                 MinList.push(t);
            }
        }
        else
        {
            MAXList.push(num);
            if(MinList.size()<MAXList.size())
            {
                 int t=MAXList.top();
                 MAXList.pop();
                 MinList.push(t);
            }
        }
    }
    
    double findMedian()
    {
        int size = MinList.size()+MAXList.size();
        //cout<<MinList.size() <<" "<<MAXList.size() <<endl;
        if(size%2==0)
        {
           return (MinList.top()+MAXList.top())/2.0;
        }
        return MinList.top();
    }
private:
    priority_queue<int,vector<int>,less<int>> MAXList;
    priority_queue<int,vector<int>,greater<int>> MinList;
};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder obj = new MedianFinder();
 * obj.addNum(num);
 * double param_2 = obj.findMedian();
 */
