class Solution {
public:
    int ladderLength(string beginWord, string endWord, vector<string>& wordList) 
    {
        int res = 1;
        unordered_set<string> hashtable;
        for(auto &s:wordList)
        {
            hashtable.insert(s);
        }
        if(!hashtable.count(endWord))
            return 0;
        queue<string> queue;
        queue.push(beginWord);
        while(!queue.empty())
        {
            for(int i = queue.size();i > 0;i--)
            {
                auto s = queue.front();
                queue.pop();
               
                for(int i = 0;i<s.size();i++)
                {
                    auto t = s;
                    for(char j = 'a'; j<= 'z' ; j++)
                    {
                        t[i] = j;
                        if(t==s)
                            continue;
                        if(hashtable.count(t))
                        {
                            if(t == endWord)
                            {
                                res++;
                                return res;
                            }
                            hashtable.erase(t);
                            queue.push(t);
                        }
                    }
                }
            }
            if(!queue.empty())
            {
                res++;
            }
        }
        return 0;
    }
};