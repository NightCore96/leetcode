class Solution {
public:
    int leastInterval(vector<char>& tasks, int n)
    {
        int ans = 0;
        int map[26] = { 0 };
        for (int i = 0; i < tasks.size(); ++i) {
            map[tasks[i] - 'A']++;
        }
        sort(map, map + 26, [](const int &a,const int &b){ return a > b;});
        int maxVal = map[0] - 1;
        int idle_slots = maxVal*n;
        for (int i = 1; i < 26; ++i) {
            idle_slots = idle_slots - min(maxVal, map[i]);
        }
        if (idle_slots > 0) {
            ans = idle_slots + tasks.size();
        }
        else {
            ans = tasks.size();
        }
        return ans;
    }
};
