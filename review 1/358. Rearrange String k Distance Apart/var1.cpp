class Solution {
public:
    string rearrangeString(string s, int k)
    {
        if (k == 0 || s.size() < k)
            return s;
        string res ="";
        //int maxCount = s.size()/k;
        unordered_map<char,int> hash;
        for(const auto& c:s)
        {
            hash[c]++;
        }
        priority_queue<pair<int,char>> pq;
        for(auto& p:hash)
        {
            pq.push({p.second,p.first});
        }
        while(!pq.empty())
        {
            vector<pair<int,char>> temp;
            int i  = 0;
            for(i =0;i<k;i++)
            {
                
                auto p = pq.top();
                temp.push_back(p);
                pq.pop();
                res += p.second;
                if(pq.empty())
                {
                    if(i!= k -1 && res.size()!=s.size())
                        return "";
                    break;
                }
                    
            }
            for(auto& t:temp)
            {
                if(t.first-1 > 0)
                    pq.push({t.first-1,t.second});
            }
        }
        return res;
    }
};
