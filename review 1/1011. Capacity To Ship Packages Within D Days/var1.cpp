class Solution {
public:
    int shipWithinDays(vector<int>& weights, int D)
    {
        int mx = INT_MIN;
        int sum = 0;
        for(const auto& w:weights)
        {
            mx = max(mx,w);
            sum += w;
        }
        int end = sum;
        int start = max(sum/D,mx);
        while(start < end)
        {
            int mid = start + (end - start) / 2;
            if(isOK(weights, mid)<=D)
                end = mid;
            else
                start = mid +1;
        }
        return start;
    }
    
    int isOK(vector<int>& weights, int c)
    {
        int res = 1;
        int sum = 0;
        for(const auto& w:weights)
        {
            sum += w;
            if(sum > c)
            {
                res++;
                sum = w;
            }
        }
        return res;
    }
}; 
