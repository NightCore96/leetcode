class Solution
{
private:
    int sum;
    vector<int> sums;
public:
    Solution(vector<int>& w)
    {
        sum = 0;
        sums.push_back(0);
        for(int i = 0; i<w.size(); i++)
        {
            sum += w[i];
            sums.push_back(sum);
        }
    }
    
    int pickIndex()
    {
        if(sum == 0)
            return -1;
        int r = rand() % sum;
        int res = -1;
        //cout<<r<<endl;
        for(int i = 1; i< sums.size();i++)
        {
            if(sums[i-1] <= r && sums[i] > r)
            {
                res = i - 1;
                return res;
            }
        }
        //sum -- ;
        return res;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(w);
 * int param_1 = obj->pickIndex();
 */
