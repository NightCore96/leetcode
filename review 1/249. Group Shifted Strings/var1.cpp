class Solution {
public:
    vector<vector<string>> groupStrings(vector<string>& strings)
    {
        unordered_map<string,vector<string>> table;
        for(auto s:strings)
        {
            string key = helper(s);
            //cout<<s <<" "<<key <<endl;
            table[key].push_back(s);
        }
        vector<vector<string>> res;
        for(auto p:table)
        {
            res.push_back(p.second);
        }
        return res;
    }
    
    string helper(string s)
    {
        if(s.empty() || s[0] == 'a')
            return s;
        int shift = s[0] - 'a';
        string res = "";
        for(auto c :s)
        {
            int t = c - shift;
            if( t < 'a')
            {
                t += 26;
            }
                
            res += (char)(t);
        }
        
        return res;
    }
};
