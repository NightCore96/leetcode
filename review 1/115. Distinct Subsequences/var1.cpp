/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    void flatten(TreeNode* root) 
    {
        if(!root)
            return;
        TreeNode* node = root;
        TreeNode* l = node->left;
        TreeNode* r = node->right;
        node->left = nullptr;
        node->right = l;
        while(node->right)
            node = node->right;
        node->right=r;
        flatten(root->right);
    }
};