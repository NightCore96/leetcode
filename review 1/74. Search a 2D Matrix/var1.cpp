class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) 
    {
        if(matrix.empty()||matrix[0].empty())
            return false;
        int m = matrix.size();
        int n = matrix[0].size();
        int start = 0;
        int end = m*n - 1;
        if(matrix[0][0] > target || matrix[m -1][n -1] < target)
            return false;
        while(start<=end)
        {
            int mid = start + (end - start)/2;
            if(matrix[mid/n][mid%n] == target)
                return true;
            else if(matrix[mid/n][mid%n] < target)
                start = mid +1;
            else
                end = mid - 1;
        }
        return false;
    }
};