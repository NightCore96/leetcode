class Solution {
public:
    int maxProfit(int k, vector<int>& prices)
    {
        int days = prices.size();
        if(k >= days/2)
        {
            int res = 0;
            for(int i = 1; i < days; i++)
            {
                int p = prices[i] - prices[i -1];
                if( p > 0)
                    res += p;
            }
            return res;
        }
        
        
        vector<vector<int>> dp(k+1,vector<int>(days,0));
        for(int i = 1; i <=k ;i++)
        {
            int p = dp[i-1][0] - prices[0];
            for(int j = 1;  j < days; j ++)
            {
                dp[i][j] = max(dp[i][j-1],prices[j] + p);
                p = max(p,dp[i-1][j] - prices[j]);
            }
        }
        return dp[k][days -1];
    }
};
