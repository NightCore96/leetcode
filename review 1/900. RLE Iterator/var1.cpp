class RLEIterator {
public:
    vector<int> data;
    int index;
    int count;
    RLEIterator(vector<int>& A)
    {
        data = A;
        index = 0;
        count = 0;
    }
    
    int next(int n)
    {
        if(index >= data.size())
            return -1;
        count += n;
        while(index < data.size() && count > data[index])
        {
            count -= data[index];
            index += 2;
        }
        if(index >= data.size())
            return -1;
        return data[index+1];
    }
};

/**
 * Your RLEIterator object will be instantiated and called as such:
 * RLEIterator* obj = new RLEIterator(A);
 * int param_1 = obj->next(n);
 */
