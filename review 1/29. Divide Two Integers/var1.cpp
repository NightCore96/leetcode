class Solution {
public:
    int divide(int dividend, int divisor) 
    {
        if(divisor == 0)
            return INT_MAX;
        long long m = abs((long long) dividend);
        long long n = abs((long long) divisor);
        long long res = 0;
        int sign = (dividend < 0 ^ divisor < 0) ? -1 : 1;
        if (m < n) 
            return 0;
        while (m >= n) 
        {
            long long t = n;
            long long p = 1;
            while(m > (t << 1))
            {
                t <<= 1;
                p <<= 1;
            }
            res += p;
            m -= t;
        }
        if(sign == -1) 
            res = -res;
        return res > INT_MAX ? INT_MAX : res;
    }
};