class NumMatrix
{
private:
    vector<vector<int>> m_matrix;
    vector<vector<int>> m_sum;
public:
    NumMatrix(vector<vector<int>>& matrix)
    {
        m_matrix = matrix;
        if(matrix.empty() || matrix[0].empty())
            return;
        m_sum = vector<vector<int>>(matrix.size()+1,vector<int>(matrix[0].size()+1,0));
        for(int i = 1 ; i < m_sum.size();i++)
        {
            for(int j = 1; j< m_sum[0].size();j++)
            {
                m_sum[i][j] = m_sum[i-1][j] + m_sum[i][j-1] +  m_matrix[i-1][j-1] - m_sum[i-1][j-1];
            }
        }
    }
    
    int sumRegion(int row1, int col1, int row2, int col2)
    {
        int res = m_sum[row2 + 1][col2 + 1] - m_sum[row1 ][col2 + 1] - m_sum[row2 + 1][col1] + m_sum[row1][col1];
        return res;
    }
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */
