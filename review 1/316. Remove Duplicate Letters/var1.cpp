class Solution {
public:
    string removeDuplicateLetters(string s)
    {
        vector<bool> list(26,false);
        vector<int> count(26,0);
        for(char& c:s)
        {
            count[c - 'a']++;
        }
        string res = "";
        for(int i = 0;i<s.size();i++)
        {
            count[s[i] - 'a']--;
            if(!list[s[i] - 'a'])
            {
                while(res.size() > 0 && s[i] < res.back() && count[res.back() - 'a'] > 0 )
                {
                    list[res.back() - 'a'] = false;
                    res.pop_back();
                }
                list[s[i] - 'a'] = true;
                res.push_back(s[i]);
                //cout<< res<<endl;
            }
        }
        return res;
    }
};
