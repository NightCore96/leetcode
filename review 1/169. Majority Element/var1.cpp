class Solution {
public:
    int majorityElement(vector<int>& nums) 
    {
        if(nums.empty())
            return NULL;
        int n = 0;
        int res;
        for(int i:nums)
        {
            if(n==0)
            {
                n++;
                res = i;
            }
            else
            {
                if(i==res)
                    n++;
                else
                    n--;
            }
        }
        return res;
    }
};