class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon)
    {
        if(dungeon.empty())
            return 0;
        int M = dungeon.size();
        int N = dungeon[0].size();
        vector<vector<int>> dp = dungeon;
        for(int i = M -1; i >= 0 ;i --)
        {
            for(int j = N -1 ; j >=0 ;j--)
            {
                if(i < M-1 && j < N-1)
                    dp[i][j] = max(1,min(dp[i][j+1],dp[i+1][j])-dp[i][j]);
                else if(i < M-1)
                    dp[i][j] = max(1,dp[i+1][j]-dp[i][j]);
                else if(j < N-1)
                    dp[i][j] = max(1,dp[i][j+1]-dp[i][j]);
                else
                    dp[i][j] = max(1,1-dp[i][j]);
            }
        }
        return dp[0][0];
    }
};
