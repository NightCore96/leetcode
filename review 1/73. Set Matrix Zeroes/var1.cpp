class Solution {
public:
    void setZeroes(vector<vector<int>>& matrix) 
    {
        int M = matrix.size();
        int N = matrix[0].size();
        int last = -1;
        for(int i=M-1;i>=0;i--)
        {
            bool isClear = false;
            for(int j=0;j<N;j++)
            {
                if(matrix[i][j]==0)
                {
                    last=i;
                    isClear=true;
                    break;
                }
            }
            if(isClear)
                break;
        }
        if(last==-1)
            return;
        for(int i=0;i<last;i++)
        {
            bool isClear = false;
            for(int j=0;j<N;j++)
            {
                if(matrix[i][j]==0)
                {
                    matrix[last][j]=0;
                    isClear=true;
                }
            }
            if(isClear)
            {
                for(int k=0;k<N;k++)
                {
                    matrix[i][k]=0;
                }
            }
        }
        
        for(int j=0;j<N;j++)
        {
            if(matrix[last][j]==0)
            {
                for(int i=0;i<M;i++)
                {
                    matrix[i][j] = 0;
                }
            }
            else
                matrix[last][j]=0;
        }
        
        
    }
};