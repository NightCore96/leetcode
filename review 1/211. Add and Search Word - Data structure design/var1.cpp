class TripNode
{
public:
    TripNode* child[26];
    bool isEnd = false;
};

class WordDictionary {
private :
    TripNode* root;
public:
    /** Initialize your data structure here. */
    WordDictionary()
    {
        root = new TripNode();
    }
    
    /** Adds a word into the data structure. */
    void addWord(string word)
    {
        TripNode* node = root;
        for(auto c : word)
        {
            if(!node->child[c-'a'])
                node->child[c-'a'] = new TripNode();
            node = node->child[c - 'a'];
        }
        node->isEnd = true;
    }
    
    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    bool search(string word)
    {
       return searchhelper(word,0,root);
    }
    
    bool searchhelper(string& s,int index,TripNode* node)
    {
        for(int i =index; i< s.size();i++)
        {
            if(s[i] == '.')
            {
                for(int j = 0; j < 26;j++)
                {
                    if(node->child[j]&&searchhelper(s,i+1,node->child[j]))
                       return true;
                }
                return false;
            }
            else if(node->child[s[i] - 'a'])
                node = node->child[s[i] - 'a'];
            else
                return false;
        }
        return node->isEnd;
    }
};

/**
 * Your WordDictionary object will be instantiated and called as such:
 * WordDictionary* obj = new WordDictionary();
 * obj->addWord(word);
 * bool param_2 = obj->search(word);
 */
