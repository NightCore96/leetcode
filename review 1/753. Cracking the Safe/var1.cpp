class Solution {
public:
    string crackSafe(int n, int k)
    {
        int target = pow(k,n);
        string res(n,'0');
        unordered_set<string> list;
        list.insert(res);
        dfs(res,n,k,target,list);
        return res;
    }
    
    void dfs(string& res,int& n,int& k,int& target,unordered_set<string>& list)
    {
        if(list.size() == target)
            return;
        string s = res.substr(res.size() - n + 1);
        for(int i = k-1;i>=0;i--)
        {
            string t = s+to_string(i);
            if(!list.count(t))
            {
                list.insert(t);
                res+=to_string(i);
                dfs(res,n,k,target,list);
            }
        }
    }
    
};
