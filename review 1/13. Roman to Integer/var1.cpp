class Solution {
public:
    int romanToInt(string s) 
    {
        char pre = ' ';
        int size = s.size();
        int sum = 0;
        for(int i = size - 1 ; i>= 0; i--)
        {
            switch(s[i])
            {
                case 'I':
                    {
                        if(pre == 'V'||pre == 'X')
                        {
                            sum -= 1; 
                        }
                        else
                        {
                            sum += 1;
                        }
                        break; 
                    }
                case 'V':
                    {
                        sum += 5;
                        break; 
                    }
                case 'X':
                    {
                        if(pre == 'L'||pre == 'C')
                        {
                            sum -= 10; 
                        }
                        else
                        {
                            sum += 10;
                        }
                        break; 
                    }
                case 'L':
                    {
                        sum += 50;
                        break; 
                    }
                case 'C':
                    {
                        if(pre == 'D'||pre == 'M')
                        {
                            sum -= 100; 
                        }
                        else
                        {
                            sum += 100;
                        }
                        break; 
                    }
                case 'D':
                    {
                        sum += 500;
                        break; 
                    }
                case 'M':
                    {
                        sum += 1000;
                        break; 
                    }
            }
            pre = s[i];
        }
        return sum;
    }
};