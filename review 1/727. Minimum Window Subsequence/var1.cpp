class Solution {
public:
    string minWindow (const string& S, const string& T)
    {
        auto i=-1;
        string ans;

        // 從 i + 1 開始檢查
        while (true)
        {
            // 先確定 T 是不是 S[i+1] 的 sub-sequence
            for (auto c: T)
            {
                // 從 S 的 i+1 開始找，到 S.end() 為止，找第一個 c 出現的 index
                i = S.find_first_of(c, i+1);
                if (string::npos == i)  // not found
                {
                    // T 不是 S[i+1] 的 sub-sequence
                    return ans;
                }
            }

            // 到這裡:
            //        T 為 S[i+1] 開始的 sub-sequence
            //        i 指向 S[i+1] 開始的 sub-sequence 的最後一個字元(即: T.back())位置

            int endOfSubSeq =++i;   // endOfSubSeq 為 "pass the end", 就是比最後一個字元還右邊一格

            // 找出 T.front() "依 T 的順序" 在 S 中 "最後" 出現的 index
            for (int j = T.size()-1; 0 <= j; j--)
            {
                // 從 S[0] 開始找, 到 S[i-1] 為止，找到 T[j] 最後出現的 index
                i = S.find_last_of(T[j], i-1);
            }

            // 到這裡:
            //        i 指向 S[i+1]開始的 sub-sequence 的 "頭"
            const auto len = endOfSubSeq - i;


            // 如果: "之前沒有答案" 或 "目前的答案比之前的答案短"
            if ("" == ans || len < ans.size())
            {
                // 用目前答案取代之前的答案
                ans = S.substr(i++, len);
            }
        }
    }
};
