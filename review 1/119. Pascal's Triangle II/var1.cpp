class Solution {
public:
    vector<int> getRow(int rowIndex) 
    {
        vector<int> res(rowIndex + 1 ,1);
        vector<int> pre(rowIndex + 1 ,1);
        for(int i =2 ;i <=rowIndex; i++)
        {
            for(int j = 1; j < i; j++)
            {
                pre[j] = res[j]+res[j-1];
            }
            res = pre;
        }
        return res;
    }
};