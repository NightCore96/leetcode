class Solution {
public:
    string nextClosestTime(string time)
    {
        int total = 1440;
        unordered_set<int> table;
        
        int t1 = time[0] - '0';
        int t2 = time[1] - '0';
        int t3 = time[3] - '0';
        int t4 = time[4] - '0';
        table.insert(t1);
        table.insert(t2);
        table.insert(t3);
        table.insert(t4);
        int now = t1 * 600 + t2* 60 + t3 * 10 + t4;
        for(int i = now +1 ; i < now + total ; i++ )
        {
            int t = i % total;
            int h1 = t / 600;
            if(!table.count(h1))
                continue;
            int h2 = (t % 600) / 60;
            if(!table.count(h2))
                continue;
            int h3 = (t % 600) % 60 / 10;
            if(!table.count(h3))
                continue;
            int h4 = (t % 600) % 60 % 10;
            if(!table.count(h4))
                continue;
            return to_string(h1)+to_string(h2)+":"+to_string(h3)+to_string(h4);
        }
        return time;
    }
};
