class Solution {
public:
    string addBinary(string a, string b) 
    {
        string res;
        int c = 0;
        int i = 0;
        reverse(a.begin(),a.end());
        reverse(b.begin(),b.end());
        while(i < a.size()||i < b.size())
        {
            int n = 0;
            if(i < a.size())
            {
                //cout << a[i] <<"  ";
                n += (a[i] - '0');
            }
                
            if(i < b.size())
            {
                //cout << b[i] <<"  ";
                n += (b[i] - '0');
            }
                
            //cout<<endl;
            n += c;
            char t  = (n % 2) + '0' ;
            
            res =  t + res;
            c = n/2; 
            i++;
        }
        if(c > 0)
        {
            char t  = c + '0';
            res = t + res;
        }
            
        return res;
    }
};