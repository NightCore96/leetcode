class Solution {
public:
    int jump(vector<int>& nums) 
    {
        int res = 0; // the number of step 
        int cutR = 0; // now you can walk
        int cutMX = 0; // max 
        for(int i=0;i<nums.size();++i)
        {
            if(i>cutR)
            {
                res++;
                cutR = cutMX;
            }
            cutMX = max(cutMX,i+nums[i]);
        }
        return res;
    }
};