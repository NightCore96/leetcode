class Solution {
public:
    vector<string> topKFrequent(vector<string>& words, int k)
    {
        unordered_map<string,int> table;
        for(auto&  w:words)
        {
            table[w]++;
        }
        auto sort = [](pair<int,string>& a,pair<int,string>b)
        {
            if(a.first == b.first)
                return a.second > b.second;
            return a.first < b.first;
        };
        priority_queue<pair<int,string>,vector<pair<int,string>>,decltype(sort)> pq(sort);
        for(auto& p:table)
        {
            pq.push({p.second,p.first});
        }
        vector<string> res;
        for(int i = 0;i<k;i++)
        {
            res.push_back(pq.top().second);
            pq.pop();
        }
        return res;
    }
};
