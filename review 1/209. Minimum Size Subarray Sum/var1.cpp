class Solution {
public :
    int minSubArrayLen(int s, vector<int>& nums)
    {
        int start = -1;
        int res = INT_MAX;
        int sum = 0;
        int size = nums.size();
        for(int i = 0;i < size;i++)
        {
            if(sum < s)
                sum += nums[i];
            while(sum >= s)
            {
                res = min(res,i-start);
                start++;
                sum -= nums[start];
            }
        }
        return res == INT_MAX ? 0 : res;
    }
};
