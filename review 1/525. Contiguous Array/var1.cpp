class Solution {
public:
    int findMaxLength(vector<int>& nums)
    {
        int res = 0;
        unordered_map<int,int> table;
        int cur0 = 0;
        int cur1 = 0;
        for(int i = 0 ; i< nums.size();i++)
        {
            int n = nums[i];
            if( n == 1)
                cur1++;
            if( n == 0)
                cur0++;
            if(cur1 == cur0)
            {
                res = max(res,i+1);
            }
            if( table.count(cur1-cur0))
            {
                res = max(res,i-table[cur1 - cur0] );
            }
            else
            {
                table[cur1 - cur0] = i;
            }
        }
        return res;
        
    }
};
