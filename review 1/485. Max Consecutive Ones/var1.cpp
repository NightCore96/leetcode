class Solution {
public:
    int findMaxConsecutiveOnes(vector<int>& nums)
    {
        int res = 0;
        int count = 0;
        for(int& i :nums)
        {
            if(i == 1)
            {
                count++;
            }
            else
            {
                //res = max(count,res);
                count = 0;
            }
            res = max(count,res);
        }
        //res = max(count,res);
        return res;
    }
};
