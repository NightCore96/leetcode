class Solution {
public:
    bool checkRecord(string s)
    {
        int a = 0;
        int l = 0;
        for(int i = 0;i < s.size() ; i++)
        {
            
            if(s[i] == 'A')
                a++;
            if(a > 1)
                return false;
            if(s[i] == 'L')
                l++;
            else
                l = 0;
            if( l > 2)
                return false;
        }
        return true;
    }
};
