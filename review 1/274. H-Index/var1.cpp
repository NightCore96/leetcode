class Solution {
public:
   int hIndex(vector<int>& C)
   {
        int n = C.size();
        vector<int> S(n+1);
        for (int i = 0; i < n; i++)
            ++S[min(n, C[i])];
        for (int i = n-1; i >= 0; i--)
            S[i] += S[i+1];
        int l = 0, h = n, r = 0;
        while (l <= h) {
            int m = (l + h) / 2;
            r = max(r, min(S[m], m));
            if (S[m] >= m)
                l = m+1;
            else
                h = m-1;
        }
        return r;
    }
};
