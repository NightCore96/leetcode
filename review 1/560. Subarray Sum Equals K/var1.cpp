class Solution {
public:
    int subarraySum(vector<int>& nums, int k)
    {
        int res = 0;
        unordered_map<int,int> sums;
        int sum = 0;
        for(int n: nums)
        {
            sum += n;
            if(sum == k)
                res++;
            if(sums.count(sum-k))
                res += sums[sum-k];
            sums[sum] ++;
        }
        return res;
    }
};
