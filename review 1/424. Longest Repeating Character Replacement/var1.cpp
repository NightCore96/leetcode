class Solution {
public:
    int characterReplacement(string s, int k)
    {
        unordered_map<char,int> list;
        int start = 0;
        int main  = 0;
        int res = 0;
        for(int i = 0; i < s.size();i++)
        {
            list[s[i]]++;
            main = max(list[s[i]],main);
            while( i - start + 1 - main > k)
            {
                list[s[start]]--;
                if(list[s[start]] < 0 )
                    list[s[start]] = 0;
                start++;
            }
            res = max(res,i - start + 1);
        }
        return res;
    }
};
