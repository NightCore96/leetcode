/**
 * // This is the BinaryMatrix's API interface.
 * // You should not implement it, or speculate about its implementation
 * class BinaryMatrix {
 *   public:
 *     int get(int row, int col);
 *     vector<int> dimensions();
 * };
 */

class Solution {
public:
    int leftMostColumnWithOne(BinaryMatrix &binaryMatrix)
    {
        auto rc = binaryMatrix.dimensions();
        int rows = rc[0];
        int clos = rc[1];
        int res = INT_MAX;
        int i = 0;
        int j = clos -1;
        while(i < rows && j >= 0)
        {
            if(binaryMatrix.get(i,j) == 1)
                j--;
            else
                i++;
        }
        return j==clos - 1 ? -1 : j+1;
    }
};
