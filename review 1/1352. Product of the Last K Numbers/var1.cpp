class ProductOfNumbers {
public:
    ProductOfNumbers() 
    {
        buffer.clear();
    }
    
    void add(int num) 
    {
        if(num != 1)
        {
            for(int i = 0; i < buffer.size(); i++)
            {
                buffer[i] *= num;
            }
        }
        buffer.push_back(num);
    }
    
    int getProduct(int k) 
    {
        int i = buffer.size() - k;
        return buffer[i];
    }
private:
    vector<int> buffer;
    
};

/**
 * Your ProductOfNumbers object will be instantiated and called as such:
 * ProductOfNumbers* obj = new ProductOfNumbers();
 * obj->add(num);
 * int param_2 = obj->getProduct(k);
 */