class Solution {
public:
    int numberOfPatterns(int m, int n)
    {
        int res = 0;
        vector<bool> visited(10, false);
        vector<vector<int>> jumps(10, vector<int>(10, 0));
        jumps[1][3] = jumps[3][1] = 2;
        jumps[4][6] = jumps[6][4] = 5;
        jumps[7][9] = jumps[9][7] = 8;
        jumps[1][7] = jumps[7][1] = 4;
        jumps[2][8] = jumps[8][2] = 5;
        jumps[3][9] = jumps[9][3] = 6;
        jumps[1][9] = jumps[9][1] = jumps[3][7] = jumps[7][3] = 5;
        res += dfs(1,1,m,n,0,jumps,visited)*4;
        res += dfs(2,1,m,n,0,jumps,visited)*4;
        res += dfs(5,1,m,n,0,jumps,visited);
        
        return res;
    }

    int dfs(int now,int step,int m,int n,int res,vector<vector<int>> &jumps,vector<bool> &visited)
    {
        if(step>=m)
            res++;
        step++;
        if(step>n)
            return res;
        visited[now] = true;
        for(int i=1;i<=9;i++)
        {
            int j = jumps[now][i];
            if(!visited[i]&&(j==0||visited[j]))
            {
                res = dfs(i,step,m,n,res,jumps,visited);
            }
        }
        visited[now] = false;
        return res;
    }
};
