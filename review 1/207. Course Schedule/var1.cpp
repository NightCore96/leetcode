class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) 
    {
        vector<vector<int>> graph(numCourses);
        vector<int> indegree(numCourses,0);
        for(auto v:prerequisites)
        {
            graph[v[1]].push_back(v[0]);
            indegree[v[0]]++;
        }
        queue<int> q;
        for(int i=0;i<numCourses;i++)
        {
            if(indegree[i]==0)
            {
                q.push(i);
            }
        }
        while(!q.empty())
        {
            int top = q.front();
            q.pop();
            for(int i:graph[top])
            {
                indegree[i]--;
                if(indegree[i]==0)
                    q.push(i);
            }
        }
        for(int i:indegree)
        {
            if(i>0)
                return false;
        }
        return true;
    }
};