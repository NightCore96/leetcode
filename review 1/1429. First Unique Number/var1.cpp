class FirstUnique {
public:
    unordered_map<int,int>m;
    queue<int>uniq;
    FirstUnique(vector<int>& nums) {
        // m.clear();
        // uniq=queue<int>();
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
        for(auto num:nums) m[num]++;
        for(auto num:nums)
            if(m[num]==1) uniq.push(num);
    }
    
    int showFirstUnique() {
        if(uniq.empty()) return -1;
        return uniq.front();
    }
    
    void add(int value) {
        m[value]++;
        if(m[value]==1) uniq.push(value);
        else
            while(!uniq.empty() && m[uniq.front()]>1) uniq.pop();
        return ;
    }
};
