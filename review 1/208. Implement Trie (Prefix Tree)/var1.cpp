class Trie {
public:
    /** Initialize your data structure here. */
    Trie() 
    {
        m_root = new TrieNode();
    }
    
    /** Inserts a word into the trie. */
    void insert(string word) 
    {
        TrieNode* node = m_root;
        for(auto c:word)
        {
            int n = c - 'a';
            if(!node->next[n])
                node->next[n] = new TrieNode();
            node = node->next[n];
        }
        node->isWord = true; 
    }
    
    /** Returns if the word is in the trie. */
    bool search(string word) 
    {
        TrieNode* node = find(word);
        return node&&node->isWord;
    }
    
    /** Returns if there is any word in the trie that starts with the given prefix. */
    bool startsWith(string prefix) 
    {
        return find(prefix) != nullptr;
    }
private:
    const static int MAXLENGTH = 'z'-'a'+1;
    struct TrieNode
    {
        TrieNode() = default;
        bool isWord = false;
        TrieNode*  next[MAXLENGTH] = {nullptr};
    };
    
    TrieNode* m_root;
    
    TrieNode* find(string word)
    {
        TrieNode* node = m_root;
        for(auto c:word)
        {
            int n = c - 'a';
            if(!node->next[n])
                return nullptr;
            node = node->next[n];
        }
        return node;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */