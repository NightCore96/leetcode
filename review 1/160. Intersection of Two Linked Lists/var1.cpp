/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) 
    {
        if(!headA||!headB)
            return nullptr;
        ListNode *n1 = headA;
        ListNode *n2 = headB;
        int l1 = 0, l2 = 0;
        while(n1)
        {
            n1 = n1->next;
            l1++;
        }
        while(n2)
        {
            n2 = n2->next;
            l2++;
        }
        if(l1>l2)
        {
            n1 = headA;
            n2 = headB;
        }
        else
        {
            n1 = headB;
            n2 = headA;
            swap(l1,l2);
        }
        for(int i=0;i<l1-l2;i++)
        {
            n1= n1->next;
        }
        for(int i=0;i<l2;i++)
        {
            if(n1 == n2)
                return n1;
            n1 = n1->next;
            n2 = n2->next;
        }
        return nullptr;
    }
    
};