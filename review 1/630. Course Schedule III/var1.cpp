class Solution {
public:
    int scheduleCourse(vector<vector<int>>& courses)
    {
        int res = 0;
        sort(courses.begin(),courses.end(),
             [](vector<int>& a,vector<int>& b)
             {
                 if(a[1] == b[1])
                     return a[0] < b[0];
                 return a[1] < b[1];
             });
        int now = 0;
        priority_queue<int> pq;
        for(auto &v:courses)
        {
            //cout<< v[0] <<" "<< v[1]<<endl;
            now += v[0];
            pq.push(v[0]);
            if(now > v[1])
            {
                now -= pq.top();
                pq.pop();
            }
        }
        return pq.size();
    }
};
