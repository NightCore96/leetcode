class Solution {
public:
    int pivotIndex(vector<int>& nums)
    {
        if(nums.empty())
            return -1;
        vector<int> sums;
        int sum = 0;
        for(auto& n:nums)
        {
            sum+= n;
            sums.push_back(sum);
        }
        for(int i = 0; i < sums.size();i++)
        {
            int l = i==0 ? 0 : sums[i-1];
            int r = i==sums.size()-1? 0 : (sums[sums.size()-1] -sums[i]);
            //cout<< l <<" " << r<<endl;
            if(l ==r)
                return i;
        }
        return -1;
    }
};
