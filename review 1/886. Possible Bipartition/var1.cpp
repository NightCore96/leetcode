class Solution {
public:
    bool possibleBipartition(int N, vector<vector<int>>& dislikes)
    {
        vector<int> people(N+1,0);
        unordered_map<int,vector<int>> graph;
        for(auto& d:dislikes)
        {
            graph[d[0]].push_back(d[1]);
            graph[d[1]].push_back(d[0]);
        }
        
        for(int i = 1;i<=N;i++)
        {
            if(people[i] == 0 && !dfs(graph,i,people,1))
                return false;
        }
        return true;
    }
    
    bool dfs(unordered_map<int,vector<int>>& graph,int now, vector<int>& people,int color)
    {
        people[now] = color;
        for(auto& g:graph[now])
        {
            if(people[g] == color)
                return false;
            if(people[g] == 0 && !dfs(graph,g,people,color * -1))
                return false;
        }
        //people[now] = 0;
        return true;
    }
};
