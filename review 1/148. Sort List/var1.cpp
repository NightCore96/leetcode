/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* sortList(ListNode* head) 
    {
        if(!head||!head->next)
            return head;
        ListNode* p1 = head;
        ListNode* p2 = head->next;
        while(p2&&p2->next)
        {
            p1 = p1->next;
            p2 = p2->next->next;
        }
        p2 = p1->next; 
        p1->next = nullptr;
        p1 = sortList(head);
        p2 = sortList(p2);
        
        return mergeTwoLists(p1,p2);
    }
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) 
    {
        ListNode* newlist = new ListNode(0);
        ListNode* head;
        head = newlist;
        if(l1==nullptr&&l2==nullptr)
            return nullptr;
        while(l1||l2)
        {
            if(!l1||l2&&l2->val<=l1->val)
            {
                newlist->next = l2;
                l2=l2->next;
            }
            else
            {
                newlist->next = l1;
                l1=l1->next;
            }
            newlist = newlist->next; 
        }
        return head->next;
    }
};