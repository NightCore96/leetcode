class Solution {
public:
    vector<vector<int>> pacificAtlantic(vector<vector<int>>& matrix)
    {
        vector<vector<int>> res;
        if(matrix.empty() || matrix[0].empty())
            return res;
        int m = matrix.size();
        int n = matrix[0].size();
        vector<vector<bool>> pvisit(m,vector<bool>(n,false));
        vector<vector<bool>> avisit(m,vector<bool>(n,false));
        for(int i = 0;i< m;i++)
        {
            dfs(i,0,INT_MIN,matrix,pvisit);
            dfs(i,n-1,INT_MIN,matrix,avisit);
        }
        for(int i = 0;i<n;i++)
        {
            dfs(0,i,INT_MIN,matrix,pvisit);
            dfs(m -1,i,INT_MIN,matrix,avisit);
        }
        for(int i = 0;i< m;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(avisit[i][j]&&pvisit[i][j])
                    res.push_back({i,j});
            }
        }
        return res;
    }
    
    void dfs(int x,int y,int pre,vector<vector<int>>& matrix,vector<vector<bool>>& visit)
    {
        if(x < 0 || x >= matrix.size() ||
           y < 0 || y >= matrix[0].size() ||
           visit[x][y] || matrix [x][y] < pre)
            return;
        visit[x][y] = true;
        dfs(x+1,y,matrix[x][y],matrix,visit);
        dfs(x-1,y,matrix[x][y],matrix,visit);
        dfs(x,y+1,matrix[x][y],matrix,visit);
        dfs(x,y-1,matrix[x][y],matrix,visit);
    }
};
