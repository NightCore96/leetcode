class Solution {
public:
    int cherryPickup(vector<vector<int>>& grid)
    {
        int n =grid.size();
        vector<vector<vector<int>>> dp(n,vector<vector<int>>(n,vector<int>(n,INT_MIN)));
        return max(0,dfs(0,0,0,grid,dp,n));
    }
private:
    int dfs(int x,int y,int back_x,vector<vector<int>>& grid,vector<vector<vector<int>>>& dp,int& n)
    {
        int back_y = x + y -  back_x;
        if(x == n || y == n || back_x == n || back_y == n)
            return INT_MIN;
        if(grid[x][y] == -1 || grid[back_x][back_y] == -1)
            return INT_MIN;
        if(x == n-1 && y == n-1)
            return grid[x][y];
        if(dp[x][y][back_x] != INT_MIN)
            return dp[x][y][back_x];
        int res = grid[x][y];
        if(x != back_x)
            res+=grid[back_x][back_y];
        res += max ({dfs(x+1,y,back_x,grid,dp,n),dfs(x,y+1,back_x,grid,dp,n),dfs(x+1,y,back_x+1,grid,dp,n),dfs(x,y+1,back_x+1,grid,dp,n)});
        dp[x][y][back_x] = res;
        return res;
    }
};
