class Solution {
public:
    vector<vector<int>> combinationSum3(int k, int n)
    {
        vector<vector<int>>  res;
        vector<int> temp;
        helper(res,temp,k,0,0,n);
        return res;
    }
    
    void helper(vector<vector<int>>& res,vector<int>& temp,int& k,int now,int sum, int& target)
    {
        if(temp.size() > k || sum > target)
            return;
        if(temp.size() == k && sum == target)
        {
            res.push_back(temp);
            return;
        }
        for(int i = now+1;i<=9;i++)
        {
            temp.push_back(i);
            helper(res,temp,k,i,sum+i,target);
            temp.pop_back();
        }
    }
    
};
