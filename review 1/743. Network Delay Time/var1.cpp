class Solution {
public:
    int networkDelayTime(vector<vector<int>>& times, int N, int K)
    {
        unordered_map<int,unordered_map<int,int>> network;
        for(auto&v : times)
        {
            network[v[0]][v[1]] = v[2];
        }
        vector<int> cost(N,INT_MAX);
        queue<int> q;
        q.push(K);
        cost[K - 1] = 0;
        while(!q.empty())
        {
            int cur = q.front();
            q.pop();
            for(auto& p:network[cur])
            {
                int next = p.first;
                int path = p.second;
                if(cost[next - 1] >  cost[cur - 1] + path )
                {
                    //cout<< next<<endl;
                    cost[next - 1] = cost[cur - 1] + path;
                    q.push(next);
                }
            }
        }
        int res = 0;
        for(auto& s: cost)
        {
            if(s == INT_MAX)
                return -1;
            res = max(res,s);
        }
        return res;
    }
};
