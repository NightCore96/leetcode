class Solution {
public:
    int numTrees(int n) 
    {
        if(n<=1)
            return 1;
        vector<int> dp(n+1,0);
        dp[0] = dp[1] = 1;
        for(int i=2;i<=n;i++)
        {
            for(int j=i;j>0;j--)
            {
                dp[i] += dp[i-j]*dp[j-1];
            }
        }
        return dp[n];
    }
};