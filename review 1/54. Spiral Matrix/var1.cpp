class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) 
    {
        
        vector<int> res;
        if(matrix.empty())
            return res;
        int lu = 0;
        int ld = 0;
        int rd = matrix.size()-1;
        int ru = matrix[0].size()-1;
        while(lu <= ru && ld <= rd)
        {
            for(int i=lu;i<=ru;i++)
                res.push_back(matrix[lu][i]);
            for(int i=lu+1;i<=rd;i++)
                res.push_back(matrix[i][ru]);
            for(int i=ru-1;rd!=ld&&i>=ld;i--)
                res.push_back(matrix[rd][i]);
            for(int i=rd-1;ru!=lu&&i>lu;i--)
                res.push_back(matrix[i][ld]);
            lu++;
            ru--;
            ld++;
            rd--;
        }
        return res;
    }
};