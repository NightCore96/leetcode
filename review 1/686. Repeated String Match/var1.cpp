class Solution {
public:
    int repeatedStringMatch(string A, string B)
    {
        int res = 1;
        string t = A;
        while(t.size() < B.size())
        {
            t += A;
            res++;
        }
        if(t.find(B) != string::npos)
            return res;
        t += A;
        res++;
        return (t.find(B) != string::npos) ? res : -1;
    }
};
