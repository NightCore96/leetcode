class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        int N = grid.size();
        int M = grid[0].size();
        vector<vector<int>> d(N,vector<int>(M,0));
        d[0][0]=grid[0][0];
        for(int i=1;i<M;i++)
        {
            d[0][i]=grid[0][i]+d[0][i-1];
        }
        for(int i=1;i<N;i++)
        {
            d[i][0]=grid[i][0]+d[i-1][0];
        }
        for(int i=1;i<M;i++)
        {
            for(int j=1;j<N;j++)
            {
                d[j][i]=grid[j][i]+min(d[j][i-1],d[j-1][i]);
            }
        }
        return (int)d[N-1][M-1];
    }
};