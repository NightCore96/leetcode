 class Solution {
 public:
     vector<int> addToArrayForm(vector<int>& A, int K)
     {
         int index = A.size() - 1;
         int carry = 0;
         while(index >= 0)
         {
             int add = K % 10;
             A[index] = (A[index] + carry + add);
             carry =  A[index] / 10;
             A[index] %= 10;
             K /= 10;
             index--;
         }
         
         while(K > 0 || carry >0)
         {
             //cout << K << " " << carry<<endl;
             int add = K % 10;
             A.insert(A.begin(),(add + carry) % 10);
             carry = (add + carry) /10;
             K/= 10;
         }
         return A;
     }
 };
