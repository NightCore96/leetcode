class Solution {
public:
    void moveZeroes(vector<int>& nums) 
    {
        int index = 0;
        for(int i : nums)
        {
            if(i == 0)
                continue;
            nums[index] = i;
            index++;
        }
        for(int i = index ; i < nums.size();i++)
        {
            nums[i] = 0;
        }
    }
};