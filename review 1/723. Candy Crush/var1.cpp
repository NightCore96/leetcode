class Solution {
public:
    vector<vector<int>> candyCrush(vector<vector<int>>& board)
    {
        vector<vector<int>> point;
        do
        {
            point.clear();
            CrushRow(board,point);
            CrushCul(board,point);
            clear(board,point);
            drop(board);
        }
        while(!point.empty());
        return board;
    }
    
    void CrushRow(vector<vector<int>>& board,vector<vector<int>>& point)
    {
        for(int i = 0;i<board.size();i++)
        {
            int same = 1;
            for(int j = 1;j<board[0].size();j++)
            {
                if(board[i][j]==0)
                    continue;
                if(board[i][j] == board[i][j-1])
                {
                    same++;
                    if(same == 3)
                    {
                        point.push_back({i,j});
                        point.push_back({i,j-1});
                        point.push_back({i,j-2});
                    }
                    if(same > 3)
                        point.push_back({i,j});
                }
                else
                {
                    same = 1;
                }
                    
            }
        }
    }
    
    void CrushCul(vector<vector<int>>& board,vector<vector<int>>& point)
    {
        for(int i = 0;i<board[0].size();i++)
        {
            int same = 1;
            for(int j = 1;j<board.size();j++)
            {
                if(board[j][i]==0)
                    continue;
                if(board[j-1][i] == board[j][i])
                {
                    same++;
                    if(same == 3)
                    {
                        point.push_back({j,i});
                        point.push_back({j-1,i});
                        point.push_back({j-2,i});
                    }
                    if(same > 3)
                        point.push_back({j,i});
                }
                else
                {
                    same = 1;
                }
                    
            }
        }
    }
    
    void clear(vector<vector<int>>& board,vector<vector<int>>& point)
    {
        for(auto& v:point)
        {
            board[v[0]][v[1]] = 0;
        }
    }
    
    void drop(vector<vector<int>>& board)
    {
        for(int i = 0;i<board[0].size();i++)
        {
            int index = board.size() -1;
            for(int j = board.size() -1;j >=0;j--)
            {
                if(board[j][i]!=0)
                {
                    swap(board[j][i],board[index][i]);
                    index--;
                }
            }
        }
    }
};
