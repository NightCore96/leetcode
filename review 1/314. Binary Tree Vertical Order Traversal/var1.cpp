/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> verticalOrder(TreeNode* root)
    {
        vector<vector<int>> res;
        if(!root)
            return res;
        unordered_map<int,vector<int>> list;
        queue<pair<TreeNode*,int>> q;
        q.push({root,0});
        int l = INT_MAX;
        int r = INT_MIN;
        while(!q.empty())
        {
            auto p= q.front();
            q.pop();
            l = min(l,p.second);
            r = max(r,p.second);
            list[p.second].push_back(p.first->val);
            if(p.first->left)
                q.push({p.first->left,p.second - 1});
            if(p.first->right)
                q.push({p.first->right,p.second + 1});
        }
        for(int i = l ;i<= r;i++)
        {
            if(list.count(i))
                res.push_back(list[i]);
        }
        return res;
    }
};
