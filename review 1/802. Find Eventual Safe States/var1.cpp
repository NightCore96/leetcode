class Solution {
public:
    vector<int> eventualSafeNodes(vector<vector<int>>& graph)
    {
        vector<int> res;
        int N = graph.size();
        vector<int> visited(N,0);
        for(int i = 0;i<N;i++)
        {
            if(dfs(graph,i,visited))
                res.push_back(i);
        }
        return res;
    }
    
    bool dfs(vector<vector<int>>& graph,int now,vector<int>& visited)
    {
        if(visited[now] != 0)
            return visited[now] != 1;
        visited[now] = 1;
        for(auto& i:graph[now])
        {
            if(!dfs(graph,i,visited))
                return false;
        }
        visited[now] = -1;
        return true;
    }
};
