class Solution
{
private:
    stack<long long> num_stk;
    stack<char> op_stk;
    void computer()
    {
        auto op = op_stk.top();
        if(op == '(')
            return;
        op_stk.pop();
        auto b = num_stk.top();
        num_stk.pop();
        auto a = num_stk.top();
        num_stk.pop();
        //cout << a << " " << b <<endl;
        switch(op)
        {
            case '+':
                num_stk.push(a+b);
                break;
            case '-':
                num_stk.push(a-b);
                break;
            case '*':
                num_stk.push(a*b);
                break;
            case '/':
                num_stk.push(a/b);
                break;
        }
    }
    void handleSpace(string& s,int &i)
    {
        while(s[i] == ' ')
            i++;
    }
    void handleNumber(string& s,int &i)
    {
        string num = "";
        while(s[i] >= '0' && s[i] <= '9')
        {
            num += s[i];
            i++;
        }
        num_stk.push(stol(num));
    }
    void handleOps(string& s,int &i)
    {
        char op = s[i];
        i++;
        switch(op)
        {
            case '+':
                while(!op_stk.empty()&&(op_stk.top() == '*' || op_stk.top() == '/' || op_stk.top() == '-'))
                    computer();
                op_stk.push(op);
                break;
            case '-':
                while(!op_stk.empty()&&(op_stk.top() == '*' || op_stk.top() == '/' || op_stk.top() == '-'))
                    computer();
                op_stk.push(op);
                break;
            case '*':
                while(!op_stk.empty()&&op_stk.top() == '/')
                    computer();
                op_stk.push(op);
                break;
            case '/':
                while(!op_stk.empty()&&op_stk.top() == '/')
                    computer();
                op_stk.push(op);
                break;
            case '(':
                op_stk.push(op);
                break;
            case ')':
                 while(!op_stk.empty())
                 {
                    computer();
                    if(op_stk.top() == '(')
                        break;
                 }
                    
                op_stk.pop();
                break;
        }
        
    }
public:
    int calculate(string s)
    {
        int i = 0;
        //cout<< s <<endl;
        while(i < s.size())
        {
            
            //cout<<i<<endl;
            if(s[i] == ' ')
            {
                handleSpace(s,i);
            }
            else if(s[i] >= '0' && s[i] <= '9')
            {
                handleNumber(s,i);
            }
            else
            {
                handleOps(s,i);
            }
        }
        while(num_stk.size() > 1)
        {
            computer();
        }
        return num_stk.top();
    }
};
