/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node() {}

    Node(int _val, Node* _next, Node* _random) {
        val = _val;
        next = _next;
        random = _random;
    }
};
*/
class Solution {
public:
    Node* copyRandomList(Node* head) 
    {
        if(!head)
            return nullptr;
        Node* res = new Node(head->val,nullptr,nullptr);
        unordered_map<Node*,Node*> rmap;
        Node* cur = head->next;
        Node* pre = res;
        rmap[head] = res;
        while(cur)
        {
            Node* n = new Node(cur->val,nullptr,nullptr);
            rmap[cur] = n;
            pre->next = n;
            pre = pre->next;
            cur = cur->next;
        }
        pre = res;
        cur = head;
        while(cur)
        {
            pre->random = rmap[cur->random];
            pre = pre->next;
            cur = cur->next;
        }
        return res;
    }
};