class Solution {
public:
    int maxEnvelopes(vector<vector<int>>& envelopes)
    {
        if(envelopes.empty())
            return 0;
        sort(envelopes.begin(),envelopes.end(),[](const vector<int>& a,const vector<int>& b)
             {
                 if(a[0]!=b[0])
                     return a[0] < b[0];
                 return a[1] > b[1];
             });
        vector<int> dp;
        for(const auto&e:envelopes)
        {
            int index = lower_bound(dp,e[1]);
            if(index == dp.size())
                dp.push_back(e[1]);
            else
                dp[index] = e[1];
        }
        return dp.size();
    }
    
    int lower_bound(vector<int>& piles, int val) {
        int left = 0, right = piles.size();
        
        while (left < right) {
            int mid = left + (right - left) / 2;
            
            if (piles[mid] >= val) {
                right = mid;
            } else {
                left = mid+1;
            }
        }
        
        return left;
    }
    
};
