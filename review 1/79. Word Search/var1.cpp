class Solution {
public:
    bool exist(vector<vector<char>>& board, string word) 
    {
        int M = board.size();
        int N = board[0].size();
        for(int i=0;i<M;i++)
        {
            for(int j=0;j<N;j++)
            {
                if(dfs(board,i,j,word,0))
                    return true;
            }
        }
        
        return false;
        
    }
    
    bool dfs(vector<vector<char>>& board,int x,int y, string& word,int pos)
    {
        bool res = false; 
        if(pos>=word.size())
            return true;
        if(x<0||y<0||x>=board.size()||y>=board[0].size()||board[x][y]!=word[pos])
            return false;
        auto temp = board[x][y];
        board[x][y] = '/0';  
        res = dfs(board,x+1,y,word,pos+1)||
              dfs(board,x-1,y,word,pos+1)||
              dfs(board,x,y+1,word,pos+1)||
              dfs(board,x,y-1,word,pos+1);
        board[x][y] = temp;
        return res;
    }
    
};