class SnapshotArray {
public:
    vector<map<int,int>> data;
    int timer;
    SnapshotArray(int length)
    {
        timer = 0;
        data = vector<map<int,int>>(length);
        for(int i = 0;i<length;i++)
        {
            data[i][timer] = 0;
        }
    }
    
    void set(int index, int val)
    {
        data[index][timer] = val;
    }
    
    int snap()
    {
        int res = timer;
        timer ++;
        return res;
    }
    
    int get(int index, int snap_id)
    {
        auto it = data[index].upper_bound(snap_id);
        if(it != data[index].begin())
            return (--it)->second;
        return it->second;
    }
};

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * SnapshotArray* obj = new SnapshotArray(length);
 * obj->set(index,val);
 * int param_2 = obj->snap();
 * int param_3 = obj->get(index,snap_id);
 */
