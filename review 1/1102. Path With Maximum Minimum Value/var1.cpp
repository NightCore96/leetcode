class Solution {
public:
    int maximumMinimumPath(vector<vector<int>>& A)
    {
        int res = 0;
        int m = A.size();
        int n = A[0].size();
        vector<vector<int>> dir = {{0,1},{0,-1},{1,0},{-1,0}};
        vector<vector<bool>> visited(A.size(),vector<bool>(A[0].size(),false));
        priority_queue<pair<int,vector<int>>> pq;
        pq.push({A[0][0],{0,0}});
        while(!pq.empty())
        {
            auto p = pq.top();
            pq.pop();
            int val = p.first;
            int x = p.second[0];
            int y = p.second[1];
            //cout << x <<" " << y <<endl;
            if( x == m -1 && y == n - 1)
                return val;
            for(const auto& d:dir)
            {
                int nextx = x + d[0];
                int nexty = y + d[1];
                if(nextx < 0 || nexty < 0 ||
                   nextx >= m || nexty >= n || visited[nextx][nexty] )
                    continue;
                visited[nextx][nexty] = true;
                pq.push({min(A[nextx][nexty],val),{nextx,nexty}});
            }
        }
        return -1;
        //return res;
    }
};s
