class Solution {
public:
    int maxAreaOfIsland(vector<vector<int>>& grid)
    {
        int res = 0;
        for(int i = 0;i<grid.size();i++)
        {
            for(int j = 0;j < grid[0].size();j++)
            {
                if(grid[i][j] == 1)
                {
                    int now = 0;
                    dfs(grid,now,res,i,j);
                }
            }
        }
        return res;
    }
    
    void dfs(vector<vector<int>>& grid,int& now,int &res,int x,int y)
    {
        if(x < 0 || y < 0 || x >= grid.size() || y >= grid[0].size())
            return;
        if(grid[x][y] != 1)
            return;
        now += 1 ;
        //cout<< now<<endl;
        res = max(res,now);
        grid[x][y] = -1;
        dfs(grid,now,res,x+1,y);
        dfs(grid,now,res,x-1,y);
        dfs(grid,now,res,x,y+1);
        dfs(grid,now,res,x,y-1);
    }
};
