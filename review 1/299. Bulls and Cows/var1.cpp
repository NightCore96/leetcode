class Solution {
public:
    string getHint(string secret, string guess)
    {
        int a = 0;
        int b = 0;
        vector<int> hash(10,0);
        for(int i = 0;i < secret.size();i++)
        {
            if(secret[i] == guess[i])
            {
                a++;
                guess[i] = '*';
            }
            else
            {
                hash[secret[i] - '0']++;
            }
        }
        for(auto &c:guess)
        {
            if(c != '*' && hash[c - '0'] > 0)
            {
                b++;
                hash[c - '0'] --;
            }
        }
        
        return to_string(a)+"A"+to_string(b)+"B";
    }
};
