/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int widthOfBinaryTree(TreeNode* root)
    {
        if(!root)
            return 0;
        int res = 0;
        queue<pair<TreeNode*,int>> q;
        q.push({root,1});
        while(!q.empty())
        {
            int l = q.front().second;
            int h;
            for(int i = q.size();i>0;i--)
            {
                auto p = q.front();
                q.pop();
                h = p.second;
                int id = h - l;
                if(p.first->left)
                    q.push({p.first->left,id*2});
                if(p.first->right)
                    q.push({p.first->right,id*2 + 1});
            }
            //cout<< l << " "<< h <<endl;
            res = max(res, h - l + 1);
        }
        return res;
    }
}; 
