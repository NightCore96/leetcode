class Solution {
public:
    vector<int> findDiagonalOrder(vector<vector<int>>& matrix)
    {
        if( matrix.empty()|| matrix[0].empty())
            return {};
        int m = matrix.size();
        int n = matrix[0].size();
        vector<deque<int>> oneD(m+n+1);
        vector<int> res;
        
        for(int i = 0;i < m;i++)
        {
            for(int j = 0; j < n ;j++)
            {
                if( (i+j) %2 == 0)
                {
                    oneD[i+j].push_front(matrix[i][j]);
                }
                else
                {
                    oneD[i+j].push_back(matrix[i][j]);
                }
            }
        }
        for(auto& v:oneD)
        {
            for(auto& i:v)
                res.push_back(i);
        }
        return res;
    }
};
