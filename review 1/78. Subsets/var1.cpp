class Solution {
public:
    vector<vector<int>> subsets(vector<int>& nums) 
    {
        vector<vector<int>> res;
        vector<int> buffer;
        if(nums.empty())
            return res;
        helper(nums,0,res,buffer);
        return res;
    }
    
    void helper(vector<int>& nums,int n,vector<vector<int>>& res,vector<int>& buffer)
    {
        res.push_back(buffer);
        for(int i=n;i<nums.size();i++)
        {
            buffer.push_back(nums[i]);
            helper(nums,i+1,res,buffer);
            buffer.pop_back();
        }
    }
};