class Solution {
public:
    bool isPeak(vector<int> &nums,int i)
    {
        if(nums.size() == 1)
            return true;
        if(i == 0 && nums[0] > nums[1])
            return true;
        if(i == nums.size() -1 && nums[ nums.size() -1] > nums[ nums.size() -2] )
            return true;
        if(i!= 0 && i != nums.size() &&nums[i -1] < nums[i] && nums[i+1] < nums[i])
            return true;
        return false;
    }
    int findPeakElement(vector<int>& nums)
    {
        int s = 0;
        int e = nums.size() -1;
        while(s < e)
        {
            int mid = s + (e - s)/2;
            if(isPeak(nums,mid))
                return mid;
            if(nums[mid] < nums[mid+1])
            {
                s = mid +1;
            }
            else
                e = mid;
        }
        return s;
    }
};
