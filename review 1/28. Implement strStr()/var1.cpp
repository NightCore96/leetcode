class Solution {
public:
    int strStr(string haystack, string needle) 
    {
        if(needle.empty())
            return 0;
        bool isSame = false;
        int hsize = haystack.size();
        int nsize = needle.size();
        for(int i = 0; i < hsize - nsize + 1; i++)
        {
            isSame = true;
            for(int j = 0; j< nsize ; j++)
            {
                if(haystack[i+j] != needle[j])
                {
                    isSame = false;
                    break;
                }
            }
            if(isSame)
                return i;
        }
        return -1;
    }
};