class Solution {
public:
    int shortestDistance(vector<vector<int>>& grid)
    {
        int r = grid.size();
        int c = grid[0].size();
        vector<vector<int>> dir = {{0,1},{1,0},{-1,0},{0,-1}};
        vector<vector<int>> total(r,vector<int>(c,0));
        vector<pair<int,int>> builds;
        int res = -1;
        bool alldetect = true;
        int walk=0;
        for(int i=0;i<r;i++)
        {
            for(int j=0;j<c;j++)
            {
                if(grid[i][j]==1)
                {
                    int minstep=-1;
                    queue<pair<int,int>> q;
                    q.push({i,j});
                    vector<vector<int>> di(r,vector<int>(c,0));
                    while(!q.empty())
                    {
                        pair<int,int> p = q.front();
                        q.pop();
                        for(auto d:dir)
                        {
                            int x = p.first+d[0];
                            int y = p.second+d[1];
                            if(x<0||y<0||x>=grid.size()||y>=grid[0].size()||grid[x][y]!=walk)
                                continue;
                            grid[x][y]--;
                            q.push({x,y});
                            int ox = p.first;
                            int oy = p.second;
                            di[x][y] = di[ox][oy]+1;
                            total[x][y]+=di[x][y];
                            if(minstep<0||minstep>total[x][y])
                            {
                                minstep = total[x][y];
                            }
                        }
                    }
                    res= minstep;
                    walk--;
                }
            }
        }
        return res;
    }
};
