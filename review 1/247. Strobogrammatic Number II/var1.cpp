class Solution {
public:
    vector<string> findStrobogrammatic(int n)
    {
        return helper(n,n);
    }
    
    vector<string> helper(int n , int t)
    {
        if( n  <=  0)
            return {""};
        if( n == 1)
            return {"0" ,"1" ,"8"};
        vector<string> num = helper(n - 2, t);
        vector<string>res;
        for(auto &s:num)
        {
            if(n != t)
                res.push_back("0"+s+"0");
            res.push_back("1"+s+"1");
            res.push_back("8"+s+"8");
            res.push_back("6"+s+"9");
            res.push_back("9"+s+"6");
        }
        return res;
    }
};
