class Solution {
public:
    int minMeetingRooms(vector<vector<int>>& intervals) 
    {
        vector<int> starts, ends;
        for (auto itv : intervals) {
            starts.push_back(itv[0]);
            ends.push_back(itv[1]);
        }
        sort(starts.begin(), starts.end());
        sort(ends.begin(), ends.end());
        int endpos = 0;
        int res = 0;
        for (int i = 0; i < starts.size(); ++i) {
            if (starts[i] < ends[endpos]) ++res;
            else ++endpos;
        }
        return res;
    }
};