/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) 
    {
        if(!head)
            return head;
        ListNode* res = new ListNode(0);
        res->next = head;
        ListNode* node = head;
        ListNode* pre = res;
        ListNode* next = node->next;
        for(int i = 1;i < m ;i++)
        {
            pre = pre->next;
        }
        node = pre->next;
        for(int i = m;i < n ;i++)
        {
            next = node->next;
            node->next = next->next;
            next->next = pre->next;
            pre->next = next;
        }
        return res->next;
    }
};