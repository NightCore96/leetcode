/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) 
    {
        vector<vector<int>> res;
        vector<int> level;
        if(!root)
            return res;
        sum -= root->val;
        level.push_back(root->val);
        helper(res,root,sum,level);
        return res;
    }
    
    void helper(vector<vector<int>> &res,TreeNode* root, int &sum,vector<int> &level)
    {
        if(!root)
            return;
        if(!root->left&&!root->right)
        {
            if(sum == 0)
            {
                res.push_back(level);
            }
            return;
        }
        if(root->left)
        {
            sum -= root->left->val;
            level.push_back(root->left->val);
            helper(res,root->left,sum,level);
            sum += root->left->val;
            level.pop_back();
        }
        if(root->right)
        {
            sum -= root->right->val;
            level.push_back(root->right->val);
            helper(res,root->right,sum,level);
            sum += root->right->val;
            level.pop_back();
        }
    }
};