class Solution {
public:
    int maxArea(vector<int>& height) 
    {
        if(height.size()<2)
            return 0;
        int n = height.size();
        int res = INT_MIN;
        int l = n-1,r = 0;
        int h = min(height[r],height[l]);
    
        while(r<l)
        {
            h = min(height[r],height[l]);
            res = max(res,h*(l-r));
            while(height[r]<=h&&r<l)
                r++;
            while(height[l]<=h&&r<l)
                l--;
        }
        return res;
    }
};