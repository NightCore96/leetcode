class Solution {
public:
    int openLock(vector<string>& deadends, string target)
    {
        if(target == "0000")
            return 0;
        unordered_set<string> end;
        for(auto&s : deadends)
        {
            if(s == "0000")
                return -1;
            end.insert(s);
        }
        int res = 0;
        queue<string> q;
        q.push("0000");
        while(!q.empty())
        {
            int size = q.size();
            res++;
            for(int i = 0;i<size;i++)
            {
                string cur = q.front();
                q.pop();
                auto next = nextNum(cur);
                for(auto&n : next)
                {
                    //cout<< n <<endl;
                    if(n == target)
                        return res;
                    if(!end.count(n))
                    {
                        end.insert(n);
                        q.push(n);
                    }
                }
                //cout<< endl;
            }
        }
        return -1;
    }
private:
    vector<string> nextNum(string& s)
    {
        vector<string> res;
        for(int i = 0 ; i <s.size();i++)
        {
            string t = s;
            t[i] = s[i]=='9'? '0': s[i]+1;
            res.push_back(t);
            t[i] = s[i]=='0'? '9': s[i]-1;
            res.push_back(t);
        }
        return res;
    }
};
