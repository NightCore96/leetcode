class Solution {
public:
    string stringShift(string s, vector<vector<int>>& shift)
    {
        int final = 0;
        for(auto a: shift)
        {
            if(a[0] == 0 )
                final += a[1];
            else
                final -= a[1];
        }
        cout<<final;
        if(final == 0)
            return s;
        
        if(final < 0) //right
        {
            final = abs(final);
            if(final > s.size())
                final %= s.size();
            for(int i = 0; i < final;i++)
            {
                s = s[s.length()-1] + s.substr(0,s.length()-1);
            }
        }
        else // left
        {
            final %= s.size();
            for(int i = 0; i < final;i++)
            {
                s = s.substr(1,s.length()-1) + s[0];
            }
        }
        return s;
    }
};
