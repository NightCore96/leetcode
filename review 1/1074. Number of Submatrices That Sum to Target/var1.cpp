class Solution {
public:
    int checksub(vector<vector<int>>& matrix,int r1,int r2,int target){
        unordered_map<int,int> m;
        int curr=0,ans=0;
        m[0]=1;
        //  ith element of 1D array = matrix[r2][i]-matrix[r1-1][i]
        for(int i=0;i<matrix[0].size();i++)
        {
            if(r1>0)
                curr+=matrix[r2][i]-matrix[r1-1][i];
            else
                curr+=matrix[r2][i];
            
            if(m.find(curr-target)!=m.end())
                ans+=m[curr-target];
            m[curr]++;
        }
        
        return ans;
    }
    
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target)
    {
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
        cout.tie(NULL);
        
        int m=matrix.size(),n=matrix[0].size();
        if(m==0 || n==0)
            return 0;
        vector<vector<int>> dp(m,vector<int>(n,0));
        for(int i=0;i<m;i++)       // Creating row prefix sum matrix
            for(int j=0;j<n;j++){
                if(i>0)
                    matrix[i][j]+=matrix[i-1][j];
            }
        int ans=0;
        for(int i=0;i<m;i++)     // For each row pair find 1D subarray sum with target
            for(int j=i;j<m;j++)
                ans+=checksub(matrix,i,j,target);
        return ans;
    }
};
