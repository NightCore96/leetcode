class Solution {
public:
    string fractionToDecimal(int numerator, int denominator)
    {
        string res;
        if(denominator==0)
            return "NaN";
        if(abs(denominator)==1)
        {
            res = to_string(numerator);
            if(denominator<0)
            {
                if(numerator>=0)
                {
                    res="-"+res;
                }
                else
                {
                    res=res.substr(1);
                }
            }
            return res;
        }
        long long dt=numerator/denominator;
        //cout << dt<<endl;
        long long t=numerator/denominator;
        long long s=abs(numerator%denominator);
        res=to_string(t);
        if(s==0)
            return res;
        if(t==0)
        {
            if((numerator>=0&&denominator<0)||(numerator<0&&denominator>0))
                res="-"+res;
        }
        res=res+".";
        unordered_map< long long,int> list;
        string smallnum;
        int index=0;
        list.insert(pair< long long,int>(s,index));
        s=s*10;
        while(s!=0)
        {
            //cout<<s<<endl;
            index++;
            long long next_t=abs(s/denominator);
            long long next_s=abs(s%denominator);
            smallnum+=to_string(next_t);
            if(next_s!=0)
            {
                auto it=list.find(next_s);
                if(it==list.end())
                {
                    list.insert(pair< long long,int>(next_s,index));
                }
                else
                {
                    int startID=it->second;
                    smallnum.insert(startID,"(");
                    smallnum+=")";
                    break;
                }
            }
            s=next_s*10;
        }
        res+=smallnum;
        return res;
    }
};
