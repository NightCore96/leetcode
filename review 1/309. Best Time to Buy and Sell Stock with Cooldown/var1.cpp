class Solution {
public:
    int maxProfit(vector<int>& prices) 
    {
        if(prices.size()<=1)
            return 0;
        int buy=0;
        int sell=-prices[0];
        int cooldown= INT_MIN;
        int i =1;
        while(i<prices.size())
        {
            int b=buy,c=cooldown,s=sell;
            buy=max(c,b);
            sell =max(b-prices[i],s);
            cooldown = s+prices[i];
            i++;
        }
        return max(cooldown,buy);
        
    }
};