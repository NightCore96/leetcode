struct LRUNode
{
    int key;
    int val;
    LRUNode* pre;
    LRUNode* next;
    LRUNode(int k,int v)
    {
        key = k;
        val = v;
        pre = nullptr;
        next = nullptr;
    }
};


class LRUCache 
{
private:
    unordered_map<int,LRUNode*> m_hashtable;
    int m_MaxCap;
    LRUNode* head;
    LRUNode* tail;
public:
    LRUCache(int capacity) 
    {
        m_MaxCap = capacity;
        head = new LRUNode(0,0);
        tail = new LRUNode(0,0);
        head->next = tail;
        tail->pre = head;
    }
    
    int get(int key) 
    {
        if(m_hashtable.find(key) != m_hashtable.end())
        {
            remove(m_hashtable[key]);
            insertHead(m_hashtable[key]);
            return m_hashtable[key]->val;
        }
        return -1;
    }
    
    void put(int key, int value) 
    {
        if(m_hashtable.find(key) != m_hashtable.end())
        {
            remove(m_hashtable[key]);
            insertHead(m_hashtable[key]);
            m_hashtable[key]->val = value;
            return;
        }
        if(m_hashtable.size()==m_MaxCap)
        {
            deleteTail();
        }
        LRUNode* temp = new LRUNode(key,value);
        m_hashtable.insert({key,temp});
        insertHead(temp);
    }
private:
    void insertHead(LRUNode* node)
    {
        node->pre = head;
        node->next = head->next;
        head->next = node;
        node->next->pre = node;
    }
    
    void remove(LRUNode* node)
    {
        node->pre->next = node->next;
        node->next->pre = node->pre;
    }
    
    void deleteTail()
    {
        LRUNode* temp = tail->pre;
        int k = temp->key;
        m_hashtable.erase(k);
        remove(temp);
        delete temp;
    }
    
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */