class Solution {
public:
    int myAtoi(string str) 
    {
        int res = 0;
        int start = 0;
        int size = str.size();
        while(start<size&&str[start] == ' ')
            start++;
        bool isMinus = false; 
        if(str[start] == '-'||str[start] == '+')
        {
            if(str[start] == '-')
                isMinus = true;
            start++;
        }
        for(int i = start ;i <size ; i++)
        {
            char c = str[i];
            if(isdigit(c))
            {
                if(!isMinus)
                {
                    int p = c - '0';
                    if(res > INT_MAX/10 || (res == INT_MAX/10 && p > 7 ))
                        return INT_MAX;
                    res = res * 10 + p;
                }
                else
                {
                    int p = (c - '0') * -1;
                    if(res < INT_MIN/10 || (res == INT_MIN/10 && p < -8 ))
                        return INT_MIN;
                    res = res * 10 + p;
                }
            }
            else
            {
                break;
            }
        }
        return res;
    }
};