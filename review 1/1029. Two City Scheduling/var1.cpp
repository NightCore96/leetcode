class Solution {
public:
    int twoCitySchedCost(vector<vector<int>>& costs)
    {
        priority_queue<pair<int,int>> pq;
        int N2 = costs.size();
        int n = N2/2;
        //cout << "N " <<n <<endl;
        int cost = 0;
        for(int i = 0;i<N2;i++)
        {
            pq.push({costs[i][0] - costs[i][1],i});
        }
        for(int i = 0;i<N2;i++)
        {
            auto p = pq.top();
            pq.pop();
            if(i >= n)
                cost += costs[p.second][0];
            else
                cost += costs[p.second][1];
        }
        return cost;
    }
};
