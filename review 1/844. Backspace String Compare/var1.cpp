class Solution {
public:
    bool backspaceCompare(string S, string T)
    {
        int i = 0;
        int j = 0;
        for(auto& c:S)
        {
            if(c == '#')
            {
                i = i > 1? --i: 0;
            }
            else
            {
                S[i] = c;
                i++;
            }
            //cout <<i <<endl;
        }
        
        for(auto& c:T)
        {
            if(c == '#')
            {
                j = j > 1? --j: 0;
            }
            else
            {
                T[j] = c;
                j++;
            }
            
        }
        
        cout<< j <<" " <<i<<endl;
        if(i != j)
            return false;
        for(int k = 0; k< i;k++)
        {
            if(S[k]!=T[k])
                return false;
        }
        
        return true;
    }
};
