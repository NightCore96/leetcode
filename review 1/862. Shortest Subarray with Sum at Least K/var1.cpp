class Solution {
public:
    int shortestSubarray(vector<int>& A, int K)
    {
        vector<int> sums;
        int sum = 0;
        sums.push_back(sum);
        for(int& i:A)
        {
            sum+=i;
            sums.push_back(sum);
        }
        
        int res = INT_MAX;
        deque<int> dq;
        for(int i =0;i<sums.size();i++)
        {
            while(!dq.empty() && sums[dq.back()] >= sums[i])
            {
                dq.pop_back();
            }
            while(!dq.empty() && sums[i] - sums[dq.front()] >=K)
            {
                int start = dq.front();
                dq.pop_front();
                res = min(res,i-start);
            }
            dq.push_back(i);
        }
        
        return res == INT_MAX? -1 : res;
    }
};
