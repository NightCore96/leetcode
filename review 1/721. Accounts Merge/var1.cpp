class Solution {
public:
    vector<vector<string>> accountsMerge(vector<vector<string>>& accounts)
    {
        unordered_map<string,int> index;
        unordered_map<int,unordered_set<int>> graph;
        for(int i = 0;i<accounts.size();i++)
        {
            int id = i;
            for(int j = 1;j < accounts[i].size();j++)
            {
                if(index.count(accounts[i][j]))
                {
                    id = index[accounts[i][j]];
                    graph[id].insert(i);
                    graph[i].insert(id);
                }
                index[accounts[i][j]] = id;
            }
        }
        vector<vector<string>> res;
        vector<bool> marged(accounts.size(),false);
        for(int i = 0;i<accounts.size();i++)
        {
            if(marged[i])
                continue;
            unordered_set<int> visited;
            set<string> mail;
            dfs(accounts,mail,visited,i,graph);
            for(auto&v :visited)
                marged[v] = true;
            vector<string> a(mail.begin(),mail.end());
            a.insert(a.begin(),accounts[i][0]);
            res.push_back(a);
        }
        return res;
    }
    
    void dfs(vector<vector<string>>& accounts,set<string>& mail,unordered_set<int>& visited,int now,unordered_map<int,unordered_set<int>>& graph)
    {
        if(visited.count(now))
            return;
        visited.insert(now);
        for(int j = 1;j < accounts[now].size();j++)
        {
            mail.insert(accounts[now][j]);
        }
        for(auto &g:graph[now])
        {
            dfs(accounts,mail,visited,g,graph);
        }
    }
};
