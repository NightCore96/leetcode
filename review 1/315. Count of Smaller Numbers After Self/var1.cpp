struct STreeNode
{
    int val;
    int small;
    STreeNode *left;
    STreeNode *right;
    STreeNode(int x) : val(x), small(0), left(NULL), right(NULL) {}
};
class Solution {
public:
    vector<int> countSmaller(vector<int>& nums) {
        if(nums.empty())
            return nums;
        STreeNode *root=NULL;
        vector<int> count(nums.size(),0);
        for(int i=nums.size()-1;i>=0;i--)
        {
            count[i]=insertBST(root,nums[i],0);
        }
        return count;
    }
private:
    int insertBST(STreeNode *&root,int v,int s)
    {
        if(!root)
        {
            root =new STreeNode(v);
            return s;
        }
        else if(root->val >=v)
        {
            root->small++;
            return insertBST(root->left,v,s);
        }
        else
        {
            int n = root->small+s+1;
            return insertBST(root->right,v,n);
        }
    }
};
