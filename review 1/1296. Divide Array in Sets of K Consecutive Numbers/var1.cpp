class Solution {
public:
    bool isPossibleDivide(vector<int>& hand, int W)
    {
        if(hand.size() % W != 0)
            return false;
        unordered_map<int,int> cards;
        for(auto& h:hand)
        {
            cards[h]++;
        }
        priority_queue<pair<int,int>> pq;
        for(auto& p:cards)
        {
            pq.push(p);
        }
        
        while(!pq.empty())
        {
            vector<pair<int,int>> temp;
            for(int i = 0;i<W;i++)
            {
                if(pq.empty())
                    return false;
                pair<int,int> card = pq.top();
                pq.pop();
                if(i != 0 && temp.back().first != card.first+1)
                    return false;
                temp.push_back({card.first,card.second-1});
            }
            for(auto& t :temp)
            {
                if(t.second > 0)
                    pq.push(t);
            }
        }
        return true;
    }
    
};
