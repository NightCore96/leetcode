class Solution {
public:
    vector<vector<string>> findLadders(string beginWord, string endWord, vector<string>& wordList) 
    {
        vector<vector<string>> result;
        unordered_set<string> dist;
        unordered_set<string> levelList;
        unordered_map<string,vector<string>> pathlist;
        queue<string> strqueue;
        bool isEnd =false;
        for(auto str:wordList)
        {
            dist.insert(str);
        }
        if(dist.find(endWord)==dist.end())
        {
            return result;
        }
        strqueue.push(beginWord);
        strqueue.push("");
        int step=1;
        while(!strqueue.empty())
        {
            string str=strqueue.front();
            strqueue.pop();
            if(str!="")
            {
                for(int i=0;i<str.length();i++)
                {
                    string tmp=str;
                    for(char j='a';j<='z';j++)
                    {
                        if(tmp[i]==j)
                            continue;
                        str[i]=j;
                        if(str==endWord)
                        {
                            isEnd=true;
                            pathlist[str].push_back(tmp);
                            goto NEXTTURN;
                        }
                        if(dist.find(str)!=dist.end())
                        {
                            pathlist[str].push_back(tmp);
                            if(levelList.find(str)==levelList.end())
                            {
                                strqueue.push(str);
                                levelList.insert(str);
                            }
                        }
                    }
                    str=tmp;
                }
            }
            else if(!strqueue.empty())
            {
                if(isEnd)
                    break;
                for(auto s:levelList)
                    dist.erase(s);
                levelList.clear();
                strqueue.push("");
            }
            NEXTTURN:;
        }
        if(pathlist.find(endWord)==pathlist.end())
            return result;
        vector<string> visit;
        visit.push_back(endWord);
        BuildPath(beginWord, 
                      endWord,
                      visit,
                      pathlist,
                      result);
        return result;
    }
private:
    void BuildPath(string beginWord, 
                   string endWord,
                   vector<string> &visit,
                   unordered_map<string,vector<string>> &pathlist,
                   vector<vector<string>> &result)
    {
        if(beginWord==endWord)
        {
            result.push_back(visit);
            return;
        }
        vector<string> path = pathlist[endWord];
        for(string s:path)
        {
            visit.insert(visit.begin(),s);
            BuildPath(beginWord, 
                      s,
                      visit,
                      pathlist,
                      result);
            visit.erase(visit.begin());
        }
    }
};